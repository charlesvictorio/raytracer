extern crate proc_macro;

use strum_macros;

use proc_macro::TokenStream;
use proc_macro2;
use quote::{format_ident, quote};
use syn::{parse_macro_input, DeriveInput, Ident};

// maybe have field attribute macro saying this field isn't a dimension

// macros used to be defined here
// define declarative macro and put its code/data into proc macro, then execute it in main.rs ???

// macro_rules! switch_args {
//     ($func_name:ident ( $arg1:expr, $arg2:expr ) ) => ($func_name ($arg2, $arg1));
// }

/*
// maybe this schould be macro
fn make_func_broadcasted_to_fields(
    // struct_name: &proc_macro::Ident,
    struct_name: &syn::Ident,
    field_names: &Vec<&Option<syn::Ident>>,
    new_func_ident: proc_macro2::TokenStream, //syn::Ident,
    func_to_call: proc_macro2::TokenStream,
) -> proc_macro2::TokenStream {
    // x: f64::sqrt(self.x)
    let func_applied_to_fields = field_names.iter().map(|name| {
        quote! { #name: #func_to_call ( self.#name )}
    });
    quote! {
        pub fn #new_func_ident (&self) -> #struct_name {
            #struct_name {
                #(#func_applied_to_fields),*
            }
        }
    }
}
*/

#[proc_macro_derive(VectorSpace)]
pub fn vecspace_derive(input: TokenStream) -> TokenStream {
    // rust code -> syntax tree
    //let ast = syn::parse(input).unwrap();
    let ast = parse_macro_input!(input as DeriveInput);

    // Don't need to make entire TokenStream at once: https://docs.rs/quote/latest/quote/macro.quote.html#combining-quoted-fragments

    let struct_name = &ast.ident;

    let fields = if let syn::Data::Struct(syn::DataStruct {
        fields: syn::Fields::Named(syn::FieldsNamed { ref named, .. }),
        ..
    }) = ast.data { named } else { unimplemented!(); };
    let ndim = fields.iter().count();
    let field_names: Vec<syn::Ident> = fields.iter().map(|f| f.clone().ident.unwrap()).collect(); // move fields out of options

    // like: `x: self.x + other.x, y: self.y + other.y, z: self.z + other.z`
    let add_each_field = field_names.iter().map(|name| {
        quote! { #name: self.#name + other.#name }
    });
    let add_each_field2 = add_each_field.clone(); // there's gotta be a better way to reuse iterators
    let sub_each_field = field_names.iter().map(|name| {
        quote! { #name: self.#name - other.#name }
    });
    let sub_each_field2 = sub_each_field.clone();

    let vec_add_and_sub_code = quote! {
        impl std::ops::Add for #struct_name {
            type Output = #struct_name;
            fn add(self, other: #struct_name) -> #struct_name {
                // #struct_name { #(#add_each_field.iter()),* }
                #struct_name { #(#add_each_field),* }
            }
        }
        impl std::ops::AddAssign for #struct_name {
            fn add_assign(&mut self, other: Self) {
                *self = Self { #(#add_each_field2),* }
            }
        }
        impl std::ops::Sub for #struct_name {
            type Output = #struct_name;
            fn sub(self, other: #struct_name) -> #struct_name {
                #struct_name { #(#sub_each_field),* }
            }
        }
        impl std::ops::SubAssign for #struct_name {
            fn sub_assign(&mut self, other: Self) {
                *self = Self { #(#sub_each_field2),* }
            }
        }
    };

    // like `x: self.x * c`
    let scale_each_field = field_names.iter().map(|name| {
        quote! { #name: self.#name * c }
    });
    let scale_each_field2 = scale_each_field.clone(); // there's gotta be a better way lmao

    // like `x: v.x * self`
    let scale_each_field_of_v = field_names.iter().map(|name| {
        quote! { #name: v.#name * self }
    });
    let neg_each_field = field_names.iter().map(|name| {
        quote! { #name: -self.#name }
    });

    let scalar_multiplication_code = quote! {
        impl std::ops::Mul<f64> for #struct_name {
            type Output = #struct_name;
            fn mul(self, c: f64) -> #struct_name {
                #struct_name { #(#scale_each_field),* }
            }
        }
        impl std::ops::Mul<#struct_name> for f64 {
            type Output = #struct_name;
            fn mul(self, v: #struct_name) -> #struct_name {
                #struct_name { #(#scale_each_field_of_v),* }
            }
        }
        impl std::ops::MulAssign<f64> for #struct_name {
            fn mul_assign(&mut self, c: f64) {
                *self = #struct_name { #(#scale_each_field2),* }
            }
        }
        impl std::ops::Neg for #struct_name {
            type Output = #struct_name;
            fn neg(self) -> #struct_name {
                #struct_name { #(#neg_each_field),* }
            }
        }
    };

    let each_field = field_names.iter().map(|name| {
        quote! { self.#name }
    });
    let params_with_commas = vec!["{:.4}"; ndim].join(", ");
    let fmt_str = "<".to_owned() + &params_with_commas + ">";

    let display_code = quote! {
        impl std::fmt::Display for #struct_name {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                write!(f, #fmt_str, #(#each_field),*)
            }
        }
    };

    let cmp_each_param = field_names.iter().map(|name| {
        quote! { feq(self.#name, other.#name) }
    });

    let equality_code = quote! {
        impl PartialEq for #struct_name {
            fn eq(&self, other: &Self) -> bool {
                #(#cmp_each_param)&&*
            }
        }
        impl Eq for #struct_name {}
    };

    let vec_axes_enum_ident = format_ident!("{}Axes", struct_name);
    let uppercase_field_names = field_names.iter().map(|name| {
        // format_ident!("Capital{}", /*name*/ struct_name)
        // need to make identifier uppercase some how
        // name
        Ident::new(&name.to_string().to_uppercase(), syn::spanned::Spanned::span(&name))
    });
    let uppercase_field_names2 = uppercase_field_names.clone();
    let define_enum_code = quote! {
        #[derive(Copy, Clone, Debug, strum_macros::EnumIter)]
        pub enum #vec_axes_enum_ident { #(#uppercase_field_names),* }
    };
    let index_usize_match_arms = field_names.iter().enumerate().map(|(i, name)| {
        quote! { #i => &self.#name }
    });
    let index_enum_match_arms = field_names.iter().zip(uppercase_field_names2).map(|(field_name, upper_field_name)| {
        quote! { <#vec_axes_enum_ident>::#upper_field_name => &self.#field_name }
    });
    let index_code = quote! {
        impl std::ops::Index<usize> for #struct_name {
            type Output = f64;
            fn index(&self, axis: usize) -> &f64 {
                match axis {
                    #(#index_usize_match_arms),* ,
                    _ => panic!("Index out of bounds")
                }
            }
        }
        impl std::ops::Index<#vec_axes_enum_ident> for #struct_name {
            type Output = f64;
            fn index(&self, axis: #vec_axes_enum_ident) -> &f64 {
                match axis {
                    #(#index_enum_match_arms),*
                }
            }
        }
    };

    let mul_each_field = field_names.iter().map(|name| {
        quote! { #name: self.#name * other.#name }
    });
    let schur_code = quote! {
        fn schur(self, other: Self) -> Self {
            #struct_name { #(#mul_each_field),* }
        }
    };

    let rms_each_field = field_names.iter().map(|name| {
        quote! { #name: ((self.#name * self.#name + other.#name * other.#name) * 0.5).sqrt() }
    });
    let rms_code = quote! {
        pub fn rms(self, other: #struct_name) -> #struct_name {
            #struct_name {
                #(#rms_each_field),*
            }
        }
    };

    // Needed for version 3
    // let add_squared_field_to_sum = field_names.iter().map(|name| {
    //     quote! { sum_of_squares.#name += p.#name * p.#name; }
    // });
    // let sqrt_of_ss_over_n = field_names.iter().map(|name| {
    //     quote! { #name: (sum_of_squares.#name * one_over_n).sqrt() }
    // });

    // Root mean square of more than 2 points
    // must be defined after schur and sqrt
    let rms_points_code = quote! {
        pub fn rms_points(pts: &[#struct_name]) -> #struct_name {
            let one_over_n: f64 = 1.0 / pts.len() as f64;

            let sum_of_squares = pts.iter()
                .fold(<#struct_name>::default(), |sum, val| sum + (*val).schur(*val));
            (sum_of_squares * one_over_n).sqrt()
            
            // let sum_of_squares = pts.iter()
            //     .fold(
            //         Vec2 { x: 0.0, y: 0.0 },
            //         |sum, val|
            //             Vec2 {
            //                 x: sum.x + *val.x * *val.x,
            //                 y: sum.y + *val.y * *val.y
            //         }
            //     );
            
            // Version 3
            // let mut sum_of_squares = <#struct_name>::default(); // additive identity, <0.0, 0.0>
            // for p in pts.iter() {
            //     #(#add_squared_field_to_sum)*
            // }
            // #struct_name {
            //     #(#sqrt_of_ss_over_n),*
            // }
        }
    };


    let add2rms_code = quote! {
        pub fn add2rms(old_rms: #struct_name, new_point: #struct_name, new_n: usize) -> #struct_name {
            lerp(old_rms.schur(old_rms), new_point.schur(new_point), 1.0 / new_n as f64).sqrt()
        }
    };

    let unaryop_of_fields = field_names.iter().map(|name| {
        quote! { #name: $func_to_call(self.#name)}
    });
    let unaryop_of_fields2 = unaryop_of_fields.clone(); // there's gotta be a better way to do this
    let broadcast_unaryop_ident_for_this_struct = format_ident!("broadcast_unaryop{}", struct_name);
    let smuggled_broadcast_unaryop_macro = quote! {
        macro_rules! #broadcast_unaryop_ident_for_this_struct {
            ($func_name:ident, $func_to_call:path) => {
                pub fn $func_name (&self) -> #struct_name {
                    #struct_name {
                        #(#unaryop_of_fields),*
                    }
                }
            };
            ($func_name:ident, $func_to_call:path, private) => { // there's gotta be a better way to do this
                fn $func_name (&self) -> #struct_name {
                    #struct_name {
                        #(#unaryop_of_fields2),*
                    }
                }
            };
        }
    };

    // let square_fields = field_names.iter().map(|name| {
    //     quote! { self.#name * self.#name }
    // });
    // let square_fields2 = square_fields.clone();
    let mul_each_field2 = field_names.iter().map(|name| {
        quote! { self.#name * other.#name }
    });
    let mul_each_field3 = mul_each_field2.clone();
    let dot_code = quote! {
        fn dot(self, other: Self) -> f64 {
            #(#mul_each_field2)+*
        }
        // Moved into default trait implementation
        // // In inlining we trust!
        // fn length_sq(self) -> f64 {
        //     // #(#square_fields2)+*
        //     self.dot(self)
        // }
        // fn length(self) -> f64 {
        //     // (#(#square_fields)+*)
        //     self.length_sq()
        //     .sqrt()
        // }
        // fn normalize(self) -> Self {
        //     let one_over_length = 1.0 / self.length();
        //     self * one_over_length
        // }
    };

    // identical to dot product, for backwards compatability
    let vec_vec_mul_code = quote! {
        impl std::ops::Mul<#struct_name> for #struct_name {
            type Output = f64;
            fn mul(self, other: #struct_name) -> f64 {
                #(#mul_each_field3)+*
            }
        }
    };

    quote! {
        // use std::ops::Mul;

        #smuggled_broadcast_unaryop_macro
        
        #vec_add_and_sub_code
        #scalar_multiplication_code
        #vec_vec_mul_code
        #display_code
        #equality_code
        #define_enum_code
        #index_code
        
        impl VectorSpace for #struct_name {
            #schur_code
            #broadcast_unaryop_ident_for_this_struct!(sqrt, f64::sqrt, private);
            #dot_code
        }
        impl #struct_name {
            // #rms_code
            // #rms_points_code
            // #add2rms_code
            #broadcast_unaryop_ident_for_this_struct!(exp, f64::exp);
            #broadcast_unaryop_ident_for_this_struct!(recip, f64::recip);
            // #broadcast_unaryop_ident_for_this_struct!(sin, f64::sin);
        }
    }
    .into()

    // transform ast
    // impl_vecspace(&ast)
}