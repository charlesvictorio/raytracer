//use std::fmt;
use std::default::Default;
use std::ops::{Add, Sub, Mul};//Neg, AddAssign, SubAssign, MulAssign};

// Topological Vector Space
// Finite-dim vector space over R
// The linops are continuous
// also normed - inner product space?

// use core_math;
pub fn lerp<T>(a: T, b: T, t: f64) -> T { a } // temp

pub trait VectorSpace:
    Copy
  + Default
  + Add<Self, Output = Self>
  + Sub<Self, Output = Self>
  + Mul<f64, Output = Self>
  // add more bounds that have already been implemented
{
    // This could go in `impl #struct_name` too, if performance warrants it. or generic'd
    // I need to know more abt overhead for calling functions
    // in diff ways: trait default implementation, etc. Does that get Monomorphization?
    
    // Do all these need to be defined for every vecspace? can they be?

    fn schur(self, other: Self) -> Self;
    fn sqrt(&self) -> Self; // vector's elements must be floats. continuous vector spaces? topological vector spaces?

    // These could be generic'd. conceptually, they work on TVS's, but TVS's dont need them
    // fn rms<T: TopologicalVectorSpace>(a: T, b: T) -> T 
    fn rms(self, other: Self) -> Self {
        ((self.schur(self) + other.schur(other)) * 0.5).sqrt()
    }
    fn rms_points(pts: &[Self]) -> Self {
        let one_over_n: f64 = 1.0 / pts.len() as f64;
        let sum_of_squares = pts.iter()
            .fold(Self::default(), |sum, val| sum + (*val).schur(*val));
        (sum_of_squares * one_over_n).sqrt()
    }
    // like add2avg<T>() but for rms
    // old_rms = \sqrt{frac{a^2 + b^2 + c^2}{3}}
    // new_rms = \sqrt{frac{a^2 + b^2 + c^2 + d^2}{4}}
    // \sqrt{ old_rms^2 * \frac{3}{4} + \frac{1}{4} * d^2 } = new_rms
    fn add2rms(old_rms: Self, new_point: Self, new_n: usize) -> Self {
        lerp(old_rms.schur(old_rms), new_point.schur(new_point), 1.0 / new_n as f64).sqrt()
    }

    fn dot(self, other: Self) -> f64; // depends on number of fields
    // In inlining we trust!
    fn length_sq(self) -> f64 {
        // #(#square_fields2)+*
        self.dot(self)
    }
    fn length(self) -> f64 {
        // (#(#square_fields)+*)
        self.length_sq()
        .sqrt()
    }
    fn normalize(self) -> Self {
        let one_over_length = 1.0 / self.length();
        self * one_over_length
    }

    // Computes the reflection of self wrt other.
    // See Computer Graphics from Scratch / Light / Figure 3-15
    // https://www.gabrielgambetta.com/computer-graphics-from-scratch/03-light.html
    // 2022.11.23: I think other must have length 1.
    // 2023.03.18: other *must* have length 1 !!!
    // This works when it is the tail of self that is touching the tail of other.
    // ^   ^   |   ^    is `self`
    //  \ /    |    \
    // ----    |
    // But if you want it where the head of self is touching the tail of other, then you should do `(-self).reflect(other)`.
    // \   ^   |    \   is `-self` // edit: i think that is `self` and you should do `-self` to get correct refl
    //  v /    |     v
    // ----    |
    fn reflect(self, other: Self) -> Self {
        if other.length() == 1.0 {
            println!("report that you didn't need to normalize")
        }
        let other = other.normalize();
        other * (2.0 * self.dot(other)) - self
    }
}

// Standard basis vectors
// use in `impl #struct_name`
// put in another crate later or something idk maybe it can go in vecspace file
#[macro_export]
macro_rules! common_vec {
    ($struct_name:ident $vec_name:ident = < $($val:literal),+ > ) => {
        pub fn $vec_name () -> $struct_name {
            <$struct_name>::new( $($val),+ )
        }
    };
}