extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(Constructor)]
pub fn constructor_derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);

    let struct_name = &ast.ident;
    let fields = if let syn::Data::Struct(syn::DataStruct {
        fields: syn::Fields::Named(syn::FieldsNamed { ref named, .. }),
        ..
    }) = ast.data { named } else { unimplemented!(); };
    let field_names = fields.iter().map(|f| &f.ident );
    let name_colon_type = fields.iter().map(|f| {
        let field_name = &f.ident;
        let field_type = &f.ty;
        quote! { #field_name: #field_type }
    });
    quote! {
        impl #struct_name {
            pub fn new( #(#name_colon_type),* ) -> #struct_name {
                #struct_name { #(#field_names),* }
            }
        }
    }
    .into()
}