// Quaternions
// Not really used, I prefer rotation mats for now.

use crate::vec3::Vec3;

#[derive(Debug, Copy, Clone)]
pub struct Quat {
    pub a: f64,
    pub b: f64,
    pub c: f64,
    pub d: f64
}

impl Quat {
    pub fn new(a: f64, b: f64, c: f64, d: f64) -> Quat {
        Quat {a, b, c, d}
    }

    pub fn from_axis_angle(axis: Vec3, angle: f64) -> Quat {
        // Get a Quat from a *normalized* axis vector and a angle in radians.
        // $$\f64::cos(\theta / 2) + \f64::sin(\theta / 2)(a \hat{i} + b \hat{j} + c \hat{k}
        let half_theta: f64 = angle / 2.0;
        let sin_half_theta: f64 = half_theta.sin();
        Quat::new(half_theta.cos(), sin_half_theta * axis.x, sin_half_theta * axis.y, sin_half_theta * axis.z)
    }

    pub fn inverse(&self) -> Self {
        Quat::new(self.a, -self.b, -self.c, -self.d)
    }

    pub fn ham(q1: &Quat, q2: &Quat) -> Quat {
        // Compute the Hamilton product of q1 and q2.
        Quat::new(
            q1.a * q2.a - q1.b * q2.b - q1.c * q2.c - q1.d * q2.d,
            q1.a * q2.b + q1.b * q2.a + q1.c * q2.d - q1.d * q2.c,
            q1.a * q2.c - q1.b * q2.d + q1.c * q2.a + q1.d * q2.b,
            q1.a * q2.d + q1.b * q2.c - q1.c * q2.b + q1.d * q2.a
        )
    }

    pub fn rotate_vec(&self, v: Vec3) -> Vec3 {
        // Rotate a Vec3 with a Quat
        // https://math.stackexchange.com/questions/40164/how-do-you-rotate-a-vector-by-a-unit-quaternion

        let v: Quat = Quat::new(0.0, v.x, v.y, v.z); // Encode v as a Quat by addding a 0
        let ans_extra_0: Quat = Quat::ham(&Quat::ham(self, &v), &self.inverse());
        Vec3::new(ans_extra_0.b, ans_extra_0.c, ans_extra_0.d) // Take away a 0
    }

}