use crate::vec3::Vec3;
// #[derive(Debug)]
// struct PointLight;
/*
#[derive(Debug)]
struct AmbientLight { intensity: f64 }
#[derive(Debug)]
struct PointLight { intensity: f64, position: Vec3 }
#[derive(Debug)]
struct DirectionalLight { intensity: f64, direction: Vec3 }

pub trait ComputeAddedBrightness {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64;
}

impl ComputeAddedBrightness for AmbientLight {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 { self.intensity }
}

fn compute_added_brightness_inner(point: Vec3, light_intensity: f64, point_to_light: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 {

    let (sphere_in_the_way, closest_t) = get_closest_ray_sphere_intersection(&point, &point_to_light, 0.001, f64::INFINITY, spheres);
    if let Some(sphere) = sphere_in_the_way {
        return 0.0;
    }

    // Diffuse reflection
    let n_dot_l = Vec3::dot(normal, point_to_light);
    //println!("n_dot_l {:?}", n_dot_l);

    let intensity_from_diffuse_reflection: f64 = {
        if n_dot_l > 0.0 {
            let cos_of_angle_btwn_normal_and_light_vecs: f64 = n_dot_l / (Vec3::length(normal) * Vec3::length(point_to_light));
            //added_intensity += light_intensity * cos_of_angle_btwn_normal_and_light_vecs;
            //println!("n_dot_l > 0, and intensity added is {}", light_intensity * cos_of_angle_btwn_normal_and_light_vecs);
            light_intensity * cos_of_angle_btwn_normal_and_light_vecs
        } else { 0.0 }
    };

    // Specular reflection
    let intensity_from_specular_reflection: f64 = {
        match specular {
            Some(s) => {
                let light_reflection: Vec3 = {
                    // component of point_to_line that is parallel to normal
                    // point_to_line projected onto normal
                    // proj_{\vec{b}} \vec{a} = \frac{\vec{a} \cdot \vec{b}}{\lVert \vec{b} \rVert}
                    // * \frac{\vec{b}}{\lVert \vec{b} \rVert}
                    // In this case, \vec{b} is a unit vector
                    let p2l_n_len: f64 = Vec3::dot(point_to_light, normal);
                    let p2l_n: Vec3 = Vec3::scale(normal, p2l_n_len);

                    Vec3::sub(Vec3::scale(p2l_n, 2.0), point_to_light)
                };
                let r_dot_v = Vec3::dot(light_reflection, view);
                //println!("vec_r {:?} r_dot_v {:?}", light_reflection, r_dot_v);
                if r_dot_v > 0.0 {
                    let cos_of_angle_btwn_reflection_and_view = r_dot_v / (Vec3::length(light_reflection) * Vec3::length(view));
                    //println!("n_dot_l > 0, and intensity added is {}", light_intensity * f64::powf(cos_of_angle_btwn_reflection_and_view, s as f64));
                    light_intensity * f64::powf(cos_of_angle_btwn_reflection_and_view, s as f64)
                } else { 0.0 }
            },
            None => { 0.0 }
        }
    };
    intensity_from_diffuse_reflection + intensity_from_specular_reflection
}

impl ComputeAddedBrightness for PointLight {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 {
        let point_to_light: Vec3 = Vec3::sub(self.position, point);
        compute_added_brightness_inner(point, self.intensity, point_to_light, normal, view, specular, spheres)
    }
}

impl ComputeAddedBrightness for DirectionalLight {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 {
        // the direction of `DirectionalLight`s go from object to light source (point to light)
        compute_added_brightness_inner(point, self.intensity, self.direction, normal, view, specular, spheres)
    }
}

//fn compute_lighting<LightType: ComputeAddedBrightness>(
fn compute_lighting(
    point: Vec3, // The point (eg on a sphere) that light (maybe) touches and colors
    normal: Vec3, // The unit normal vector, perpendicular to the surface at that point
    view: Vec3, // A vector from the point to the camera (i think)
    specular: Option<i64>, // The shininess of an object
    lights: &Vec<Box<dyn ComputeAddedBrightness>>, // The lights that will be illuminating that point
    spheres: &Vec<Sphere>
) -> f64 {
    // Compute how bright a surface should be at a point
    let mut total_intensity: f64 = 0.0;
    //let length_n: f64 = Vec3::length(normal);
    //let length_v: f64 = Vec3::length(view);

    for light in lights.iter() {
        total_intensity += light.compute_added_brightness(point, normal, view, specular, spheres);
    }
    total_intensity
}
*/