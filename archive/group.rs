// Archiving group because I don't want it
use crate::core_math::feq;
use crate::vec3::Vec3;
use crate::color::Color;
use crate::matrix::Mat;
use crate::ray::Ray;
use crate::object::{Object, Intersection, Material};
use crate::aabb::Aabb;

// implement shape
// have list of &children, and one Option<&parent>
// add shape to group
// ray group intersection
// normal
// transform shape

pub struct Group<'a> {
    // object space to group space
    // o2g: Mat,
    // g2o: Mat,
    // children: Vec<Box<&'a dyn Object>>,
    children: Vec<&'a dyn Object>,
    parent: Option<&'a dyn Object>,
    aabb: Aabb,
}

impl Object for Group<'_> {
    fn get_material(&self) -> &Material {
        panic!("Will never get here, since a ray will intersect with a terminal subobject, which actually does have a material");
    }
    fn get_all_ray_obj_ints(&self, ray: &Ray) -> Vec<Intersection> {
        let mut ints: Vec<Intersection> = Vec::new();
        for child in self.children.iter() { // linear
            ints.extend(child.get_all_ray_obj_ints(ray))
        }
        return ints;
    }
    fn get_first_ray_obj_int(&self, ray: &Ray) -> Option<f64> {
        let ints = self.get_all_ray_obj_ints(ray); // lol not faster
        let mut min_t = f64::INFINITY;
        let mut min_ti: Option<usize> = None;
        for (i, Intersection { t: t, obj: _ }) in ints.iter().enumerate() {
            if *t < min_t {
                min_t = *t;
                min_ti = Some(i)
            }
        }
        if let Some(_) = min_ti {
            return Some(min_t);
        }
        return None;
    }
    fn normal_at(&self, point: Vec3) -> Vec3 {
        panic!("Will never get here, since a ray will intersect with a terminal subobject, which actually does have a normal");
    }
    fn base_color_at(&self, point: Vec3) -> Color {
        panic!("Will never get here, since a ray will intersect with a terminal subobject, which actually does have a base color");
    }

    fn get_aabb(&self) -> &Aabb {
        return &self.aabb;
    }
}