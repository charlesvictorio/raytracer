// don't use this. archive it maybe
use crate::core_math::feq;
use crate::vec3::Vec3;
use crate::color::Color;
use crate::ray::Ray;
use crate::object::{Object, Intersection};
use crate::material::Material;
use crate::aabb::Aabb;

#[derive(Debug)]
pub struct Triangle {
    pub v1: Vec3, pub v2: Vec3, pub v3: Vec3, // 3 verticies of triangle
    pub e1: Vec3, pub e2: Vec3, // edges: e1 is from v1 to v2. e2 from v1 to v3
    pub vn1: Vec3, pub vn2: Vec3, pub vn3: Vec3, // vertex normals
    pub vt1: (f64, f64), pub vt2: (f64, f64), pub vt3: (f64, f64), // texture verticies
    pub material: usize,
}

impl Triangle {
    // new, just from 3 verticies.
    // Triangles will mainly be made from .obj files, not from Triangle::new().
    // Make coordinates in CCW order.
    // [scratch a pixel](https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/geometry-of-a-triangle.html)
    pub fn new(v1: Vec3, v2: Vec3, v3: Vec3, material: usize) -> Triangle {
        let e1 = v2 - v1;
        let e2 = v3 - v1;
        // should norm be e1 x e2 or e2 x e1?
        // should it be length 1?
        let norm = Vec3::cross(e1, e2).normalize();
        Triangle {
            v1, v2, v3,
            e1, e2,
            vn1: norm, vn2: norm, vn3: norm, 
            vt1: (0.0, 0.0), vt2: (0.0, 0.0), vt3: (0.0, 0.0), 
            material
        }
    }

    // inner helper function for barycentric coordinates (uv coordinates relative to triangle verticies)
    pub fn inner_uv(v1: Vec3, v2: Vec3, v3: Vec3, point: Vec3) -> (f64, f64) {
        let one_over_det = 1.0 / Vec3::s_trip_prod(v1, v2, v3);
        let u = Vec3::s_trip_prod(v1, point, v3) * one_over_det;
        let v = Vec3::s_trip_prod(v1, v2, point) * one_over_det;
        (u, v)
    }

    // Möller-Trumbore ray-triangle intersection algorithm
    // For triangles, you need to calculate the uv to calculate the t.
    // So return the t, u, v of the intersection if it exists.
    pub fn inner_intersection(&self, ray: &Ray) -> Option<(f64, f64, f64)> {
        let o_minus_a = ray.start - self.v1;
        let neg_d = -ray.dir;
        let e1xe2 = Vec3::cross(self.e1, self.e2); // self.norm is this, normalized.
        // Taking the dot product of the direction and the normal vector also lets me know if the ray is parallel to the triangle.
        let det = neg_d * e1xe2;
        if feq(det, 0.0) {
            return None;
        }
        let recip_det = 1.0 / det;
        let u = neg_d * Vec3::cross(o_minus_a, self.e2) * recip_det;
        if u < 0.0 || u > 1.0 {
            return None;
        }
        let v = neg_d * Vec3::cross(self.e1, o_minus_a) * recip_det;
        if v < 0.0 || u + v > 1.0 {
            return None;
        }
        let t = o_minus_a * e1xe2 * recip_det;
        return Some((t, u, v));
    }

}

impl Default for Triangle {
    fn default() -> Self {
        Triangle::new(
            Vec3::ihat(),
            Vec3::jhat(),
            Vec3::zero(),
            0
        )
    }
}

impl Object for Triangle {
    fn get_aabb(&self) -> Aabb {
        let mut aabb = Aabb::default();
        aabb.add_point(self.v1);
        aabb.add_point(self.v2);
        aabb.add_point(self.v3);
        return aabb;
    }
    fn get_centroid(&self) -> Vec3 {
        let one_third = 1.0 / 3.0;
        let centroid = Vec3::new(
            (self.v1.x + self.v2.x + self.v3.x) * one_third,
            (self.v1.y + self.v2.y + self.v3.y) * one_third,
            (self.v1.z + self.v2.z + self.v3.z) * one_third
        );
        return centroid;
    }

    // It doesn't take any extra work to calculate the uv and t
    // just calculate t and uv and throw away the uv 
    fn is_hit_by(&self, ray: &Ray) -> Option<f64> {
        self.inner_intersection(ray).map(|(t, _, _)| t)
    }

    fn intersection(&self, ray: &Ray) -> Option<Intersection> {
        self.inner_intersection(ray)
            .map(|(t, u, v)| {
                let p = ray.plug_in_t(t);
                assert!(ray.dir.length() == 1.0); // testing
                Intersection {
                    p,
                    uv: (u, v),
                    norm: self.norm_at(p),
                    wo: -ray.dir.normalize(),
                    obj: self,
                    t
                }
            })
    }
    fn get_material_i(&self) -> usize {
        self.material
    }
    fn uv_at(&self, point: Vec3) -> (f64, f64) {
        Triangle::inner_uv(self.v1, self.v2, self.v3, point)
    }
    fn norm_at(&self, _point: Vec3) -> Vec3 {
        // assert point is on triangle
        // interpolate vertex normals
        
    }
}
