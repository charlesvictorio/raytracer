// Color
// yet another 3d vector space

use std::fmt;
use std::ops::{Add, Sub, Mul, AddAssign, SubAssign, MulAssign};
use std::convert::Into;

use crate::core_math::{feq, lerp};

use constructor::Constructor;
use image::{Rgb, Rgba};
use vecspace::VectorSpace;
use vecspace::common_vec;
use vecspace_derive::VectorSpace;

// some issues may stem from * being schur vs dot. find all Color * Color, replace with Color.schur(Color)
#[derive(Debug, Copy, Clone, Default, Constructor, VectorSpace)]
pub struct Color {
    // RGB values on a scale from 0.0 to 1.0.
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

/*
impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<{:.4}, {:.4}, {:.4}>", self.r, self.g, self.b)
    }
}

impl PartialEq for Color {
    fn eq(&self, other: &Self) -> bool {
        feq(self.r, other.r) && feq(self.g, other.g) && feq(self.b, other.b)
    }
}

impl Eq for Color {}

impl Add for Color {
    type Output = Color;
    fn add(self, other: Color) -> Color {
        Color { r: self.r + other.r, g: self.g + other.g, b: self.b + other.b }
    }
}

impl Sub for Color {
    type Output = Color;
    fn sub(self, other: Color) -> Color {
        Color { r: self.r - other.r, g: self.g - other.g, b: self.b - other.b }
    }
}

// Hadamard product or Schur product
impl Mul for Color {
    type Output = Color;
    fn mul(self, other: Color) -> Color {
        Color { r: self.r * other.r, g: self.g * other.g, b: self.b * other.b }
    }
}

// Scaling colors
impl Mul<f64> for Color {
    type Output = Color;
    fn mul(self, c: f64) -> Color {
        Color::new(self.r * c, self.g * c, self.b * c)
    }
}

impl Mul<Color> for f64 {
    type Output = Color;
    fn mul(self, v: Color) -> Color {
        Color::new(v.r * self, v.g * self, v.b * self)
    }
}

impl AddAssign for Color {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            r: self.r + other.r,
            g: self.g + other.g,
            b: self.b + other.b
        }
    }
}

impl SubAssign for Color {
    fn sub_assign(&mut self, other: Self) {
        *self = Self {
            r: self.r - other.r,
            g: self.g - other.g,
            b: self.b - other.b
        }
    }
}

impl MulAssign for Color {
    fn mul_assign(&mut self, other: Self) {
        *self = Self {
            r: self.r * other.r,
            g: self.g * other.g,
            b: self.b * other.b
        }
    }
}
*/

impl Color {
    // pub fn new(r: f64, g: f64, b: f64) -> Color {
    //     Color { r, g, b }
    // }

    // Take rgb color channels [0, 255] turn it into a color
    pub fn from_rgb(r: u8, g: u8, b: u8) -> Color {
        Color::new(
        r as f64 / 255.0,
        g as f64 / 255.0,
        b as f64 / 255.0,
        )
    }

    common_vec!(Color black = <0.0, 0.0, 0.0>);
    common_vec!(Color white = <1.0, 1.0, 1.0>);
    // pub fn black() -> Color {
    //     Color { r: 0.0, g: 0.0, b: 0.0 }
    // }

    // pub fn white() -> Color {
    //     Color { r: 1.0, g: 1.0, b: 1.0 }
    // }

    /*
    pub fn scale(self, c: f64) -> Self {
        Self { r: self.r * c, g: self.g * c, b: self.b * c }
    }
    */

    // pub fn avg2colors(c1: Color, c2: Color) -> Color {
    //     Color::new(
    //         f64::sqrt((c1.r * c1.r + c2.r * c2.r) / 2.0),
    //         f64::sqrt((c1.g * c1.g + c2.g * c2.g) / 2.0),
    //         f64::sqrt((c1.b * c1.b + c2.b * c2.b) / 2.0)
    //     )
    // }

    // pub fn avg_colors(colors: &[Color]) -> Color {
    //     let mut r_ssq: f64 = 0.0;
    //     let mut g_ssq: f64 = 0.0;
    //     let mut b_ssq: f64 = 0.0;
    //     for &Color { r, g, b } in colors.iter() {
    //         r_ssq += r * r;
    //         g_ssq += g * g;
    //         b_ssq += b * b;
    //     }
    //     let n: f64 = colors.len() as f64;
    //     Color::new(
    //         f64::sqrt(r_ssq / n),
    //         f64::sqrt(g_ssq / n),
    //         f64::sqrt(b_ssq / n)
    //     )
    // }

    // // Add a color to an existing average color
    // // without calculating the original sum
    // // prev_avg is the existing average color
    // // new_color is the new color you want to mix in
    // // n is the total number of colors, including self!
    // // [How to add and subtract values from an average?](https://math.stackexchange.com/questions/22348/how-to-add-and-subtract-values-from-an-average)
    // // TODO: after merge, use add2rms instead
    // pub fn add2avg(prev_avg_color: Color, new_color: Color, new_n: usize) -> Color {
    //     lerp(prev_avg_color, new_color, 1.0 / new_n as f64)
    // }
    
    // Transform [0, 1] f64 value (for Color type) to [0, 255] u8 (for ImageBuffer type)
    pub fn clamp_f64_to_u8(x: f64) -> u8 {
        let x = x * 255.0;
        if x <= 0.0 { // 2023.12.25: Do the first two branches ever happen?
            0u8
        } else if x >= 255.0 {
            255u8
        } else {
            x.round() as u8
        }
    }
    
    pub fn theoretical_color_to_practical_color(color: Color) -> Rgba<u8> {
        // Rust manipulates Color, but image drawer needs clamped Rgba<u8> i guess
        Rgba([
             Color::clamp_f64_to_u8(color.r),
             Color::clamp_f64_to_u8(color.g),
             Color::clamp_f64_to_u8(color.b),
             255 // full opacity
        ])
    }

    // Turn an Rgb object into Color
    // also scales colors to [0, 1]
    pub fn from_rgb_object<T>(rgb: &Rgb<T>) -> Color 
        where T: Copy + Into<f64>
    {
        Color::new(
            rgb.0[0].into() / 255.0,
            rgb.0[1].into() / 255.0,
            rgb.0[2].into() / 255.0,
        )
    }
}





//pub fn scale_and_clamp_color(color: Rgba<u8>, k: f64) -> Rgba<u8> {
    //Rgba([
         //clamp_f64_to_u8(color.0[0] as f64 * k), // r
         //clamp_f64_to_u8(color.0[1] as f64 * k), // g
         //clamp_f64_to_u8(color.0[2] as f64 * k), // b
         //255 // full opacity
    //])
//}

/* not needed
impl Vec3 {
    pub fn ly2rz(&self) -> Self {
        // Take a vector defined in a "left-handed, y-up" coord system,
        // and transform it into the equivalent "right-handed, z-up" coord system
        // This is an isomorphism, I think
        // To switch, just swap the y and z coords.
        // So, the inverse function is the same.
        Vec3::new(self.x, self.z, self.y)
    }

    pub fn rz2ly(&self) -> Self {
        // Take a vector defined in a "right-handed, z-up" coord system,
        // and transform it into the equivalent "left-handed, y-up" coord system
        Vec3::new(self.x, self.z, self.y)
    }
}
*/

fn main() {}

#[cfg(test)]
mod tests {
    use crate::core_math::avg2points;

    use super::*;

    #[test]
    fn avg2colors_test() {
        let c1: Color = Color::new(255.0 / 255.0, 0.0, 0.0);
        let c2: Color = Color::black();
        let avg_c: Color = avg2points(c1, c2);
        assert_eq!(
            avg_c,
            Color::new(/* 180.31222920256963 */ 0.5, 0.0, 0.0)
        );
    }
}