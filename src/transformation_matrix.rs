use crate::vec3::Vec3;
use crate::matrix::Mat;

#[derive(Debug, Copy, Clone)]
pub struct Mat3x3 {
    pub a11: f64, pub a12: f64, pub a13: f64,
    pub a21: f64, pub a22: f64, pub a23: f64,
    pub a31: f64, pub a32: f64, pub a33: f64
}

impl Mat3x3 {
    pub fn new(
        a11: f64, a12: f64, a13: f64,
        a21: f64, a22: f64, a23: f64,
        a31: f64, a32: f64, a33: f64
    ) -> Mat3x3 {
        Mat3x3 {
            a11, a12, a13,
            a21, a22, a23,
            a31, a32, a33
        }
    }

    pub fn mm_mul(a: Mat3x3, b: Mat3x3) -> Mat3x3 {
        Mat3x3::new(
            a.a11 * b.a11 + a.a12 * b.a21 + a.a13 * b.a31, // a11
            a.a11 * b.a12 + a.a12 * b.a22 + a.a13 * b.a32, // a12
            a.a11 * b.a13 + a.a12 * b.a23 + a.a13 * b.a33, // a13

            a.a21 * b.a11 + a.a22 * b.a21 + a.a23 * b.a31, // a21
            a.a21 * b.a12 + a.a22 * b.a22 + a.a23 * b.a32, // a22
            a.a21 * b.a13 + a.a22 * b.a23 + a.a23 * b.a33, // a23

            a.a31 * b.a11 + a.a32 * b.a21 + a.a33 * b.a31, // a31
            a.a31 * b.a12 + a.a32 * b.a22 + a.a33 * b.a32, // a32
            a.a31 * b.a13 + a.a32 * b.a23 + a.a33 * b.a33  // a33
        )
    }

    pub fn mv_mul(a: Mat3x3, v: Vec3) -> Vec3 {
        Vec3::new(
            a.a11 * v.x + a.a12 * v.y + a.a13 * v.z,
            a.a21 * v.x + a.a22 * v.y + a.a23 * v.z,
            a.a31 * v.x + a.a32 * v.y + a.a33 * v.z
        )
    }


}

/* 
what to do:
- [x] v.transform(mat)
- [x] mat.translate(x, y, z)
- [x] mat.scale(x, y, z)
- [x] mat.rotate(x, y, z)
- // shear
 */


impl Vec3 {
    // Transform a 3d vector by a 4x4 transformation matrix
    // The extra dimension is needed for translation.
    // But the output throws away the bottom dimension
    // The caching and inlining probably don't make it faster.
    // Time it and remove it if it doesn't do anything.
    pub fn transform(&self, mat: &Mat) -> Self {
        assert!(mat.nrows == 4 && mat.ncols == 4);
        // Vec3::new(
        //     self.x * mat[[0, 0]] + self.y * mat[[0, 1]] + self.z * mat[[0, 2]] + mat[[0, 3]],
        //     self.x * mat[[1, 0]] + self.y * mat[[1, 1]] + self.z * mat[[1, 2]] + mat[[1, 3]],
        //     self.x * mat[[2, 0]] + self.y * mat[[2, 1]] + self.z * mat[[2, 2]] + mat[[2, 3]]
        // )
        let x = self.x; let y = self.y; let z = self.z;
        Vec3::new(
            x * mat.entries[0] + y * mat.entries[1] + z * mat.entries[2] + mat.entries[3],
            x * mat.entries[4] + y * mat.entries[5] + z * mat.entries[6] + mat.entries[7],
            x * mat.entries[8] + y * mat.entries[9] + z * mat.entries[10] + mat.entries[11]
        )
    }

    // Transform a 3d vector by a 4x4 transformation matrix, without translating.
    // Basically you don't want to use the 4th column, which is in charge of translating the vector. 
    // So basically it is a 3x3 matrix.
    pub fn transform_dont_translate(&self, mat: &Mat) -> Self {
        assert!(mat.nrows == 4 && mat.ncols == 4);
        Vec3::new(
            self.x * mat[[0, 0]] + self.y * mat[[0, 1]] + self.z * mat[[0, 2]] /* + mat[[0, 3]] */,
            self.x * mat[[1, 0]] + self.y * mat[[1, 1]] + self.z * mat[[1, 2]] /* + mat[[1, 3]] */,
            self.x * mat[[2, 0]] + self.y * mat[[2, 1]] + self.z * mat[[2, 2]] /* + mat[[2, 3]] */
        )
    }
}

impl Mat {
    // x/y/z is how much to translate in the directions of
    // the x/y/z axes.
    // │ 1 0 0 x │
    // │ 0 1 0 y │
    // │ 0 0 1 z │
    // │ 0 0 0 1 │
    pub fn get_tranlation_mat(x: f64, y: f64, z: f64) -> Mat {
        Mat::new(4, 4, vec![
            1.0, 0.0, 0.0, x,
            0.0, 1.0, 0.0, y,
            0.0, 0.0, 1.0, z,
            0.0, 0.0, 0.0, 1.0
        ])
    }

    // x/y/z is how much to scale in the directions of
    // the x/y/z axes.
    // │ x 0 0 0 │
    // │ 0 y 0 0 │
    // │ 0 0 z 0 │
    // │ 0 0 0 1 │
    pub fn get_scaling_mat(x: f64, y: f64, z: f64) -> Mat {
        Mat::new(4, 4, vec![
              x, 0.0, 0.0, 0.0,
            0.0,   y, 0.0, 0.0,
            0.0, 0.0,   z, 0.0,
            0.0, 0.0, 0.0, 1.0
        ])
    }

    // yaw, pitch, roll 
    // Multiply vector by a rotation mat to apply the rotation.
    // for right handed coordinate systems.
    // For this right-handed, z-up, y-forward coord system,
    // - Rotating on the x axis is pitch, not roll
    // - Rotating on the y axis is roll, not pitch
    // - Rotating on the z axis is yaw
    // https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions

    pub fn get_yaw_mat(yaw: f64) -> Mat {
        Mat::new(4, 4, vec![
            f64::cos(yaw), -f64::sin(yaw), 0.0, 0.0,
            f64::sin(yaw),  f64::cos(yaw), 0.0, 0.0,
                      0.0,            0.0, 1.0, 0.0,
                      0.0,            0.0, 0.0, 1.0 
        ])
    }

    pub fn get_roll_mat(roll: f64) -> Mat {
        Mat::new(4, 4, vec![
             f64::cos(roll), 0.0, f64::sin(roll), 0.0,
                        0.0, 1.0,            0.0, 0.0,
            -f64::sin(roll), 0.0, f64::cos(roll), 0.0,
                        0.0, 0.0,            0.0, 1.0
        ])
    }

    pub fn get_pitch_mat(pitch: f64) -> Mat {
        Mat::new(4, 4, vec![
            1.0,             0.0,              0.0, 0.0,
            0.0, f64::cos(pitch), -f64::sin(pitch), 0.0,
            0.0, f64::sin(pitch),  f64::cos(pitch), 0.0,
            0.0,             0.0,              0.0, 1.0
        ])
    }

    pub fn get_rotation_mat(yaw: f64, pitch: f64, roll: f64) -> Mat {
        Mat::new(4, 4, vec![
            f64::cos(roll) * f64::cos(yaw),
            -f64::cos(pitch) * f64::sin(yaw) + f64::sin(pitch) * f64::sin(roll) * f64::cos(yaw),
            f64::sin(pitch) * f64::sin(yaw) + f64::cos(pitch) * f64::sin(roll) * f64::cos(yaw),
            0.0,

            f64::cos(roll) * f64::sin(yaw),
            f64::cos(pitch) * f64::cos(yaw) + f64::sin(pitch) * f64::sin(roll) * f64::sin(pitch),
            -f64::sin(pitch) * f64::cos(yaw) + f64::cos(pitch) * f64::sin(roll) * f64::sin(yaw),
            0.0,

            -f64::sin(roll),
            f64::sin(pitch) * f64::cos(roll),
            f64::cos(pitch) * f64::cos(roll),
            0.0,

            0.0, 0.0, 0.0, 1.0
        ])
    }

    // Take a transformation represented by a matrix,
    // And add a translation to the end of it.
    // Mat::identity(4).translate(x, y, z);
    // mat.translate(x, y, z)
    pub fn translate(&self, x: f64, y: f64, z: f64) -> Mat {
        self.mmul(&Mat::get_tranlation_mat(x, y, z))
    }

    // Take a transformation represented by a matrix,
    // And add a scaling to the end of it.
    // Mat::identity(4).scale(x, y, z);
    // mat.scale(x, y, z)
    pub fn scale(&self, x: f64, y: f64, z: f64) -> Mat {
        self.mmul(&Mat::get_scaling_mat(x, y, z))
    }

    // Take a transformation represented by a matrix,
    // And add a rotation to the end of it.
    // Mat::identity(4).rotate(yaw, pitch, roll);
    // mat.translate(yaw, pitch, roll)
    pub fn rotate(&self, yaw: f64, pitch: f64, roll: f64) -> Mat {
        self.mmul(&Mat::get_rotation_mat(yaw, pitch, roll))
    }

}

// Weak, lack coverage
#[cfg(test)]
mod tests {
    use super::*;
    use std::f64::consts::PI;

    #[test]
    fn translate() {
        assert!(
            Vec3::new(-3.0, 4.0, 5.0).transform(
                &Mat::identity(4)
                    .translate(5.0, -3.0, 2.0)
            )
            == Vec3::new(2.0, 1.0, 7.0)
        );

        assert!(
            Vec3::new(-3.0, 4.0, 5.0).transform(
                &Mat::identity(4)
                    .translate(5.0, -3.0, 2.0)
                    .inverse().unwrap()
            )
            == Vec3::new(-8.0, 7.0, 3.0)
        );
    }

    #[test]
    fn scale() {
        assert!(
            Vec3::new(-4.0, 6.0, 8.0).transform(
                &Mat::identity(4)
                    .scale(2.0, 3.0, 4.0)
            )
            == Vec3::new(-8.0, 18.0, 32.0)
        );

        assert!(
            Vec3::new(-4.0, 6.0, 8.0).transform(
                &Mat::identity(4)
                    .scale(2.0, 3.0, 4.0)
                    .inverse().unwrap()
            )
            == Vec3::new(-2.0, 2.0, 2.0)
        );
    }

    #[test]
    fn rotate() {
        let v: Vec3 = Vec3::new(0.0, 1.0, 0.0);
        let half_quarter: Mat = Mat::get_pitch_mat(PI / 4.0);
        let full_quarter: Mat = Mat::get_pitch_mat(PI / 2.0);
        assert!(
            v.transform(&half_quarter) == Vec3::new(0.0, f64::sqrt(2.0) / 2.0, f64::sqrt(2.0) / 2.0)
        );
        assert!(
            v.transform(&full_quarter) == Vec3::new(0.0, 0.0, 1.0)
        );
        assert!(
            v.transform(&half_quarter.inverse().unwrap()) == Vec3::new(0.0, f64::sqrt(2.0) / 2.0, -f64::sqrt(2.0) / 2.0)
        );
    }
}