use std::f64::consts::TAU;
use rand::{distributions::{Distribution, Standard}, rngs::ThreadRng, Rng};
use rand_distr::StandardNormal;
use image::{ImageBuffer, Rgba};

use vecspace::VectorSpace;
use crate::{color::Color, vec3::Vec3};

const WIDTH: u32 = 300;
const HEIGHT: u32 = 300;
const NSAMPLES: u32 = 100;
const COLOR_DISTANCE: f64 = 0.001;

fn topleft_colrow_to_centered_xz(col: u32, row: u32) -> (f64, f64) {
    let centered_x: f64 = (col as f64) - (WIDTH as f64) / 2.0;
    let centered_z: f64 = -1.0 * (row as f64) + (HEIGHT as f64) / 2.0;
    (centered_x, centered_z)
}

// refactor image layer of abstraction, then put this func there
fn color_vec_to_image_buffer_data(v: &Vec<Color>) -> Vec<u8> {
    let mut data: Vec<u8> = Vec::with_capacity(4 * v.len());
    for c in v.iter() {
        data.push(Color::clamp_f64_to_u8(c.r));
        data.push(Color::clamp_f64_to_u8(c.g));
        data.push(Color::clamp_f64_to_u8(c.b));
        data.push(255);
    }
    data
}

fn sample_unit_circle(rng: &mut ThreadRng) -> (f64, f64) {
    let theta: f64 = TAU * rng.gen::<f64>();
    (theta.cos(), theta.sin())
}

// maybe make part of Vec3 impl
fn sample_unit_sphere(rng: &mut ThreadRng) -> Vec3 {
    Vec3::new(
        rng.sample(StandardNormal),
        rng.sample(StandardNormal),
        rng.sample(StandardNormal),
    ).normalize()
}

// Get a vector in the unit hemisphere, centered on n
// n: Unit vector that the hemisphere is centered on.
// Every vector in the hemisphere has an equal chance of being chosen.
fn sample_unit_hemisphere_uniform(n: Vec3, rng: &mut ThreadRng) -> Vec3 {
    let v = sample_unit_sphere(rng);
    if v * n > 0.0 {
        return v;
    }
    return -v;
}

// Idk if i should put these in color.rs bc maybe Vec3 shouldnt be a dependency of color
impl std::ops::Add<Vec3> for Color {
    type Output = Color;
    fn add(self, other: Vec3) -> Color {
        Color::new(self.r + other.x, self.g + other.y, self.b + other.z)
    }
}

impl std::ops::AddAssign<Vec3> for Color {
    fn add_assign(&mut self, other: Vec3) {
        *self = Self {
            r: self.r + other.x,
            g: self.g + other.y,
            b: self.b + other.z
        }
    }
}

impl Distribution<Color> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Color {
        let (r, g, b) = rng.gen();
        Color::new(r, g, b)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn color_distance_test() {
        let mut rng = rand::thread_rng();

        let mut color_of_pixel: Color = rng.gen();
        
        let mut color_data = Vec::with_capacity((WIDTH * HEIGHT) as usize);
        for _ in 0..(WIDTH * HEIGHT) {
            color_data.push(color_of_pixel);
            let color_nudge = sample_unit_sphere(&mut rng) * COLOR_DISTANCE;
            color_of_pixel += color_nudge;
        }
        
        let data: Vec<u8> = color_vec_to_image_buffer_data(&color_data);
        let imgbuf: ImageBuffer<Rgba<u8>, Vec<u8>> = ImageBuffer::from_raw(WIDTH, HEIGHT, data).unwrap();
        //data = imgbuf.into_raw();
        imgbuf.save("out.png").unwrap();
    }
}