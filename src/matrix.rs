/*
 * what to do:
 * - [x] equality
 * - [x] add, sub, scalar multiplication
 * - [x] matmul
 * - [x] get i
 * - [x] transpose
 * - [x] rref
 *     - [x] row ops
 * - [x] determinant
 * - [x] invert
 * - [x] multiply vec
*/

// add tests for rref, inverse, etc
use std::fmt;
use std::ops::{Index, IndexMut, Add, Sub, Range};

use crate::core_math::feq;
use crate::vec3::Vec3; // only thing that needs Vec3 is vmul

#[derive(Debug, Clone)]
pub struct Mat {
    pub nrows: usize,
    pub ncols: usize,
    pub entries: Vec<f64>
}

impl Mat {
    pub fn new(nrows: usize, ncols: usize, entries: Vec<f64>) -> Mat {
        assert_eq!(
            nrows * ncols, entries.len(),
            "number of rows and columns doesn't match number of entries"
        );
        
        Mat { nrows, ncols, entries }
    }

    // Get the dimensions/shape of a mat. (nrows, ncols)
    pub fn shape(&self) -> (usize, usize) {
        (self.nrows, self.ncols)
    }

    pub fn same_shape(&self, other: &Self) -> bool {
        return self.nrows == other.nrows && self.ncols == other.ncols;
    }
    pub fn is_square(&self) -> bool {
        return self.nrows == self.ncols;
    }
    //pub fn iter(self) -> Iter<'_, f64> {
        //self.entries.iter()
    //}

    pub fn scalar_mul(&self, c: f64) -> Self {
        let mut scaled_entries: Vec<f64> = Vec::with_capacity(self.entries.len());
        for a in self.entries.iter() {
            scaled_entries.push(c * a);
        }
        Mat::new(self.nrows, self.ncols, scaled_entries)
    }

    // `A.mmul(B)` is `BA`
    // Start with `A` and multiply `b` from the *left*.
    // To do `ABC`, do `C.mmul(B).mmul(A)`
    pub fn mmul(&self, other: &Self) -> Self {
        assert_eq!(other.ncols, self.nrows,
            "Amount of columns of left mat must match amount of rows of right mat");
        let mut product_entries: Vec<f64> = Vec::with_capacity(other.nrows * self.ncols);
        // The product mat will have the same amount of rows as `other`
        // and the same amount of cols as `self`.
        for other_row_i in 0..other.nrows {
            for self_col_j in 0..self.ncols {
                // The entry of the product at the ith row and jth col
                // is the dot product of the ith row of `other` and the jth col of `self`.
                let mut dot_product: f64 = 0.0;
                for k in 0..other.ncols {
                    dot_product +=
                          other[[other_row_i, k]]   // kth item of ith row of other. other.entries[i * other.ncols + k]
                        * self[[k, self_col_j]]; // kth item of jth col of self. other.entries[k * other.ncols + j]
                }
                product_entries.push(dot_product);
            }
        }
        Mat::new(other.nrows, self.ncols, product_entries)
    }

    // Get identity matrix of size n.
    pub fn identity(n: usize) -> Mat {
        let mut i_entries: Vec<f64> = vec![0.0; n * n];
        for i in 0..n {
            i_entries[i * n + i] = 1.0;
        }
        Mat::new(n, n, i_entries)
    }

    // Get a nxn matrix of all zeroes.
    pub fn zeroes(n: usize) -> Mat {
        Mat::new(n, n, vec![0.0; n * n])
    }

    // Get the transpose of a matrix.
    pub fn transpose(&self) -> Mat {
        let mut t_entries: Vec<f64> = Vec::with_capacity(self.entries.len());
        // a^T_{i, j} = a_{j, i}
        for t_i in 0..self.ncols {
            for t_j in 0..self.nrows {
                t_entries.push(self[[t_j, t_i]]);
            }
        }
        Mat::new(self.ncols, self.nrows, t_entries)
    }

    // Switch rows `r1` and `r2` in place.
    pub fn ero_switch_mut(&mut self, r1: usize, r2: usize) {
        assert!(r1 < self.nrows && r2 < self.nrows);
        for j in 0..self.ncols {
            (self[[r1, j]], self[[r2, j]]) = (self[[r2, j]], self[[r1, j]]);
        }
    }

    // Scale row `r` by `c` in place.
    pub fn ero_scale_mut(&mut self, r: usize, c: f64) {
        assert!(r < self.nrows,
            "for {}, r={} must be less than nrows={}", self, r, self.nrows);
        assert!(!feq(c, 0.0), "scaling a row by 0 changes the mat's solutions");
        for j in 0..self.ncols {
            self[[r, j]] *= c;
        }
    }

    // Replace row `r1` with `r1 + c * r2`
    pub fn ero_add_mut(&mut self, r1: usize, c: f64, r2: usize) {
        assert!(r1 < self.nrows && r2 < self.nrows);
        for j in 0..self.ncols {
            self[[r1, j]] += c * self[[r2, j]];
        }
    }

    // Get the determinant of a matrix.
    // First get it into row echelon form, then multiply the main diagonal.
    pub fn det(&self) -> f64 {
        // The determinant is only defined for square matricies.
        assert!(self.is_square(), "{} is not square", self);
        // The determinant of a 2d matrix is `ad - bc`.
        if self.nrows == 2 {
            return self[[0, 0]] * self[[1, 1]] - self[[0, 1]] * self[[1, 0]];
        } 
        // If the matrix is bigger than 2x2:
        // Get it into row eschelon form.
        let mut m: Mat = self.clone();
        let mut positive_sign: bool = true;
        // Go through each column
        for j in 0..m.ncols {
            // First, get a pivot.
            // Look at all of the entries that are in the jth column and are at or below the jth row.  
            // Find a non-zero entry and put it in the jth row (if it isn't there already).
            // If that area has all zeroes, mission accomplished, move to the next column.
            let pivot: Option<f64> = m.get_pivot_4det(&mut positive_sign, j);

            match pivot {
                Some(_) => {
                    // There is a pivot in the area.
                    // It is in the jth col *and the jth row*.
                    // Make all zeroes below it
                    m.make_all_zeroes_below_pivot_4det(j);
                }
                None => {
                    // There are all zeroes in the area.
                    // I don't need to do anything, move to the next col.
                    continue;
                }
            }
        }
        // It should be in REF now.

        // Multiply the diagonals
        let mut diagonal_product: f64 = 1.0;
        for i in 0..m.nrows {
            diagonal_product *= m[[i, i]];
        }
        // Remember to add the sign
        let sign: f64 = {
            if positive_sign {1.0} else {-1.0}
        };
        return sign * diagonal_product;
    }

    // Find a nonzero entry of the jth column at the jth row or below.  
    // Move it to the jth row if it isn't there already.
    // Helper function for det().
    fn get_pivot_4det(&mut self, sign_var: &mut bool, j: usize) -> Option<f64> {
        for i in j..self.nrows {
            if !feq(self[[i, j]], 0.0) {
                if i != j {
                    self.ero_switch_mut(i, j);
                    *sign_var = !*sign_var;
                }
                return Some(self[[j, j]]);
            }
        }
        return None;
    }

    fn make_all_zeroes_below_pivot_4det(&mut self, j: usize) {
        for i in (j + 1)..self.nrows { // I think j + 1 will have out of bounds error for the last column. however 5..2 just doesnt do anything so i think we'll be fine.
            // No ero is needed if a_{i, j} is already 0;
            if feq(self[[i, j]], 0.0) {
                continue;
            }
            // r_i = r_i - (a_{i, j} / a_{j, j}) * r_j
            self.ero_add_mut(i, -self[[i, j]] / self[[j, j]], j);
        }
    }

    // Put a matrix into its reduced row eschelon form.
    // - Pivot: First nonzero entry in a row.
    // - All pivots are 1.
    // - All pivots are the only nonzero entry in their column.
    // - All pivots are to the right of the ones above them.
    // - All rows of all zeroes are at the bottom.
    // algorithm: https://www.math.purdue.edu/~shao92/documents/Algorithm%20REF.pdf
    pub fn rref2(&mut self) {
        let mut pivot_coords: Vec<(usize, usize)> = Vec::with_capacity(self.ncols);
        for i in 0..self.nrows {
            // Get the topleftmost nonzero entry from the ith row to the last row.
            let topleftmost_nonzero_entry_ij: Option<(usize, usize)> = self.get_topleftmost_nonzero_entry_ij(i); // step 2
            
            if topleftmost_nonzero_entry_ij.is_none() {
                if i == 0 {
                    // If the mat is all zeroes, it is already in rref.
                    return;
                }
                // If the rows from i to nrows - 1 are all zero,
                // It is in REF.
                break;
            }

            let (pivot_i, pivot_j) = topleftmost_nonzero_entry_ij.unwrap();
            self.put_1_in_ppos(pivot_i, pivot_j, i); // step 3. "topmost" row = i I think
            self.make_all_zeroes_above_or_below_pivot_4rref(i, pivot_j, false); // step 4
            pivot_coords.push((i, pivot_j));
        }
        // Should be in REF now.
        // println!("{:?}", pivot_coords);
        // If the number of pivot columns is equal to the number of columns
        // aka if there is a pivot in every column, then there are no free vars
        // so mat is row equiv to a mat with 1 on main diag and 0 everywhere else
        // aka I with zero rows on the bottom
        // the main diagonal already has 1s and every entry below it is already 0,
        // just need to zero out all entries above the main diag.
        if pivot_coords.len() == self.ncols {
            self.zero_out_above_main_diag();
            return;
        }
        // Otherwise, I need to do it the old fashioned way.
        // println!("336 self={}", self);
        // Go from right to left
        for &(i, j) in pivot_coords.iter().rev() {
            self.make_all_zeroes_above_or_below_pivot_4rref(i, j, true);
        }
        // Should be in rref now
    }

    // Finds the first nonzero entry of a submat going left to right, then top to bottom.
    // If the mat is all zeroes, return None.
    // 
    // Go col by col, from 0 to ncols - 1.
    // In each col, go row by row, from start_i to nrows - 1.
    // 
    // Step 2 for rref2().
    fn get_topleftmost_nonzero_entry_ij(&self, start_i: usize) -> Option<(usize, usize)> {
        for j in 0..self.ncols {
            for i in start_i..self.nrows {
                if !feq(self[[i, j]], 0.0) {
                    return Some((i, j));
                }
            }
        }
        return None;
    }

    // Use a_i,j to put 1 in the pivot position.
    // topmost_i is the row that 1 should be in.
    // pivot position is a_{topmost_i, j}.
    // Step 3 for rref2().
    fn put_1_in_ppos(&mut self, i: usize, j: usize, topmost_i: usize) {
        // switch if needed
        if i != topmost_i {
            self.ero_switch_mut(i, topmost_i);
        }
        // scale if needed
        if !feq(self[[topmost_i, j]], 1.0) {
            self.ero_scale_mut(topmost_i, 1.0 / self[[topmost_i, j]]);
        }
    }

    // a_{pivot_i, pivot_j} = 1
    // This will be used to zero out all entries above/below it, in col pivot_j.
    // │ 1 2 │ r2 = r2 - 3 r1 │ 1  2 │
    // │ 3 4 │       ->       │ 0 -2 │
    // Helper function for rref2().
    // Same as make_all_zeroes_below_pivot_4det but denominator of c is always 1.
    // I'm keeping them separate for now in case I need to make changes to them.
    // above: if true, zero out above. if false, zero out below.
    fn make_all_zeroes_above_or_below_pivot_4rref(&mut self, pivot_i: usize, pivot_j: usize, above: bool) {
        assert!(feq(self[[pivot_i, pivot_j]], 1.0),
            "for self={}, the entry at pivot_i={} and pivot_j={} should be 1, not self[[pivot_i, pivot_j]]={}",
            self, pivot_i, pivot_j, self[[pivot_i, pivot_j]]);
        
        let range: Range<usize> = {
            if above {
                0..pivot_i // done in reverse but it doesn't matter
            } else {
                (pivot_i + 1)..self.nrows
            }
        };

        for i in range {
            if !feq(self[[i, pivot_j]], 0.0) {
                self.ero_add_mut(i, -self[[i, pivot_j]], pivot_i); // r_i = r_i - a_{i, j} * r_{pivot_i}
            }
        }
    }

    // Zero out all entries above the main diagonal.
    // │ a b c │    │ a 0 0 │
    // │ d e f │ -> │ d e 0 │
    // │ g h i │    │ g h i │
    fn zero_out_above_main_diag(&mut self) {
        for j in 0..self.ncols {
            for i in 0..j { // i < j
                self[[i, j]] = 0.0;
            }
        }
    }

    // Concatenate two matricies horizontally.
    // │ a b │ concat │ e f │ is │ a b e f │
    // │ c d │        │ g h │    │ c d g h │
    // Must have same number of rows.
    pub fn concat(m1: &Mat, m2: &Mat) -> Mat {
        assert_eq!(m1.nrows, m2.nrows);
        let result_ncols: usize = m1.ncols + m2.ncols;
        let mut result_entries: Vec<f64> = Vec::with_capacity(result_ncols * m1.nrows);
        for i in 0..m1.nrows {
            for j in 0..m1.ncols {
                result_entries.push(m1[[i, j]]);
            }
            for j in 0..m2.ncols {
                result_entries.push(m2[[i, j]]);
            }
        }
        Mat::new(m1.nrows, result_ncols, result_entries)
    }

    // Split two matricies horizontally.
    // │ a b e f │ split before │ a b │ and │ e f │ 
    // │ c d g h │ col 2 makes  │ c d │     │ g h │
    // pub fn split(&self, j: usize) {}

    // For inverting mats. Get just the 2nd half, which is the inverse of a mat.
    // │ 1 0 0 0.4285714285714284 -0.42857142857142866 0.5714285714285714 │
    // │ 0 1 0 0.1428571428571429 -0.14285714285714285 -0.14285714285714285 │
    // │ 0 0 1 0.2857142857142857 0.047619047619047616 0.047619047619047616 │
    // ->
    // │ 0.4285714285714284 -0.42857142857142866 0.5714285714285714 │
    // │ .1428571428571429 -0.14285714285714285 -0.14285714285714285 │
    // │ 0.2857142857142857 0.047619047619047616 0.047619047619047616 │
    fn get_2nd_half(&self) -> Mat {
        let new_ncols = self.ncols / 2;
        let mut e: Vec<f64> = Vec::with_capacity(self.nrows * new_ncols);
        for i in 0..self.nrows {
            for j in new_ncols..self.ncols {
                e.push(self[[i, j]]);
            }
        }
        Mat::new(self.nrows, new_ncols, e)
    }

    // Get the inverse of a mat.
    pub fn inverse(&self) -> Option<Mat> {
        assert!(self.is_square(), "Only square mats can be invertible.");
        if feq(self.det(), 0.0) {
            return None;
        }
        let mut self_and_i: Mat = Mat::concat(self, &Mat::identity(self.nrows));
        self_and_i.rref2(); // turns into "i and self^-1"
        return Some(self_and_i.get_2nd_half());
    }

    // Multiply a 3x3 matrix to a 3D vector.
    pub fn vmul(&self, v: Vec3) -> Vec3 {
        assert!(self.nrows == 3 && self.ncols == 3);
        Vec3::new(
            v.x * self[[0, 0]] + v.y * self[[0, 1]] + v.z * self[[0, 2]],
            v.x * self[[1, 0]] + v.y * self[[1, 1]] + v.z * self[[1, 2]],
            v.x * self[[2, 0]] + v.y * self[[2, 1]] + v.z * self[[2, 2]]
        )

    }
}

// Try to truncate to 4 decimal places.
impl fmt::Display for Mat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut mat_str: String = String::from("\n");
        for i in 0..self.nrows {
            let mut row_str: String = String::from("│ ");
            for j in 0..self.ncols {
                row_str.push_str(&(self[[i, j]].to_string() + " "));
            }
            row_str.push_str("│\n");
            mat_str.push_str(&row_str);
        }
        //mat_str.push_str("\n");
        write!(f, "{}", mat_str)
        //write!(f, "hello")
    }
}

impl Index<[usize; 2]> for Mat {
    // Get value at row-th row and col-th column
    // zero-indexed
    type Output = f64;
    fn index<'a>(&'a self, [i, j]: [usize; 2]) -> &'a f64 {
        &self.entries[i * self.ncols + j]
    }
}

impl IndexMut<[usize; 2]> for Mat {
    fn index_mut<'a>(&'a mut self, [i, j]: [usize; 2]) -> &'a mut f64 {
        &mut self.entries[i * self.ncols + j]
    }
}

impl PartialEq for Mat {
    fn eq(&self, other: &Mat) -> bool {
        for (s_entry, o_entry) in self.entries.iter().zip(other.entries.iter()) {
            if !feq(*s_entry, *o_entry) {
                return false;
            }
        }
        return true;
    }
}

impl Eq for Mat {}

// do i need lifetimes for this?
impl<'a> Add<&'a Mat> for &'a Mat {
    type Output = Mat;
    fn add(self, other: &'a Mat) -> Mat {
        assert!(self.same_shape(other), "{} is size {:?} but {} is size {:?}", self, self.shape(), other, other.shape());
        let mut sum_entries: Vec<f64> = Vec::with_capacity(self.entries.len());
        for (s, o) in self.entries.iter().zip(other.entries.iter()) {
            sum_entries.push(s + o);
        }
        Mat::new(self.nrows, self.ncols, sum_entries)
    }
}

impl<'a> Sub<&'a Mat> for &'a Mat {
    type Output = Mat;
    fn sub(self, other: &'a Mat) -> Mat {
        assert!(self.same_shape(other), "{} is size {:?} but {} is size {:?}", self, self.shape(), other, other.shape());
        let mut diff_entries: Vec<f64> = Vec::with_capacity(self.entries.len());
        for (s, o) in self.entries.iter().zip(other.entries.iter()) {
            diff_entries.push(s - o);
        }
        Mat::new(self.nrows, self.ncols, diff_entries)
    }
}
            
fn main() {
    /* should use this when making tests
    let m1: Mat = Mat { nrows: 2, ncols: 2, entries: vec![1.0, 2.0, 3.0, 4.0] };
    let m2: Mat = Mat { nrows: 2, ncols: 2, entries: vec![1.0, 2.0, 3.0, -4.0] };
    let m3: Mat = Mat::new(2, 4, vec![
                           3.0, 2.0, 1.0, 5.0,
                           9.0, 1.0, 3.0, 0.0]);
    let mut m4: Mat = Mat::new(4, 3, vec![
                           2.0, 9.0, 0.0,
                           1.0, 3.0, 5.0,
                           2.0, 4.0, 7.0,
                           8.0, 1.0, 5.0]);
    println!("m1 == m2? {}", m1 == m2);
    println!("m1: {} m2: {}", m1, m2);
    println!("m1 + m2: {}", &m1 + &m2);
    println!("m1 - m2: {}", &m1 - &m2);
    println!("-2 * m1: {}", (&m1).scale(-2.0));
    println!("{} * {}: {}", m3, m4, (&m4).mmul(&m3));
    assert!((&m4).mmul(&m3) == Mat::new(2, 3, vec![
                                        50.0, 42.0, 42.0,
                                        25.0, 96.0, 26.0]));
    println!("I_5: {}", Mat::identity(5));
    println!("m4.transpose(): {}", m4.transpose());

    println!("m4: {}", m4);
    m4.ero_switch_mut(0, 1);
    println!("then switch 0th and 1st rows: {}", m4);
    m4.ero_scale_mut(3, -3.7);
    println!("then scale 3rd row by -3.7: {}", m4);
    m4.ero_add_mut(1, -2.0, 0);
    println!("then replace 1st row with 1st row plus -2 * 0th row: {}", m4);
    m4.ero_scale_mut(0, 0.5);
    m4.ero_add_mut(1, -1.0, 0);
    m4.ero_add_mut(2, -2.0, 0);
    m4.ero_add_mut(3, -8.0, 0);
    m4.ero_scale_mut(1, -2.0 / 3.0);
    m4.ero_add_mut(2, 5.0, 1);
    m4.ero_add_mut(3, 35.0, 1);
    m4.ero_scale_mut(2, 1.0 / m4[[2, 2]]);
    m4.ero_add_mut(3, -m4[[3, 2]], 2);
    
    println!("{}", m4);
    let mut m5: Mat = Mat::new(3, 3, vec![
        9.0, 9.0, 3.0,
        4.0, 4.0, 7.0,
        6.0, 6.0, 5.0]);
    println!("{}", m5.det());

    

    let mut m1: Mat = Mat::new(2, 2, vec![
        1.0, 1.0,
        2.0, 1.0
    ]);

    // assert_eq!(
    //     m1,
    //     Mat::identity(2)
    // );
    let mut m2: Mat = Mat::new(3, 3, vec![
        1.0, 0.0, 2.0,
        1.0, 1.0, 2.0,
        2.0, 1.0, 6.0
    ]);

    // assert_eq!(
    //     m2,
    //     Mat::identity(3)
    // );
    let mut m3: Mat = Mat::new(2, 5, vec![
        1.0, 1.0, 2.0, 2.0, 3.0,
        4.0, 4.0, 8.0, 7.0, 3.0
    ]);

    let mut m4: Mat = Mat::new(6, 3, vec![
        1.0, 0.0, 1.0,
        1.0, 1.0, 1.0,
        2.0, 1.0, 2.0,
        5.0, 0.0, 5.0,
        6.0, 6.0, 6.0,
        7.0, 5.0, 7.0
    ]);

    let mut m5: Mat = Mat::new(2, 4, vec![
        1.0, 3.0, 2.0, 1.0,
        2.0, -3.0, 0.0, -2.0
    ]);

    let mut m6: Mat = Mat::new(3, 4, vec![
        -1.0, 1.0, 0.0, 1.0,
        -2.0, -3.0, -1.0, -2.0,
        -3.0, -1.0, -2.0, -1.0
    ]);

    let mut m7: Mat = Mat::new(3, 3, vec![
        0.0, 1.0, 3.0,
        -1.0, -3.0, 3.0,
        1.0, -3.0, 0.0
    ]);

    let mut m8: Mat = Mat::new(3, 3, vec![
        0.0, 1.0, 2.0,
        0.0, 3.0, 4.0,
        0.0, 5.0, 6.0,
    ]);

    let mut m9: Mat = Mat::new(3, 3, vec![
        2.0, 0.0, -2.0,
        -2.0, 0.0, -2.0,
        -1.0, 0.0, 3.0
    ]);
    
    for mut m in [&mut m1, &mut m2, &mut m3, &mut m4, &mut m5, &mut m6, &mut m7, &mut m8, &mut m9] {
        println!("from {} ...", m);
        m.rref2();
        println!("... to {}", m);
    }

    // assert_eq!(
    //     m3,
    //     Mat::new(2, 5, vec![
    //         1.0, 1.0, 2.0, 0.0, -15.0,
    //         0.0, 0.0, 0.0, 1.0, 9.0
    //     ])
    // );

    let m7_inv: Mat = m7.inverse().unwrap();
    // println!("A A^-1 = {}", m7_inv.mmul(&m7));
    println!("A^-1 A = {}", m7.mmul(&m7_inv));

    println!("{}", m2);
    m2.ero_switch_mut(0, 1);
    println!("{}", m2);
    */
}

#[cfg(test)]
mod tests {
    use super::*;
    // Determinants from worksheet.
    #[test]
    fn det() {
        let m1: Mat = Mat::new(2, 2, vec![
            5.0, -2.0,
            3.0, -1.0
        ]);
        assert!(feq(m1.det(), 1.0));
        let m2: Mat = Mat::new(2, 2, vec![
            5.0, 10.0,
            -3.0, -6.0
        ]);
        assert!(feq(m2.det(), 0.0));
        let m3: Mat = Mat::new(3, 3, vec![
            1.0, 0.0, 1.0,
            2.0, 0.0, -3.0,
            4.0, -5.0, 11.0
        ]);
        assert!(feq(m3.det(), -25.0));
        let m4: Mat = Mat::new(3, 3, vec![
            4.0, -1.0, 0.0,
            2.0, 0.0, -3.0,
            4.0, -5.0, 11.0
        ]);
        assert!(feq(m4.det(), -26.0));
        let m5: Mat = Mat::new(3, 3, vec![
            1.0, 2.0, 3.0,
            4.0, 5.0, 6.0,
            7.0, 8.0, 9.0
        ]);
        assert!(feq(m5.det(), 0.0));
        let m6: Mat = Mat::new(3, 3, vec![
            0.0, 1.0, 0.0,
            1.0, 0.0, 1.0,
            4.0, 4.0, 3.0
        ]);
        assert!(feq(m6.det(), 1.0));
        let m7: Mat = Mat::new(4, 4, vec![
            3.0, 0.0, 1.0, 14.0,
            0.0, -3.0, 0.0, -3.0,
            0.0, 0.0, -5.0, 11.0,
            0.0, 0.0, 0.0, 2.0
        ]);
        assert!(feq(m7.det(), 90.0));
        let m8: Mat = Mat::new(5, 5, vec![
            0.0, 0.0, 0.0, 0.0, 10.0,
            0.0, 0.0, 0.0, 2.0, 0.0,
            0.0, 0.0, 3.0, 0.0, 0.0,
            0.0, -2.0, 0.0, 0.0, 0.0,
            -5.0, 0.0, 0.0, 0.0, 0.0
        ]);
        assert!(feq(m8.det(), 600.0));
    }

    #[test]
    fn concat() {
        assert_eq!(
            Mat::concat(
                &Mat::new(2, 2, vec![1.0, 2.0, 3.0, 4.0]),
                &Mat::new(2, 2, vec![5.0, 6.0, 7.0, 8.0]),
            ),
            Mat::new(2, 4, vec![1.0, 2.0, 5.0, 6.0, 3.0, 4.0, 7.0, 8.0])
        );
    }

    // #[test]
    // fn rref() {
        
    // }

}