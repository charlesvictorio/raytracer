use std::f64::consts::{PI, TAU, FRAC_1_PI, FRAC_1_SQRT_2};
use crate::core_math::{QuadraticSolutions, quadratic_formula};
use crate::vec3::Vec3;
use vecspace::VectorSpace;
use crate::color::Color;
use crate::matrix::Mat;
use crate::ray::Ray;
use crate::object::{Object, Intersection};
use crate::material::Material;
use crate::aabb::Aabb;

#[derive(Debug, Clone)]
// pub struct ParaSurf<F> {
//     pub r: F, // r(u, v) defines parametric surface
//     pub ru: F, // derivative of r wrt u
//     pub rv: F, // derivative of r wrt v

pub struct ParaSurf {
    pub r: fn(f64, f64) -> Vec3, // r(u, v) defines parametric surface
    pub ru: fn(f64, f64) -> Vec3, // derivative of r wrt u
    pub rv: fn(f64, f64) -> Vec3, // derivative of r wrt v

    // pub o2w: Mat,
    // pub w2o: Mat,
    pub material: usize,
}

impl/*<F>*/ Object for ParaSurf/*<F>*/
/*where 
    F: Fn(f64, f64) -> Vec3 */
{
    fn get_aabb(&self) -> Aabb {
        return Aabb::default(); // placeholder
        // let aabb_in_os = Aabb::new(
        //     Vec3::new(-1.0, -1.0, -1.0),
        //     Vec3::new(1.0, 1.0, 1.0)
        // );
        // let aabb = aabb_in_os.transform(&self.o2w);
        // return aabb;
    }
    fn get_centroid(&self) -> Vec3 {
        return (self.r)(0.0, 0.0); // placeholder
        // Vec3::zero().transform(&self.o2w)
    }
    fn is_hit_by(&self, ray: &Ray) -> Option<f64> {
        self.intersection(ray).map(|intr| intr.t)
    }

    fn intersection(&self, ray: &Ray) -> Option<Intersection> {
        // guess stores the best guess so far of where the actual ray-ps intr is
        let mut guess: PointOnObj = self.get_initial_guess(ray); // initial guess point // maybe this should return intersection (but cant have )
        
        let mut u = 0.0; // change later
        let mut v = 0.0;
        let threshold = 1.0;
        let mut iterations = 0;
        loop {
            let t = ((guess.p - ray.start) * guess.norm) / (ray.dir * guess.norm);
            let q = ray.plug_in_t(t); // intr of ray and tan plane
            
            let q_minus_p = q - guess.p;
            let q_minus_p_dot_rv = q_minus_p * guess.dpdv;
            let q_minus_p_dot_ru = q_minus_p * guess.dpdu;
            let ru_ru = guess.dpdu * guess.dpdu;
            let ru_rv = guess.dpdu * guess.dpdv;
            let rv_rv = guess.dpdv * guess.dpdv;
            // Cramer's rule
            let one_over_det_a = 1.0 / (ru_ru * rv_rv - 2.0 * ru_rv);
            let det_a1 = q_minus_p_dot_ru * rv_rv - ru_rv * q_minus_p_dot_rv;
            let det_a2 = ru_ru * q_minus_p_dot_rv - q_minus_p_dot_ru * ru_rv;
            let du = det_a1 * one_over_det_a;
            let dv = det_a2 * one_over_det_a;

            if du * du + dv * dv < threshold * threshold {
                return Some(Intersection {
                    p: (self.r)(u, v),
                    uv: (u, v),
                    norm: self.norm_from_uv(u, v),
                    wo: -ray.dir.normalize(),
                    obj: self,
                    t
                });
            }
            if iterations > 200 {
                return None;
                // panic!("didn't converge");
            }

            u += du;
            v += dv;
            guess = self.get_point_on_obj_from_uv(u, v); // rearrange. maybe get_initial_uv
            iterations += 1;
        }
    }



    fn get_material_i(&self) -> usize {
        self.material
    }
    
    // Convert a point on the object in world space to a (u, v) coordinate in texture space.
    // 1. Convert point to object space.
    // 2. Convert that point into spherical coordinates.
    //    - rho always = 1 on the surface
    //    - theta = atan(y / x). actually use y.atan2(x), which goes between -pi and pi
    //    - phi = acos(z). goes from 0 to pi
    // 3. Convert (theta, phi) to (u, v)
    //    - u and v are within [0, 1]
    //    - u goes from 0 to 1 as you go CCW around the sphere.
    //    - u = 0 at P = <-1.0, -EPSILON, 0.0>, u = 1 at P = <-1.0, 0.0, 0.0>
    //    - v goes from 0 to 1 as it goes from south pole to north pole
    // In the ray tracer challenge, theta = x.atan2(y) and u = -(1.0 / TAU) * theta + 0.5, which is the wrong way.
    // This results in u = 0 being at -zhat instead of <-1.0, -EPSILON, 0.0>.
    fn uv_at(&self, point: Vec3) -> (f64, f64) {
        todo!();
        // let point_os = point.transform(&self.w2o);
        // let theta = point_os.y.atan2(point_os.x);
        // let phi = point_os.z.acos();
        // let u = (1.0 / TAU) * theta + 0.5;
        // let v = -FRAC_1_PI * phi + 1.0;
        // return (u, v);
    }

    fn norm_at(&self, point: Vec3) -> Vec3 {
        todo!();
        // Convert point from world space to object space. 
        // The vector from the origin to that point (in object space) is the direction of the normal vector.
        // let normal_dir_os: Vec3 = point.transform(&self.w2o);
        // // Convert that vector to world space.
        // // [Why multiply the normal vector by the inverse transpose?](https://paroj.github.io/gltut/Illumination/Tut09%20Normal%20Transformation.html)
        // // Translation wont affect converting the norm vec to ws
        // let normal_dir_ws: Vec3 = normal_dir_os.transform/*_dont_translate*/(
        //     &self.w2o
        //         .transpose()
        // );
        // let normal_ws: Vec3 = normal_dir_ws.normalize();
        // return normal_ws;
    }

    // Note: both uv_at() and norm_at() convert the same point into the same object space, maybe make the functions take a point in object space and convert the ws point to os on the caller's side.


}

// PointOnParaSurf
#[derive(Debug)]
struct PointOnObj { // like intersection but wo associated ray. if this is useful, move this to object.rs
        pub p: Vec3,
        pub uv: (f64, f64),
        pub norm: Vec3,

        // not in Intersection, helpful for ParaSurfs
        pub dpdu: Vec3,
        pub dpdv: Vec3,
        // should it store obj?
}

impl/*<F>*/ ParaSurf/*<F>
where
    F: Fn(f64, f64) -> Vec3 */
{
    // pub fn new(o2w: Mat, material: usize) -> Self {
    //     let w2o = o2w.inverse().unwrap();
        
    //     Self { o2w, w2o, material }
    // }
    
    fn get_point_on_obj_from_uv(&self, u: f64, v: f64) -> PointOnObj {
        PointOnObj {
            p: (self.r)(u, v),
            uv: (u, v),
            norm: self.norm_from_uv(u, v),
            dpdu: (self.ru)(u, v),
            dpdv: (self.rv)(u, v),
        }
    }

    // Intersection algorithm needs default point
    // In future, it should be based on intersections btwn this PS and similar rays
    // For this to work, there needs to be a cache (scene-wide?) of intersections
    // Lazy answer is just r(0, 0) assuming (0, 0) is valid param
    fn get_initial_guess(&self, ray: &Ray) -> PointOnObj {
        let u = PI / 4.0;
        let v = PI / 4.0;
        self.get_point_on_obj_from_uv(u, v)
    }

    fn norm_from_uv(&self, u: f64, v: f64) -> Vec3 {
        Vec3::cross((self.ru)(u, v), (self.rv)(u, v)).normalize()
    }
}

// impl Default for ParaSurf {
//     fn default() -> Self {
//         Self::new(Mat::identity(4), 0/*None*/)
//     }
// }

#[cfg(test)]
mod tests {
    use image::{ImageBuffer, Rgba};

    use crate::{core_math::{feq, EPSILON}, camera::Camera};
    use super::*;

    #[test]
    fn ps_intr_test() {
        let sphere = ParaSurf {
            r:  |u: f64, v: f64| Vec3::new( u.sin() * v.cos(), u.sin() * v.sin(), u.cos()),
            ru: |u: f64, v: f64| Vec3::new( u.cos() * v.cos(), u.cos() * v.sin(), -(u.sin())),
            rv: |u: f64, v: f64| Vec3::new(-u.sin() * v.sin(), u.sin() * v.cos(), 0.0),
            material: 0,
        };

        // make this into func. param: fn(cam ray) -> color
        let cam: Camera = Camera::new(
            60 * 2,
            34 * 2,
            1.0,
            9.0 / 16.0,
            1.0,
            Camera::view_transform(Vec3::jhat() * -6.0, Vec3::zero(), Vec3::khat()),
        );
        let mut imgbuf = ImageBuffer::new(cam.width_px as u32, cam.height_px as u32);
        for (col, row, pixel) in imgbuf.enumerate_pixels_mut() {
            let camera_ray: Ray = cam.cam_ray_for_pixel(col, row);

            let theoretical_color_at_pixel: Color = match sphere.intersection(&camera_ray) {
                Some(intr) => Color::white() * (6.0 - intr.t ),
                None => Color::black(),
            };
            let practical_color_at_pixel: Rgba<u8> = Color::theoretical_color_to_practical_color(theoretical_color_at_pixel);
        
            *pixel = practical_color_at_pixel;
            println!("0 {} {} {:?}", row, col, practical_color_at_pixel);
        }
        // Remember, this is relative to the terminal's cwd!
        imgbuf.save("testps.png").unwrap();
    }
}
