use crate::vec3::Vec3;
use vecspace::VectorSpace;
use crate::matrix::Mat;
use crate::ray::Ray;


// #[derive(Debug)]
pub struct Camera {
    // Width and height of output picture in pixels.
    pub width_px: i64,
    pub height_px: i64,
    // Width, height, depth of the diagetic viewport in diagetic units.
    pub viewport_width: f64,
    pub viewport_height: f64,
    pub viewport_depth: f64,
    // Transf mat that transforms the camera, camera space to world space
    pub c2w: Mat,
}

impl Camera {
    pub fn new(width_px: i64, height_px: i64, viewport_width: f64, viewport_height: f64, viewport_depth: f64, c2w: Mat) -> Camera {
        Camera { width_px, height_px, viewport_width, viewport_height, viewport_depth, c2w}
    }

    // Convert a pixel's coordinates from (col, row) from the top left corner to (x, z) from the middle.
    // ![diagram](../col_row_to_canvas_x_z_input_output.jpg)
    // Not talking about the world, still talking about pixels.
    fn topleft_colrow_to_centered_xz(&self, col: u32, row: u32) -> (f64, f64) {
        let centered_x: f64 = (col as f64) - (self.width_px as f64) / 2.0;
        let centered_z: f64 = -1.0 * (row as f64) + (self.height_px as f64) / 2.0;
        (centered_x, centered_z)
    }

    // Use a pixel's 2D pos get the 3D viewport vec it represents.
    // Maps each 2D coordinate on the canvas to a (3D vector from the camera to the equivalent) 3D coordinate in the diagetic viewport.
    // These vectors are relative to the camera (? in camera space).
    // +z is up, +x is right, +y is forward.
    // // use `ray.transform(c2w)` to move and rotate camera.
    fn centered_xz_to_viewport_vec3(
        &self,
        centered_x: f64, centered_z: f64
    ) -> Vec3 {
        Vec3::new(
            // Dimensional analysis:
            // Say the image is 6 pixels wide and the viewport is 10 diagetic units wide.
            // If a pixel is 2 pixels to the right of the middle,
            // then the vector it represents is `2 pixels * (10 du's / 6 pixels) = 3.33 du's` to the right of the middle.
            centered_x * (self.viewport_width / (self.width_px as f64)),
            self.viewport_depth,
            centered_z * (self.viewport_height / (self.height_px as f64))
        )
    }

    // Make a transf mat (camera space to world space)
    // based on:
    // - `pos`: Where the camera is.
    // - `to`: What the (center of the) camera is pointing at.
    // - `top`: The normal of the top of the camera. Like tilt.
    //          <0, 0, 1> = no tilt
    pub fn view_transform(pos: Vec3, to: Vec3, top: Vec3) -> Mat {
        // Vector pointing in the same direction of the camera
        let fwd: Vec3 = (to - pos).normalize();
        let right: Vec3 = Vec3::cross(fwd, top.normalize());
        let true_top: Vec3 = Vec3::cross(right, fwd);
        // If the rightmost col was [0, 0, 0, 1], the mat would have orthonormal cols, so the matrix's inverse would be its inverse
        let c2w: Mat = Mat::new(4, 4, vec![
            right.x, fwd.x, true_top.x, pos.x,
            right.y, fwd.y, true_top.y, pos.y,
            right.z, fwd.z, true_top.z, pos.z,
            0.0, 0.0, 0.0, 1.0
        ]);
        return c2w;
    }

    // Get the ray of the camera corresponding to a certain pixel of the output image.
    // Helper function, extracting code for pixel-by-pixel debugging.
    // Pixel's (col, row) coords -> pixel's centered (x, z) coords -> camera ray in camera space -> cam ray in world space
    pub fn cam_ray_for_pixel(&self, col: u32, row: u32) -> Ray {
        let (centered_x, centered_z) = self.topleft_colrow_to_centered_xz(col, row);
        let camera_direction_relative_to_camera_pyramid: Vec3 = self.centered_xz_to_viewport_vec3(centered_x, centered_z); // direction vector in camera space
        let camera_ray_cspc: Ray = Ray::new(
            Vec3::zero(),
            camera_direction_relative_to_camera_pyramid
        );
        // Convert camera ray to worldspace
        return camera_ray_cspc.transform(&self.c2w);
    }
}

impl Default for Camera {
    fn default() -> Self {
        Camera::new(
            160, 90,
            1.0, 9.0 / 16.0, 1.0,
            Mat::identity(4)
        )
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn view_transform_tests() {
        // default camera pos
        assert_eq!(
            Camera::view_transform(
                Vec3::zero(),
                Vec3::jhat(),
                Vec3::khat()
            ),
            Mat::identity(4)
        );
        // looking backwards, at the viewer
        assert_eq!(
            Camera::view_transform(
                Vec3::new(0.0, 0.0, 0.0),
                Vec3::new(0.0, -1.0, 0.0),
                Vec3::new(0.0, 0.0, 1.0)
            ),
            Mat::get_scaling_mat(-1.0, -1.0, 1.0)
        );
    }
}