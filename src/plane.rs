use crate::core_math::feq;
use crate::vec3::Vec3;
use vecspace::VectorSpace;
use crate::color::Color;
use crate::matrix::Mat;
use crate::ray::Ray;
use crate::object::{Object, Intersection};
use crate::material::Material;
use crate::aabb::Aabb;


#[derive(Debug)]
pub struct Plane {
    // matrix to transform points/vecs/rays from the object space of this plane into world space.
    pub o2w: Mat,
    // matrix to do the inverse
    pub w2o: Mat,
    pub n: Vec3,
    // pub aabb: Aabb,
    pub material: usize,
}

// Object space is a square in the xy plane.
// p0 = <0, 0, 0>, n = <0, 0, 1>, {-1 < x < 1, -1 < y < 1}
impl Plane {

    pub fn new(o2w: Mat, material: usize) -> Plane {
        let w2o = o2w.inverse().unwrap();
        let n = Vec3::khat().transform_dont_translate(&o2w);
        Self { o2w, w2o, n, material: material }
    }

    pub fn make_o2w(p0: Vec3, yaw: f64, pitch: f64, roll: f64, x_apothem: f64, y_apothem: f64) -> Mat {
        return Mat::identity(4)
            .scale(x_apothem, y_apothem, 1.0)
            .rotate(yaw, pitch, roll)
            .translate(p0.x, p0.y, p0.z);
    }
    pub fn local_is_on_plane(&self, point: Vec3) -> bool {
        return
            // Check if the point is on the unbounded xy plane
            feq(point.z, 0.0)
            // Check if the point is in the bounded square
            // x and y within (-1, 1)
            && point.x * point.x < 1.0
            && point.y * point.y < 1.0 
    }
    // return the t value of the intersection between the ray and the local bounded plane, if it exists
    pub fn local_ray_plane_int(&self, ray: &Ray) -> Option<f64> {
        if feq(ray.dir.z, 0.0) {
            return None;
        }
        // T value of intersection.
        // let t = -ray.start.z / ray.dir.z;
        let t = -ray.start.z * ray.one_over_dir.z;
        // Check to see if point is in bounds.
        // Duplicate work since this happens later.
        // Coordinates of intersection.
        let p = ray.plug_in_t(t);
        if p.x * p.x < 1.0 && p.y * p.y < 1.0 {
            return Some(t)
        }
        return None; 
    }
}

impl Object for Plane {
    fn get_aabb(&self) -> Aabb {
        let aabb_os = Aabb::new(
            Vec3::new(-1.0, -1.0, 0.0),
            Vec3::new(1.0, 1.0, 0.0)
        );
        let aabb = aabb_os.transform(&self.o2w);
        return aabb;
    }
    fn get_centroid(&self) -> Vec3 {
        Vec3::zero().transform(&self.o2w)
    }

    fn is_hit_by(&self, ray: &Ray) -> Option<f64> {
        let ray_os = ray.transform(&self.w2o);
        return self.local_ray_plane_int(&ray_os);
    }

    // uses default intersection() (making it a default implementation in impl Object {} doesnt work tho)
    fn intersection(&self, ray: &Ray) -> Option<Intersection> {
        // default implementation
        self.is_hit_by(ray)
        .map(|t| {
            let p = ray.plug_in_t(t);
            Intersection {
                p,
                uv: self.uv_at(p),
                norm: self.norm_at(p),
                wo: -ray.dir.normalize(), // I know for a fact that ray.dir isn't always unit length
                obj: self,
                t
            }
        })
    }

    fn get_material_i(&self) -> usize {
        self.material
    }
    fn uv_at(&self, point: Vec3) -> (f64, f64) {
        let point_os = point.transform(&self.w2o);
        let u = 0.5 * point_os.x + 0.5;
        let v = 0.5 * point_os.y + 0.5;
        return (u, v);
    }
    fn norm_at(&self, point: Vec3) -> Vec3 {
        assert!(self.local_is_on_plane(point.transform(&self.w2o)));
        self.n
    }
}

impl Default for Plane {
    fn default() -> Self {
        Self::new(Mat::identity(4), 0)
    }
}
