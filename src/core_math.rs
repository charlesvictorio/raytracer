// Core, fundamental math helper code.
// It is important that nothing in this file depends on anything else in the crate.
// But lots of things do depend on this file.
// In the dependency DAG, this file has a large in-degree, and 0 out-degree

use std::ops::{Mul, Add};

// Quadratic formula


pub enum QuadraticSolutions {
    ZeroSols,
    OneSol(f64),
    TwoSols(f64, f64),
}

pub fn quadratic_formula(a: f64, b: f64, c: f64) -> QuadraticSolutions {
    // Calculate the real roots of a quadratic equation $ax^2 + bx + c$
    // using the quadratic formula $x = \frac{-b \pm \sqrt{b^2 - 4ac}{2a}$

    let discriminant: f64 = b * b - 4.0 * a * c;
    if discriminant < 0.0 {
        return QuadraticSolutions::ZeroSols;
    } else if discriminant == 0.0 { // may not catch all times when d = 0 bc floating point errors
        return QuadraticSolutions::OneSol(-b / (2.0 * a));
    }

    let sqrt_disc: f64 = f64::sqrt(discriminant);
    let t1 = (-b + sqrt_disc) / (2.0 * a); // t1 > t2 always? I think yes unless a is negative
    let t2 = (-b - sqrt_disc) / (2.0 * a);
    return QuadraticSolutions::TwoSols(t1, t2);
}

// Float comparisons

pub const EPSILON: f64 = 0.00000001;

pub fn feq(a: f64, b: f64) -> bool {
    (a - b).abs() < EPSILON
}

/*
pub fn feq0(a: f64) -> bool {
    a.abs() < EPSILON
}
*/

// Idk how to categorize this

// Arithmetic mean
pub fn avg2points<T>(a: T, b: T) -> T 
where
    T: Add<T, Output = T>,
    T: Mul<f64, Output = T>
{
    (a + b) * 0.5
}

// Arithmetic mean of more than 2 points
pub fn avg_points<T>(pts: &[T]) -> T
where
    T: Copy
    + Default
    + Add<T, Output = T>
    // + std::iter::Sum
    + Mul<f64, Output = T>
{
    let one_over_n: f64 = 1.0 / pts.len() as f64;
    // pts.iter().copied().sum::<T>() * one_over_n // works
    pts.iter()
        .fold(T::default(), |sum, val| sum + *val)
        * one_over_n
}

// Add a value to an existing average value
// without calculating the original sum
// prev_avg is the existing average valgue
// new_val is the new value you want to mix in
// n is the total number of values, including self!
// [How to add and subtract values from an average?](https://math.stackexchange.com/questions/22348/how-to-add-and-subtract-values-from-an-average)
pub fn add2avg<T>(prev_avg: T, new_val: T, new_n: usize) -> T
where
    T: Copy
    + Add<T, Output = T>
    + Mul<f64, Output = T>
{
    lerp(prev_avg, new_val, 1.0 / new_n as f64)
}

// Linear interpolation
// t goes from 0 to 1
// Can lerp f64s, Vec3s, Colors, anything which you can multiply to floats to get T, and add them together.
// Can lerp any set with addition and scalar multiplication
pub fn lerp<T>(a: T, b: T, t: f64) -> T
where // Omg the rust trait bounds system is frustrating to use but so satisfying when you get it right
    T: Mul<f64, Output = T>, 
    T: Add<T, Output = T>
{
    a * (1.0 - t) + b * t
}

// Probability