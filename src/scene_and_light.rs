// This trait has stuff for both intersections and color
// Color needs intersections, intersections dont need color

use crate::core_math::{feq, EPSILON, lerp};
use crate::color::Color;
use crate::vec3::{Vec3, Vec3Axes};
use vecspace::VectorSpace;
use crate::ray::Ray;
use crate::object::{Intersection, Object};
use crate::material::Material;
use crate::aabb::Aabb;

use strum::IntoEnumIterator; // Apparantly needed here instead of vec3.rs
use image::{ImageBuffer, Rgba};

// For scene.render()
use crate::camera::Camera;

// #[derive(Debug, Default)]
pub struct Scene<'scene> {
// pub struct Scene<'scene, BackgroundTemplate: GetBackground> {
    // pub objects: Vec<Box<&'scene dyn Object>>, // ~~idk why this is a reference~~ Edit 2023.04.07: Now I know
    // pub objects: Vec<Box<dyn Object>>,
    // pub objects: &'scene Vec<Box<dyn Object + 'scene>>,
    pub objects: Vec<Box<dyn Object + 'scene>>,
    pub lights: Vec<Box<dyn Light>>,
    pub camera: Camera,
    materials: Vec<Material>,
    bvh: Bvh,
    // triangle_meshes: Vec<TriangleMesh<'a>>,

    // background: BackgroundTemplate
    background: Color,
}

/*
// Put in background.rs

trait GetBackground {
    // Assume ray hasn't hit anything, otherwise get_background() wouldn't be called
    fn get_background(r: Ray) -> Color;
}

struct SolidBackgroundColor(Color);
struct SkyBox; // texture
*/

// Intersection Logic
// =====================

impl<'scene> Scene<'scene> {
    pub fn new(
        // objects: Vec<Box<&'scene dyn Object>>,
        // objects: &'scene Vec<Box<dyn Object + '_>>,
        objects: Vec<Box<dyn Object + 'scene>>,
        lights: Vec<Box<dyn Light>>,
        camera: Camera,
        materials: Vec<Material>,
        // obj_files: Vec<&'a str>,
        background: Color,
    ) -> Scene<'scene> {
        let bvh = Bvh::build(&objects, 4);
        Scene { objects, lights, camera, materials, bvh, /* triangle_meshes: Vec::new()*/ background }
    }

    // Add a material to a scene, and then return a reference to it.
    pub fn add_material(&mut self, material: Material) -> &Material {
        let index_of_added_material = self.materials.len();
        self.materials.push(material);
        return &self.materials[index_of_added_material];
    }

    /*
    // Find the intersections of this ray and every object in the scene
    pub fn intersect(&self, &ray: Ray) -> Vec<Intersection> {
        // ...
        // for object in objects:
        //     intrs += object.intersect(ray)
    }
    */

    // Get the first thing that the ray intersects with in the scene.
    // Within a certain range of t values.
    // todo: use bvh
    // Then, obj.get_first_ray_obj_int() will only be called for the objs in a terminal node of the bvh
    pub fn get_first_thing_ray_hits(&self, ray: &Ray, min_t: f64, max_t: f64) -> Option<Intersection> {
        /*
        // optimize if slow
        let mut closest_obj_i: Option<usize> = None; // index of object in self.objects vector
        let mut closest_obj_t: f64 = f64::INFINITY; // t value of ray
        for (i, obj) in self.objects.iter().enumerate() {
            if let Some(t) = obj.is_hit_by(ray) {
                if (t < closest_obj_t) && (t > min_t) && (t < max_t) {
                    closest_obj_i = Some(i);
                    closest_obj_t = t;
                }
            }
        }
        if let Some(i) = closest_obj_i {
            return self.objects[i].intersection(ray); // always Some. have to recalculate one
        }
        return None;
        */
        return self.bvh.get_first_thing_ray_hits(&self.objects, ray, min_t, max_t);
    }
}

#[derive(Default)]
struct Bvh { // T must implement object, but this bound is enforced elsewhere.
    nodes: Vec<BvhNode>, // Since the Bvh is a complete binary tree with m leaf nodes, it will have 2m - 1 nodes in total. These can be preallocated.
    //root_i: usize,
    //nodes_used: usize, // nodes[nodes_used] is where you should add another node
    // prim_info: Vec<(Vec3, Aabb, Box<&'a dyn Object>)>, // split this into 3 vecs, last one being &scene.objects ?
    centroids: Vec<Vec3>,
    aabbs: Vec<Aabb>,
    // ptrs2objs: Vec<Box<&'a dyn Object>>,
    prim_indicies: Vec<usize>,
    number_of_bins: usize, // Settings variable which holds the number of bins to make when finding best split plane.
}

// If the node is a leaf node, than first_prim and prim_count matter and prim_count != 0
// If the node isnt a leaf node, than left_child_i and right_child_i matter
#[derive(Debug, Default, Clone)]
struct BvhNode {
    aabb: Aabb, // used for intersections
    // aabb_of_centroids: Aabb, // only used to find best split plane
    left_child_i: usize, // maybe can use implicit indexing? 2n + 1, 2n + 2. https://cs.stackexchange.com/questions/87154/why-does-the-formula-2n-1-find-the-child-node-in-a-binary-heap // or maybe use pointers
    right_child_i: usize, // maybe wont work if binary tree isn't complete. anyway, right_child_i is always left_child_i + 1
    first_prim: usize,
    prim_count: usize
}

#[derive(Default, Clone)]
struct Bin {
    object_count: usize,
    aabb: Aabb,
}

impl Bvh {

    // Get the indicies of the primitives in a particular node, given its first_prim and prim_count
    fn prims_of_node(&self, first_prim: usize, prim_count: usize)
        // -> &[usize] {
        // &self.prim_indicies[node.first_prim..node.first_prim + node.prim_count]
        -> std::iter::Take<std::iter::Skip<std::slice::Iter<'_, usize>>>{
        self.prim_indicies.iter().skip(first_prim).take(prim_count)
    }

    // impl Bvh
    // Make a new node in a Bvh using its first_prim and prim_count
    // - childless
    // - calculates aabb so the caller side doesnt have to
    fn bvh_node_from_prim_bounds(&self, first_prim: usize, prim_count: usize) -> BvhNode {
        let mut aabb_of_node = Aabb::default();
        for &i in self.prims_of_node(first_prim, prim_count) {
            // let aabb_of_prim = &self.prim_info[i].1;
            let aabb_of_prim = &self.aabbs[i];
            aabb_of_node.subsume(aabb_of_prim);
        }

        BvhNode {
            aabb: aabb_of_node, 
            left_child_i: 0,
            right_child_i: 0,
            first_prim,
            prim_count,
        }
    }

    // can probably improve with SIMD and parallelization
    // multiple objects at once, or all 3 axis at once.
    pub fn build<'a>(
        // objects: &Vec<Box<&'a dyn Object>>,
        objects: &'a Vec<Box<dyn Object + '_>>, // can be + 'a or + '_ ?
        number_of_bins: usize // number of bins, x: 4
    ) -> Self {
        // Step 1: Make bvh (with nodes, prim_info, and prim_indicies) using a list of objects.
        // Preallocated version
        // let mut nodes: Vec<BvhNode> = vec![BvhNode::default(); 2 * objects.len() - 1];
        // Non-preallocated version
        let nodes: Vec<BvhNode> = Vec::with_capacity(2 * objects.len() - 1);
        let prim_indicies: Vec<usize> = (0..objects.len()).collect();
        let (centroids, aabbs): (Vec<Vec3>, Vec<Aabb>) = Bvh::make_prim_info(&objects);
        let mut bvh = Bvh { nodes, centroids, aabbs, prim_indicies, number_of_bins };
        // ~~Step 2: Make aabb of the centroids of all objects in root node (aka all objects)~~
        // Step 3: Make a root node with everything in it, which will be recursively subdivided.
        // nodes[0] = BvhNode  // preallocated version
        let root_node = bvh.bvh_node_from_prim_bounds(0, objects.len());
        bvh.nodes.push(root_node);
        // let mut next_unused_node_i: usize = 1; // preallocated version
        // Step 4: Recursively subdivide root node.
        bvh.subdivide(0);
        // Step 5: When done, return the bvh.
        return bvh;
    }

    // Helper function (step 1) for Bvh::build()
    // Make a vec of (centroid, aabb, ptr to obj)'s
    fn make_prim_info<'a> (
        // objects: &Vec<Box<&'a dyn Object>>
        objects: &'a Vec<Box<dyn Object + '_>>, // can be + 'a or + '_ ?
    ) -> (Vec<Vec3>, Vec<Aabb>) {
        let mut centroids: Vec<Vec3> = Vec::with_capacity(objects.len());
        let mut aabbs: Vec<Aabb> = Vec::with_capacity(objects.len());
        for obj in objects.iter() {
            centroids.push(obj.get_centroid());
            aabbs.push(obj.get_aabb());
        }
        return (centroids, aabbs);
    }

    // ~~Helper function (step 2) for Bvh::build()~~
    // Makes an aabb of the centroids of a node.
    // Finds the smallest/largest x/y/z values of all the centroids of the primitives of a node.
    fn make_aabb_of_centroids/*<'a>*/(&self, node: &BvhNode) -> Aabb {
        let mut aabb_of_centroids_of_node = Aabb::default();
        for centroid in self.prims_of_node(node.first_prim, node.prim_count)
            .map(|&i| self.centroids[i])
        {
            aabb_of_centroids_of_node.add_point(centroid);
        }
        return aabb_of_centroids_of_node;
    }

    // Helper function (step 4) for Bvh::build()
    // Needs to pass index, not pointer, since rust thinks that `node2split: &mut BvhNode` is mutably borrowing self again.
    fn subdivide(&mut self, node2split_i: usize) {
        let node2split: &BvhNode = &self.nodes[node2split_i];
        // Step 4.0: Base case: If there are <= 2 objects in the node, don't divide it anymore.
        if node2split.prim_count <= 2 {
            return;
        }
        // Step 4.1: Find best way to divide the node into 2 subnodes, one on each side of a axis-aligned plane.
        if let Some((axis, split_pos)) = self.find_best_split_plane(node2split_i) {
            // Step 4.2: Partition the prim indicies by the axis and plane.
            let right_first_prim = self.partition_prims_of_node(node2split_i, axis, split_pos);
            // Step 4.3: Make 2 child bvh nodes
            // right_first_prim is the first index of an object that is on the right of the splitting plane
            // The left child node should have indicies from node.first_prim to right_first_prim - 1,
            // the right child node should have indicies from i to node.first_prim + node.prim_count - 1
            
            // Won't make child nodes if "best split plane" leaves one side empty and other side with all objects
            let left_child_i_or_none: Option<usize> = self.try_to_make_child_nodes(
                node2split_i,
                // &mut self.nodes[node2split_i],
                right_first_prim
            );
            if let Some(left_child_i) = left_child_i_or_none {
                // Step 4.4: Recurse again
                self.subdivide(left_child_i);
                self.subdivide(left_child_i + 1);
            }
            // Another base case: If there wasn't a way to split the node to make it have a lower SAH than the unsplit node, than stop.
        }
    }

    // Helper function (step 4.1) for bvh.subdivide()
    fn find_best_split_plane(&mut self, node2split_i: usize) -> Option<(Vec3Axes, f64)> {
        let node2split = &self.nodes[node2split_i];
        let aabb_of_centroids = self.make_aabb_of_centroids(node2split);
        let extent: Vec3 = aabb_of_centroids.diagonal(); // The length, width, and height of the node's aabb of centroids. // used in 4.1.0, 4.1.2
        let mut best_axis_and_split_pos: Option<(Vec3Axes, f64)> = None; // 4.1.4 and return
        let mut lowest_cost: f64 = node2split.sah_cost(); // lowest cost rn is cost of unsplit node // 4.1.4
        for axis in Vec3Axes::iter() { // Steps in 4.1 will be repeated.
            // Step 4.1.0: If any of the length/width/height of the node's aabb of centroids is zero,
            // than the you can't subdivide by this axis.
            if feq(extent[axis], 0.0) {
                continue;
            }
            // Step 4.1.1: Make n bins.
            let mut bins: Vec<Bin> = vec![Bin::default(); self.number_of_bins]; // 4.1.2, 4.1.3, 4.1.4

            // Step 4.1.2: Go through each object('s info), and populate the bins by updating the object counts and aabbs.
            // How to find which bin an object goes in:
            // Let's say there are n bins, [0, 1, 2, ... n - 1]
            // First, you want to find how far away the object's centroid is from the aabb's minimum: `centroid.x - node2split.aabb.min.x`
            // Then, normalize it so it goes from 0 to 1, like a percentage: `(centroid.x - node2split.aabb.min.x) / (node2split.aabb.max.x - node2split.aabb.min.x)`
            // Then, scale the "percentage" so it goes from 0 to n: `n * (centroid.x - node2split.aabb.min.x) / (node2split.aabb.max.x - node2split.aabb.min.x)`
            // If the object is on the right edge (centroid.x == node2split.aabb.max.x), than the "percentage" will be 1, and the scaled percentage will be n, but using n as an index into a vector with n items is out of bounds, so just use the rightmost one (bins[n - 1]): `min(n * (centroid.x - node2split.aabb.min.x) / (node2split.aabb.max.x - node2split.aabb.min.x) as i64, n - 1)`
            // Division is expensive, so store the non-varying part of the equation into the scale variable.
            let scale = self.number_of_bins as f64 / extent[axis]; // 4.1.2, 4.1.4
            let left_edge: f64 = aabb_of_centroids.min[axis]; // The smallest <axis>-value of all the primitives in the node.
            let max_bin_i = /* 2 * */ self.number_of_bins - 1; // change 1
            for &i in self.prims_of_node(node2split.first_prim, node2split.prim_count) {
                let centroid = &self.centroids[i];
                let aabb = &self.aabbs[i];
                let bin_i = max_bin_i.min(((centroid[axis] - left_edge) * scale) as usize);
                println!("debug change 1: max_bin_i = {}, other = {}, bin_i = {}", max_bin_i, ((centroid[axis] - left_edge) * scale) as usize, bin_i);
                // Add object to bin.
                bins[bin_i].object_count += 1;
                bins[bin_i].aabb.subsume(aabb);
            }

            // Step 4.1.3: Go through the bins both ways, and make cumsums.
            let (left_cumsums, right_cumsums) = self.make_cumsums(&bins);
            
            // Step 4.1.4: Go through the cumsums and find the axis and splitting plane with the smallest SAH
            // vars needed: &self, left_cumsums: &Vec<Bin>, right_cumsums: &Vec<Bin>, lowest_cost: &mut f64, best_axis_and_split_pos: &mut Option<(Axis, f64)>, node_to_split_i: usize, scale: f64
            for (i, (left_cumsum, right_cumsum)) in left_cumsums.iter().zip(right_cumsums.iter().rev()).enumerate() {
                let cost: f64 = left_cumsum.aabb.surface_area() * left_cumsum.object_count as f64
                    + right_cumsum.aabb.surface_area() * right_cumsum.object_count as f64;
                // If the SAH of the unsplit node is smaller than the smallest SAH from splitting the node, then stop.
                // Otherwise, continue on to partition the prim indicies by the axis and plane.
                if cost < lowest_cost {
                    lowest_cost = cost;
                    dbg!(left_edge); dbg!(scale); dbg!(i as f64 + 1.0);
                    best_axis_and_split_pos = Some((
                        axis,
                        left_edge + scale * (i as f64 + 1.0) // i + 1 because you want the plane to be on the right side of the bin
                    ));
                }
            }
        }

        best_axis_and_split_pos
    }

    // Helper function (step 4.1.3) for bvh.find_best_split_plane()
    fn make_cumsums(&self, bins: &Vec<Bin>) -> (Vec<Bin>, Vec<Bin>) {
        // right_cumsums stores the nodes in reverse order
        // left is [0, 1, 2, 3], right is [3, 2, 1, 0]
        // left_cumsum  = [bin 0, bin 0 + bin 1, bin 0 + bin 1 + bin 2 /*, bin 0 + bin 1 + bin 2 + bin 3 */]    // uses 0, 1, 2,
        // right_cumsum = [bin 3, bin 3 + bin 2, bin 3 + bin 2 + bin 1 /*, bin 3 + bin 2 + bin 1 + bin 0 */]    // uses 3, 2, 1
        // left_cumsum[i] corresponds with right_cumsum[self.number_of_bins - 1 - i], not right_cumsum[i]
        // If you are traversing left_cumsum, make sure to traverse right_cumsum in the reverse direction.
        
        /*
        let mut left_cumsums: Vec<Bin> = Vec::with_capacity(self.number_of_bins); // vec![Bin::default(); self.number_of_bins];
        let mut right_cumsums: Vec<Bin> = left_cumsums.clone();
        let mut rolling_left_sum: Bin = Bin::default();
        let mut rolling_right_sum: Bin = rolling_left_sum.clone();
        for (left_bin, right_bin) in bins.iter().zip(bins.iter().rev()) { // there's definately a cumsum pattern for rust iterators, right?
            // left_bin is the ith from the right, right_bin is the ith from the right
            // Add left bin to rolling left cumsum
            rolling_left_sum.object_count += left_bin.object_count;
            rolling_left_sum.aabb.subsume(&left_bin.aabb);
            // Set the cumsum for this bin to be the rolling sum.
            left_cumsums.push(rolling_left_sum.clone());
            // Do the same for the right bins
            rolling_right_sum.object_count += right_bin.object_count;
            rolling_right_sum.aabb.subsume(&right_bin.aabb);
            right_cumsums.push(rolling_right_sum.clone());
        }
        (left_cumsums, right_cumsums)
        */
        
        // Let's say `bins` = [0, 1, 2, 3],
        
        // Except last one
        // left_to_right = [0, 1, 2]
        let left2right = bins.iter().take(bins.len() - 1);

        // except first one, reversed
        // right_to_left = [3, 2, 1] 
        let right2left = bins.iter().skip(1).rev();
        
        // let left_and_right_cumsums: (Vec<Bin>, Vec<Bin>) =
        left2right.zip(right2left)
            .scan(
                (Bin::default(), Bin::default()),
                |(left_sum_so_far, right_sum_so_far), (left_bin, right_bin)| {
                    // left_bin is the ith from the right, right_bin is the ith from the right
                    // Add left bin to left sum
                    left_sum_so_far.object_count += left_bin.object_count;
                    left_sum_so_far.aabb.subsume(&left_bin.aabb);
                    // Do the same for the right bins
                    right_sum_so_far.object_count += right_bin.object_count;
                    right_sum_so_far.aabb.subsume(&right_bin.aabb);
                    // Some((*left_sum_so_far, *right_sum_so_far))
                    Some((left_sum_so_far.clone(), right_sum_so_far.clone()))
                }
            )
            .unzip()

    }

    // Helper function (step 4.2) for bvh.subdivide()
    // Make it so the indicies of every primitive in the node on the left side of the splitting plane
    // are to the left of the indicies of every primitive in the node on the right of the splitting plane
    // in the prim_indicies vec
    // self.prim_indicies = [ prims not in the node | indicies to the left of the splitting plane | indicies to the right of it | prims not in the node ]
    // Like quicksort partition
    // return the first index that points to a triangle on the right of the splitting plane 
    fn partition_prims_of_node(&mut self, node_i: usize, axis: Vec3Axes, split_pos: f64) -> usize {
        let node = &self.nodes[node_i];
        // Point to the first primitive in the node
        let mut i = node.first_prim; // idx should only be used in prim_idxs list, not any other list i think? can enforce with type alias?
        // Point to the last primitive in the node
        let mut j = node.first_prim + node.prim_count - 1;
        println!("hello"); dbg!(node_i); dbg!(axis); dbg!(split_pos); dbg!(node); dbg!(i); dbg!(j);
        dbg!(&self.prim_indicies);
        dbg!((i..=j).map(|k| self.centroids[k][axis]).collect::<Vec<f64>>());
        while i <= j {
            /*
            // Move i forwards until it finds a primitive that is on the right side of the splitting plane,
            // and needs to go to the right of the vec
            while self.prim_info[i].0[axis] <= split_pos {
                i += 1;
            }
            // Then move j backwards until it finds a primitive that is on the left side of the splittin plane,
            // and needs to go to the left of the vec.
            while self.prim_info[j].0[axis] >= split_pos {
                j -= 1;
            }
            // When these 2 items are identified, swap them
            if i < j {
                let i_holder = self.prim_indicies[i];
                self.prim_indicies[i] = self.prim_indicies[j];
                self.prim_indicies[j] = i_holder;
            }
            */
            // Move i forwards until it finds a primitive that is on the right side of the splitting plane,
            // and needs to go to the right side of the vec
            // if self.centroids[i][axis] < split_pos {
            if self.centroids[self.prim_indicies[i]][axis] < split_pos {
                i += 1;
                // Don't worry about i going out of bounds.
                // If i is out of bounds, this function will end before self.prim_indicies[i] is called.
                // j is <= the last idx of the list (j <= self.prim_indicies.len() - 1)
                // and if i > j, the func will return.
            } else {
                // Then, swap the objects at i and j and decrement j
                // If the object at j was supposed to be on the right,
                // it will be swapped back to the right and the next closest triangle will be chosen
                let i_holder = self.prim_indicies[i];
                self.prim_indicies[i] = self.prim_indicies[j];
                self.prim_indicies[j] = i_holder;
                dbg!(i, j, &self.prim_indicies);
                j -= 1;
            }
        }
        return i;
    }

    // Step 4.3: Helper function for bvh.subdivide()
    // right_first_prim: first index of a primitive on the right of the plane
    // if children were made, return the indicies of the children (right child index = left child index + 1)
    // otherwise, return None
    fn try_to_make_child_nodes(&mut self, parent_i: usize, right_first_prim: usize) -> Option<usize> {
        let parent = &self.nodes[parent_i]; // immutable borrow
        let left_prim_count = right_first_prim - parent.first_prim;
        let right_prim_count = parent.prim_count - left_prim_count;
        // Base case: If one side is empty, than you might as well not make child nodes and have the parent be a leaf node.
        if left_prim_count == 0 || right_prim_count == 0 {
            return None;
        }
        // make child nodes
        let left_child: BvhNode = self.bvh_node_from_prim_bounds(parent.first_prim, left_prim_count);
        let right_child: BvhNode = self.bvh_node_from_prim_bounds(right_first_prim, right_prim_count);
        // Add them to the bvh's nodes vec
        let left_child_i = self.nodes.len();
        self.nodes.push(left_child);
        self.nodes.push(right_child);
        // Stuff you don't have to do for step 3 of Bvh::build()
        // Connect the child nodes to the parent
        let parent = &mut self.nodes[parent_i]; // mutable borrow. I don't use the old parent afterward
        parent.left_child_i = left_child_i;
        parent.right_child_i = left_child_i + 1;
        // Indicate that the parent isn't a leaf node
        parent.prim_count = 0;
        return Some(left_child_i);
    }

    // used in scene.get_first_thing_ray_hits()
    // ray-bvh intersection
    // traverse
    pub fn get_first_thing_ray_hits<'scene>(&self, objects: &'scene Vec<Box<dyn Object + 'scene>>, ray: &Ray, min_t: f64, max_t: f64) -> Option<Intersection<'scene>> {
        // Stack of nodes to visit. DFS
        let mut nodes2visit: Vec<&BvhNode> = vec![&self.nodes[0]]; // start with root node
        let mut smallest_t_so_far = f64::INFINITY;
        let mut closest_int_so_far: Option<Intersection> = None;
        loop {
            if nodes2visit.is_empty() {
                return closest_int_so_far;
            }
            let node = nodes2visit.pop().unwrap(); // Get and remove the node at the top of the stack
            if !node.aabb.intersect(ray) {
                continue;
            }
            if node.is_leaf_node() {
                let closest_int_for_this_node = self.ray_leaf_int(objects, node, ray, smallest_t_so_far, min_t, max_t);
                // Any ray-leaf intersections will return None if their t's aren't closer than smallest_t_so_far.
                if let Some(int) = &closest_int_for_this_node {
                    smallest_t_so_far = int.t;
                    closest_int_so_far = closest_int_for_this_node;
                }
            } else {
                // internal node
                nodes2visit.push(&self.nodes[node.right_child_i]);
                nodes2visit.push(&self.nodes[node.left_child_i]);
            }
        }
    }

    // use Scene and bvh use idxs not ptrs? maybe pass scene.objects instead of whole scene
    // If you just want the closest int between a ray and every object in the leaf node, then set smallest_t_so_far to f64::INFINITY.
    // Otherwise, it will return None if none of the intersections are closer than smallest_t_so_far.
    fn ray_leaf_int<'scene> (&self, objects: &'scene Vec<Box<dyn Object + 'scene>>, leaf: &BvhNode, ray: &Ray, smallest_t_so_far: f64, min_t: f64, max_t: f64) -> Option<Intersection<'scene>> {
        let mut t_to_beat = smallest_t_so_far;
        let mut closest_obj: Option<&dyn Object> = None;
        // let mut closest_obj: Option<&Box<&dyn Object>> = None;
        for &i in self.prims_of_node(leaf.first_prim, leaf.prim_count) {
            let obj = &*objects[i];
            // let obj: &Box<dyn Object> = &scene.objects[i];
            if let Some(t) = obj.is_hit_by(ray) {
                if t < t_to_beat && min_t < t && t < max_t {
                    t_to_beat = t;
                    closest_obj = Some(obj);
                }
            }
        }
        // return closest_obj.map(|obj| Intersection::new(smallest_t_so_far, obj.clone())); // clone the box?
        return closest_obj.and_then(|obj| obj.intersection(ray)); // have to recalculate
    }

}


impl BvhNode {
    // Calculate the SAH cost of this node, to compare it with the SAH cost of 2 subnodes.
    pub fn sah_cost(&self) -> f64 {
        self.aabb.surface_area() * (self.prim_count as f64)
    }

    pub fn is_leaf_node(&self) -> bool {
        self.prim_count != 0 // does .into() work?
    }
}

// STUFF INVOLVING COLOR
// =====================
impl Scene<'_> {


    // Trace a ray and find a color
    // min_t should be 1.0 for camera ray, epsilon otherwise
    pub fn color_at_ray(&self, ray: &Ray, min_t: f64, current_recursion_depth: u8) -> Color {
        if current_recursion_depth == 0 {
            return Color::black();
        }
        
        // What does this ray hit?
        if let Some(intr) = self.get_first_thing_ray_hits(ray, min_t, f64::INFINITY) {
            // handle reflection and refraction later
            // println!("1 norm at {:?} is {:?}", ray.plug_in_t(t), obj.norm_at(ray.plug_in_t(t)));
            // let point = ray.plug_in_t(t);
            // let uv = obj.uv_at(point);
            // return self.color_at_point(point, obj, uv, -ray.dir.normalize(), current_recursion_depth);
            println!("1 intersection report: p: {:?}, uv: {:?}, norm: {:?}, wo: {:?}, obj, t: {:?}", intr.p, intr.uv, intr.norm, intr.wo, intr.t);
            return self.color_at_point(&intr, current_recursion_depth)
        }

        return self.background; // eventually have self.get_background_at(ray) to deal with uniform colors and skyboxes, and other fancy effects
    }

    // need: obj, xyz, uv
    pub fn color_at_point(&self, intr: &Intersection, current_recursion_depth: u8) -> Color {
        // let (u, v) = intr.uv;
        // let normal_at_point = obj.norm_at(point);
        let local_color = self.local_color_at_point(intr);
        // let r = self.materials[intr.obj.get_material_i()].reflectiveness.uv2texel(intr.uv.0, intr.uv.1);
        let r = self.materials[intr.obj.get_material_i()].specular_chance; // 2023.01.10 JUSTMAKEITWORK: just so it works for now, gonna change the rt algo anyway
        if feq(r, 0.0)  {
            return local_color;
        }
        let not_refractive = true;
        if not_refractive {
            // Shoot a reflection ray to figure out what color should be in the reflection of the object
            // then LERP it with the local color.
            let reflected_dir = intr.wo.reflect(intr.norm);
            let reflected_ray = Ray::new(intr.p, reflected_dir);
            let color_from_reflection = self.color_at_ray(&reflected_ray, EPSILON /* not 1.0 since not cam ray */, current_recursion_depth - 1);
            println!("2 {:?} {:?}", reflected_ray, color_from_reflection);
            // return color_from_reflection.scale(r) + local_color.scale(1.0 - r);
            return lerp(local_color, color_from_reflection, r);
        }
        panic!("cosmic ray detected");
    }

    // Get the color of a point on an object, but
    // ignore the object's reflectiveness and refractiveness.
    // Pretend the object is just diffuse. 2023.07.13: and specular right? just not r or r
    pub fn local_color_at_point(&self, intr: &Intersection) -> Color {
        // let mtrl = obj.get_material().unwrap_or(&Material::DEFAULT);
        // let base_color_at_point = obj.base_color_at(point);
        let mtrl = &self.materials[intr.obj.get_material_i()];
        let base_color_at_point = self.base_color_at_point_on_obj(intr);

        // let mut total_local_color: Color = base_color_at_point * ( /* multiply by light.intensity? of each light? */ mtrl.ambient.uv2texel(intr.uv.0, intr.uv.1)); // start off with free 0.2 * base color
        let mut total_local_color: Color = base_color_at_point; // 2023.01.10 JUSTMAKEITWORK: just so it works for now, gonna change the rt algo anyway
        for light in self.lights.iter() {
            total_local_color += light.added_color(self, intr, &mtrl, base_color_at_point); // can mtrl be gotten on the func side?
        }
        // println!("1 {:?}", total_local_color);
        return total_local_color;
    }

    // Base color depends on material.
    // The object only has an index to its material, but the scene owns the vec of materials.
    // So, the function to get the base color of an object at a point must be a method of Scene, not an Object struct like Sphere.
    // Each Object struct implements their xyz2uv() (world/object space to texture space) method,
    // which can be used for color texture mapping, displacement mapping, ambient occlusion mapping, etc
    // 2023.07.13: pass mtrl as param?
    pub fn base_color_at_point_on_obj(&self, intr: &Intersection) -> Color {
        let mtrl = &self.materials[intr.obj.get_material_i()];
        return mtrl.color.uv2texel(intr.uv.0, intr.uv.1);
    }

}


pub trait Light {
    // Get the added color of this *individual* light.
    // The light's color affects how it illuminates objects.
    fn added_color(&self, scene: &Scene, intr: &Intersection, material_at_point: &Material, base_color_at_point: Color) -> Color;

    // Get the vector from the point to the light.
    // fn get_l(&self, point: Vec3) -> Vec3;

    // Is something blocking the ray between the light and the point, so the point is in shadow?
    // Or is the path unimpeded/unoccluded?
    // Or something in between?
    // Completely unobstructed -> 1.0
    // Completely in shadow -> 0.0
    fn visibility(&self, scene: &Scene, point: Vec3) -> f64;
}

// pub trait AreaLight

// Camera stuff
// =====================
// Camera stuff calls scene.color_at_ray() (color stuff),
// which calls scene.get_first_thing_ray_hits() (intersection stuff)

impl Scene<'_> {
    // Render the scene and export it to a .png file.
    // `export_path` is relative to the cwd of the terminal running the program.
    // // `rays_per_px` must be a square number, probably only {1, 4, maybe 9}
    // The 
    pub fn render(&self, export_path: &str) {
        let mut imgbuf = ImageBuffer::new(self.camera.width_px as u32, self.camera.height_px as u32);
        let max_rec_depth: u8 = 3;

        for (col, row, pixel) in imgbuf.enumerate_pixels_mut() {
            let camera_ray: Ray = self.camera.cam_ray_for_pixel(col, row);

            let theoretical_color_at_pixel: Color = self.color_at_ray(&camera_ray, 1.0, max_rec_depth);
            let practical_color_at_pixel: Rgba<u8> = Color::theoretical_color_to_practical_color(theoretical_color_at_pixel);
        
            *pixel = practical_color_at_pixel;
            println!("0 {} {} {:?}", row, col, practical_color_at_pixel);
        }
        // Remember, this is relative to the terminal's cwd!
        imgbuf.save(export_path).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        matrix::Mat,
        sphere::Sphere,
        light2_implementations::PointLight
    };

    #[test]
    fn t1() {
        println!("nthtnh");
        // test iter_prim_is
    }

    #[test]
    fn test_render() {
        
        let mut materials: Vec<Material> = Vec::new();
        let mut objects_arena: Vec<Box<dyn Object>> = Vec::new();
        materials.push(Material::default());
        let s1: Sphere = Sphere::new(Mat::identity(4), materials.len() - 1);
        objects_arena.push(Box::new(s1)); // maybe shouldnt have ref lol
        let mut s = Scene {
            // objects: &objects_arena,
            objects: objects_arena,
            lights: Vec::new(),
            camera: Camera::default(),
            materials: materials,
            bvh: Bvh { nodes: Vec::new(), centroids: Vec::new(), aabbs: Vec::new(), prim_indicies: Vec::new(), number_of_bins: 4 },
            background: Color::black()
        };

        let l1: PointLight =  PointLight::new(Color::white() * 0.8, Vec3::new(10.0, 10.0, -10.0));
        s.lights.push(Box::new(l1));
        let cam: Camera = Camera::new(
            60,
            34,
            1.0,
            9.0 / 16.0,
            1.0,
            Mat::identity(4).translate(0.0, -5.0, 0.0),
        );
        s.camera = cam;
        s.bvh = Bvh::build(&s.objects, 4);
        
        s.render("out.png");
    }
}