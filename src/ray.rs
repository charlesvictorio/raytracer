use std::fmt;

use crate::vec3::Vec3;
use crate::matrix::Mat;

#[derive(Debug, Clone)]
pub struct Ray {
    pub start: Vec3,
    pub dir: Vec3, // direction
    pub one_over_dir: Vec3,
}

impl fmt::Display for Ray {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "r = {} + t * {}", self.start, self.dir)
    }
}

impl PartialEq for Ray {
    fn eq(&self, other: &Self) -> bool {
        self.start == other.start && self.dir == other.dir
    }
}

impl Eq for Ray {}

impl Ray {
    // Make a new ray.
    pub fn new(start: Vec3, dir: Vec3) -> Ray {
        Ray { start, dir, one_over_dir: dir.recip() }
    }

    // Find the position of a ray at "time" t.
    pub fn plug_in_t(&self, t: f64) -> Vec3 {
        self.start + self.dir * t
    }

    // Apply a transformation mat to a ray.
    pub fn transform(&self, mat: &Mat) -> Ray {
        Ray::new(
            self.start.transform(mat),
            self.dir.transform_dont_translate(mat)
        )
    }

}

#[cfg(test)]
mod tests {
    use super::*;
    use std::f64::consts::PI;

    #[test]
    fn pos() {
        let r1: Ray = Ray::new(
            Vec3::new(2.0, 3.0, 4.0),
            Vec3::new(1.0, 0.0, 0.0)
        );
        assert!(r1.plug_in_t(0.0) == Vec3::new(2.0, 3.0, 4.0));
        assert!(r1.plug_in_t(1.0) == Vec3::new(3.0, 3.0, 4.0));
        assert!(r1.plug_in_t(-1.0) == Vec3::new(1.0, 3.0, 4.0));
        assert!(r1.plug_in_t(2.5) == Vec3::new(4.5, 3.0, 4.0));
    }

    #[test]
    fn transform() {
        // Weirdly, rotating a ray is not shown.
        let r1: Ray = Ray::new(
            Vec3::new(1.0, 2.0, 3.0),
            Vec3::new(0.0, 1.0, 0.0)
        );
        let translate_mat: Mat = Mat::get_tranlation_mat(3.0, 4.0, 5.0);
        let scaling_mat: Mat = Mat::get_scaling_mat(2.0, 3.0, 4.0);
        assert!(
            r1.transform(&translate_mat)
            == Ray::new(
                Vec3::new(4.0, 6.0, 8.0),
                Vec3::new(0.0, 1.0, 0.0)
            )
        );
        assert!(
            r1.transform(&scaling_mat)
            == Ray::new(
                Vec3::new(2.0, 6.0, 12.0),
                Vec3::new(0.0, 3.0, 0.0)
            )
        );

        let r1: Ray = Ray::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0)
        );
        let m3: Mat = Mat::identity(4)
            .rotate(-PI / 2.0, 0.0, 0.0)
            .scale(2.0, 1.0, 1.0)
            .translate(5.0, 3.0, 7.0);
        assert!(r1.transform(&m3) == Ray::new(Vec3::new(5.0, 3.0, 7.0), Vec3::new(2.0, 0.0, 0.0)));
    }
}