use std::f64::consts::{PI, TAU, FRAC_1_PI, FRAC_1_SQRT_2};
use crate::core_math::{QuadraticSolutions, quadratic_formula};
use crate::vec3::Vec3;
use vecspace::VectorSpace;
use crate::color::Color;
use crate::matrix::Mat;
use crate::ray::Ray;
use crate::object::{Object, Intersection};
use crate::material::Material;
use crate::aabb::Aabb;

#[derive(Debug, Clone)]
pub struct Sphere {
    pub o2w: Mat,
    pub w2o: Mat,
    pub material: usize,
}

impl Object for Sphere {
    fn get_aabb(&self) -> Aabb {
        let aabb_in_os = Aabb::new(
            Vec3::new(-1.0, -1.0, -1.0),
            Vec3::new(1.0, 1.0, 1.0)
        );
        let aabb = aabb_in_os.transform(&self.o2w);
        return aabb;
    }
    fn get_centroid(&self) -> Vec3 {
        Vec3::zero().transform(&self.o2w)
    }

    // is_hit_by and intersection are basically wrappers around solve_ray_sphere_intersection_quadratic_equation
    // which return the results in the right formats.

    fn is_hit_by(&self, ray: &Ray) -> Option<f64> {
        let t_vals: QuadraticSolutions = self.solve_ray_sphere_intersection_quadratic_equation(ray);
        match t_vals {
            QuadraticSolutions::TwoSols(t1, t2) => {
                return Some(t1.min(t2));
            }
            QuadraticSolutions::OneSol(t1) => {
                return Some(t1);
            }
            QuadraticSolutions::ZeroSols => {
                return None;
            }
        }
    }

    // uses default intersection() (making it a default implementation in impl Object {} doesnt work tho)
    fn intersection(&self, ray: &Ray) -> Option<Intersection> {
        // default implementation
        self.is_hit_by(ray)
        .map(|t| {
            let p = ray.plug_in_t(t);
            Intersection {
                p,
                uv: self.uv_at(p),
                norm: self.norm_at(p),
                wo: -ray.dir.normalize(), // I know for a fact that ray.dir isn't always unit length
                obj: self,
                t
            }
        })
    }



    fn get_material_i(&self) -> usize {
        self.material
    }
    
    // Convert a point on the object in world space to a (u, v) coordinate in texture space.
    // 1. Convert point to object space.
    // 2. Convert that point into spherical coordinates.
    //    - rho always = 1 on the surface
    //    - theta = atan(y / x). actually use y.atan2(x), which goes between -pi and pi
    //    - phi = acos(z). goes from 0 to pi
    // 3. Convert (theta, phi) to (u, v)
    //    - u and v are within [0, 1]
    //    - u goes from 0 to 1 as you go CCW around the sphere.
    //    - u = 0 at P = <-1.0, -EPSILON, 0.0>, u = 1 at P = <-1.0, 0.0, 0.0>
    //    - v goes from 0 to 1 as it goes from south pole to north pole
    // In the ray tracer challenge, theta = x.atan2(y) and u = -(1.0 / TAU) * theta + 0.5, which is the wrong way.
    // This results in u = 0 being at -zhat instead of <-1.0, -EPSILON, 0.0>.
    fn uv_at(&self, point: Vec3) -> (f64, f64) {
        let point_os = point.transform(&self.w2o);
        let theta = point_os.y.atan2(point_os.x);
        let phi = point_os.z.acos();
        let u = (1.0 / TAU) * theta + 0.5;
        let v = -FRAC_1_PI * phi + 1.0;
        return (u, v);
    }

    fn norm_at(&self, point: Vec3) -> Vec3 {
        // Convert point from world space to object space. 
        // The vector from the origin to that point (in object space) is the direction of the normal vector.
        let normal_dir_os: Vec3 = point.transform(&self.w2o);
        // Convert that vector to world space.
        // [Why multiply the normal vector by the inverse transpose?](https://paroj.github.io/gltut/Illumination/Tut09%20Normal%20Transformation.html)
        // Translation wont affect converting the norm vec to ws
        let normal_dir_ws: Vec3 = normal_dir_os.transform/*_dont_translate*/(
            &self.w2o
                .transpose()
        );
        let normal_ws: Vec3 = normal_dir_ws.normalize();
        return normal_ws;
    }

    // Note: both uv_at() and norm_at() convert the same point into the same object space, maybe make the functions take a point in object space and convert the ws point to os on the caller's side.


}

impl Sphere {
    pub fn new(o2w: Mat, material: usize) -> Self {
        let w2o = o2w.inverse().unwrap();
        
        Self { o2w, w2o, material }
    }

    // Extract common behavior for the functions that get all intersections and the nearest one.
    // Solve the quadratic equation that finds the t-values for the 0-2 ray-sphere intersections.
    fn solve_ray_sphere_intersection_quadratic_equation(&self, ray: &Ray) -> QuadraticSolutions {
        // Convert the ray to the sphere's object space.
        let ray_os: Ray = ray.transform(&self.w2o);
        // let sphere_center_to_ray_start: Vec3 = ray_os.start - sphere.center;
        let sphere_center_to_ray_start: Vec3 = ray_os.start; // in the sphere's object space, its center is at the origin.
        let a: f64 = ray_os.dir * ray_os.dir;
        let b: f64 = 2.0 * (sphere_center_to_ray_start * ray_os.dir);
        let c: f64 = sphere_center_to_ray_start * sphere_center_to_ray_start - /* sphere.radius * sphere.radius */ 1.0; // in the sphere's object space, its radius is 1.
        return quadratic_formula(a, b, c); // t-values
    }
}

impl Default for Sphere {
    fn default() -> Self {
        Self::new(Mat::identity(4), 0/*None*/)
    }
}

#[cfg(test)]
mod tests {
    use crate::core_math::{feq, EPSILON};
    use super::*;

    impl Sphere {
        // Compute the intersection of a ray and a sphere.
        // out: A vector of btwn [0, 2] intersections 
        // to get the coords, use the ray equation and plug in the t value(s) if they exist
        // is coord from one t always closer than cord from other t? 
        fn get_all_ray_obj_ints(&self, ray: &Ray) -> /*Vec<Intersection>*/Vec<(f64, Box<&dyn Object>)> {
            let t_vals: QuadraticSolutions = self.solve_ray_sphere_intersection_quadratic_equation(ray);
            let intersections: Vec<(f64, Box<&dyn Object>)> = match t_vals {
                QuadraticSolutions::TwoSols(mut t1, mut t2) => {
                    // Add the intersections in ascending order of t.
                    if t1 > t2 {
                        (t1, t2) = (t2, t1);
                    }
                    vec![
                        (t1, Box::new(self)),
                        (t2, Box::new(self))
                    ]
                    // if t1 < t2, else switch bindings
                }
                QuadraticSolutions::OneSol(t1) => {
                    vec![(t1, Box::new(self))]
                }
                QuadraticSolutions::ZeroSols => {
                    Vec::new()
                }
            };
            return intersections;
        }
    }

    #[test]
    fn ray_sphere_intr() {
        let s1: Sphere = Sphere::default();

        let r1: Ray = Ray::new(
            Vec3::new(0.0, 0.0, -5.0),
            Vec3::new(0.0, 0.0, 1.0)
        );
        let r1_s1_intrs: Vec<(f64, Box<&dyn Object>)> = s1.get_all_ray_obj_ints(&r1);
        assert!(r1_s1_intrs.len() == 2 && r1_s1_intrs[0].0 == 4.0 && r1_s1_intrs[1].0 == 6.0);

        let r2: Ray = Ray::new(
            Vec3::new(0.0, 1.0, -5.0),
            Vec3::new(0.0, 0.0, 1.0)
        );
        let r2_s1_intrs: Vec<(f64, Box<&dyn Object>)> = s1.get_all_ray_obj_ints(&r2);
        assert!(r2_s1_intrs.len() == 1 && r2_s1_intrs[0].0 == 5.0);
    
        let r3: Ray = Ray::new(
            Vec3::new(0.0, 2.0, -5.0),
            Vec3::new(0.0, 0.0, 1.0)
        );
        let r3_s1_intrs: Vec<(f64, Box<&dyn Object>)> = s1.get_all_ray_obj_ints(&r3);
        assert!(r3_s1_intrs.len() == 0);
    
        let r4: Ray = Ray::new(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 0.0, 1.0)
        );
        let r4_s1_intrs: Vec<(f64, Box<&dyn Object>)>= s1.get_all_ray_obj_ints(&r4);
        assert!(r4_s1_intrs.len() == 2 && r4_s1_intrs[0].0 == -1.0 && r4_s1_intrs[1].0 == 1.0);
    }

    #[test]
    fn sphere_normals() {
        let s: Sphere = Sphere::default();

        assert_eq!(
            s.norm_at(Vec3::new(1.0, 0.0, 0.0)),
            Vec3::new(1.0, 0.0, 0.0)
        );
        assert_eq!(
            s.norm_at(Vec3::new(0.0, 1.0, 0.0)),
            Vec3::new(0.0, 1.0, 0.0)
        );
        let x: f64 = f64::sqrt(3.0) / 3.0;
        assert_eq!(
            s.norm_at(Vec3::new(x, x, x)),
            Vec3::new(x, x, x)
        )
    }

    #[test]
    fn some_int_tests() {
        let s4: Sphere = Sphere::new(
            Mat::identity(4)
                .scale(2.0, 1.0, 1.0),
            0 //None
        );
        let r1: Ray = Ray::new(
            Vec3::new(-2.0, 0.0, 2.0),
            Vec3::new(1.0, 0.0, -3.0).normalize()
        );
        if let Some(t) = s4.is_hit_by(&r1) {
            let point_of_int: Vec3 = r1.plug_in_t(t);
            let norm_at_point_of_int: Vec3 = s4.norm_at(point_of_int);
            println!("t = {}, r1.dir = {}", t, r1.dir);
            println!("norm at {}, which is {} in os, is {}", point_of_int, point_of_int.transform(&s4.w2o), norm_at_point_of_int);
        } else {
            println!("no intersection")
        }
    }

    #[test]
    fn test_xyz2uv() {
        let s1 = Sphere::default();
        let examples: Vec<(Vec3, (f64, f64))> = vec![
            (Vec3::new(-1.0, 0.0, 0.0), (1.0, 0.5)),
            (Vec3::new(-1.0, -EPSILON, 0.0), (0.0, 0.5)),
            (Vec3::new(0.0, -1.0, 0.0), (0.25, 0.5)),
            (Vec3::new(1.0, 0.0, 0.0), (0.5, 0.5)),
            (Vec3::new(0.0, 1.0, 0.0), (0.75, 0.5)),
            (Vec3::new(0.0, 0.0, 1.0), (0.5, 1.0)),
            (Vec3::new(0.0, 0.0, -1.0), (0.5, 0.0)),
            (Vec3::new(0.0, -FRAC_1_SQRT_2, FRAC_1_SQRT_2), (0.25, 0.75))
        ];
        for (point, (expected_u, expected_v)) in examples {
            let (observed_u, observed_v) = s1.uv_at(point);
            assert!(feq(observed_u, expected_u));
            assert!(feq(observed_v, expected_v));
        }
    }
}
