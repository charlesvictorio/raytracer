use std::fs::File;
use std::io::{BufReader, BufRead};
use std::collections::HashMap;

use crate::aabb::Aabb;
use crate::core_math::feq;
use crate::object::{Object, Intersection};
use crate::ray::Ray;
use crate::vec3::Vec3;
use vecspace::VectorSpace;

// https://en.wikipedia.org/wiki/Polygon_mesh#Face-vertex_meshes
// https://www.pbr-book.org/3ed-2018/Shapes/Triangle_Meshes
#[derive(Debug)]
pub struct TriMesh {
    pub verticies: Vec<Vertex>,
    // If triangle 16 uses verticies 3, 7, and 2, than ts[16] = [3, 7, 2].
    // Verticies are listed in CCW order
    pub ts: Vec<[usize; 3]>,
    // index of the mesh's material in the scene.materials vector
    material: usize,
}

// Store xyz, uv, n info in one struct, not seperately
#[derive(Debug)]
pub struct Vertex {
    xyz: Vec3, // points in 3d world space
    uv: (f64, f64), // uv coordinates of verticies on surface parameterization. used for textures
    n: Vec3, // normal at vertex
}

impl TriMesh {
    pub fn from_obj_file(fname: &str) -> TriMesh { // wrap TriMesh in Result
        let mut vs = vec![Vec3::zero()]; // maybe remove dummy verticies, maybe not
        let mut vts: Vec<(f64, f64)> = vec![(0.0, 0.0)];
        let mut vns = vec![Vec3::zero()];
        // let mut f_lines: Vec<&[&str; 3]> = Vec::new(); // Vec<[&str; 3]>, Vec<[String; 3]>, Vec<[[usize; 3]; 3]>. how much ownership do i need
        let mut f_lines: Vec<[String; 3]> = Vec::new();
        let /*mut*/ material = 0;

        let file = File::open(fname).unwrap(); // todo: handle file opening errors
        let reader = BufReader::new(file);
        for line in reader.lines() {
            let line = line.unwrap(); // this will always work right?
            let tokens: Vec<&str> = line.split_whitespace().collect(); // i think this needs to be owned

            match tokens.as_slice() {
                [] => {continue}
                ["v", x, y, z] => {
                    vs.push(Vec3::new(
                        x.parse::<f64>().unwrap(),
                        y.parse::<f64>().unwrap(),
                        z.parse::<f64>().unwrap()
                    ));
                }
                ["vt", u, v] => {
                    vts.push((
                        u.parse::<f64>().unwrap(),
                        v.parse::<f64>().unwrap(),
                    ));
                }
                ["vn", x, y, z] => {
                    vns.push(Vec3::new(
                        x.parse::<f64>().unwrap(),
                        y.parse::<f64>().unwrap(),
                        z.parse::<f64>().unwrap()
                    ));
                }
                ["f", v1, v2, v3] => {
                    // f_lines.push(&[v1, v2, v3])
                    f_lines.push([v1.to_string(), v2.to_string(), v3.to_string()])
                }
                ["f", v1, v2, v3, v4] => { // splitting a rectangle into 2 triangles
                    // f_lines.push(&[v1, v2, v3]);
                    // f_lines.push(&[v1, v3, v4]);
                    f_lines.push([v1.to_string(), v2.to_string(), v3.to_string()]);
                    f_lines.push([v1.to_string(), v3.to_string(), v4.to_string()]);
                }
                ["f", verticies @ ..] => { // face with >= 5 face
                    for i in 2..(verticies.len() - 1) { // fanning
                        // f_lines.push(&[verticies[1], verticies[i], verticies[i + 1]])
                        f_lines.push([verticies[1].to_string(), verticies[i].to_string(), verticies[i + 1].to_string()])
                    }
                }
                ["#", ..] => continue,
                _ => {
                    println!("Skipping line \"{}\" because it is not recognized as .obj syntax", line);
                }
            }
        }
        
        println!("vs: {:?}", vs);
        println!("vts: {:?}", vts);
        println!("vns: {:?}", vns);
        println!("f_lines: {:?}", f_lines);

        // version 2: store v1,2,3 (like "3/3/3") in dict, associate with idx in xyzs, uvs, ns
        let mut verticies: Vec<Vertex> = Vec::new();
        let mut ts: Vec<[usize; 3]> = Vec::new();
        let mut already_in_tm: HashMap<&String, usize> = HashMap::new(); // cache. has vertex already been added to verticies?
        let mut indicies_of_tri_to_add: [usize; 3] = [0, 0, 0]; // holder. gets updated every line, gets added to ts
        for line in f_lines.iter() {
            for (i, vertex_str) in line.iter().enumerate() {
                indicies_of_tri_to_add[i] = *already_in_tm.entry(vertex_str)
                    .or_insert_with(|| {
                        // vertex_str = "1/2/3" -> v = 1, vt = 2, vn = 3
                        let v_is: Vec<usize> = vertex_str.split("/").map(|s| s.parse::<usize>().unwrap()).collect();

                        let v  = v_is[0];
                        let vt = v_is[1];
                        let vn = v_is[2];
        
                        // add vertex to verticies and already_in_tm
                        let j = verticies.len(); // j is the index of the vertex I just added in the vector verticies
                        verticies.push(Vertex {
                            xyz: vs[v], uv: vts[vt], n: vns[vn]
                        });
                        j
                    })
            }
            ts.push(indicies_of_tri_to_add)
        }

        let tm = TriMesh {
            verticies,
            ts,
            material
        };
        tm

        /*
        // Now handle faces
        let mut tm = TriMesh {
            xyzs: Vec::new(), // vec![Vec3::zero()], // do the dummy values go here or in vs, etc
            uvs: Vec::new(), // vec![(0.0, 0.0)],
            ns: Vec::new(), // vec![Vec3::zero()],
            ts: Vec::new(),
            material
        };
        // Set of all previously seen v/vt/vn's
        // If previously seen, then they were already added to the tm and the vertex info is stored in xyzs, uvs, and ns with a certain index
        // So return that index.
        // If not previously seen, then add the vertex info to xyzs, uvs, and ns and cache the index where they are stored.
        // key: (v1, vt1, vn1), value: the index in xyzs, uvs, and ns that corresponds to that vertex
        let mut indicies_of_t_to_add: [usize; 3] = [0, 0, 0];
        let mut already_in_tm: HashMap<(usize, usize, usize), usize> = HashMap::new();
        for f_line in f_lines.iter() { // [v1, v2, v3]
            for (i, vvtvn) in f_line.iter().enumerate() {
                // Convert line string to usizes.
                // line is "f 6/4/1 3/5/3 7/6/5" -> f_line = ["6/4/1", "3/5/3", "7/6/5"]
                // -> v1 = "6/4/1"
                // -> v1_is = vec![6, 4, 1]
                // -> v1 = 6, vt1 = 4, vn1 = 1
                let v_is: Vec<usize> = vvtvn.split("/").map(|s| s.parse::<usize>().unwrap()).collect();
                let v  = v_is[0];
                let vt = v_is[1];
                let vn = v_is[2];

                // if (v, vt, vn) not in already_in_tm:
                //     // should all have the same index
                //     assert!(tm.xyzs.len() == tm.uvs.len() == tm.ns.len());
                //     tm.xyzs.push(vs[v]);
                //     tm.uvs.push(vts[vt]);
                //     tm.ns.push(vns[vn]);
                //     already_in_tm[(v, vt, vn)] = tm.xyzs.len() - 1; // index of what i just added in all 3 arrays
                //     indicies_of_t_to_add[i] = tm.xyzs.len() - 1;
                // else:
                //     indicies_of_t_to_add[i] = already_in_tm[(v, vt, vn)];

                indicies_of_t_to_add[i] = *already_in_tm.entry((v, vt, vn))
                    .or_insert_with(|| {
                        assert!(tm.xyzs.len() == tm.uvs.len() && tm.uvs.len() == tm.ns.len()); // should all have the same index
                        tm.xyzs.push(vs[v]);
                        tm.uvs.push(vts[vt]);
                        tm.ns.push(vns[vn]);
                        tm.xyzs.len() - 1 // index of what was just added in all 3 arrays
                    })

            }
            tm.ts.push(indicies_of_t_to_add);
        }
        tm
        */
    }

    /*
    fn add_triangle_from_vertex_indicies(tm: &mut TriMesh, cache: &mut HashMap<(usize, usize, usize), usize>, v1: &str, v2: &str, v3: &str) {
        let v1_is: Vec<usize> = v1.split("/").map(|s| s.parse::<usize>().unwrap()).collect();
        let v2_is: Vec<usize> = v2.split("/").map(|s| s.parse::<usize>().unwrap()).collect();
        let v3_is: Vec<usize> = v3.split("/").map(|s| s.parse::<usize>().unwrap()).collect();

        let v1  = v1_is[0]; let v2  = v2_is[0]; let v3  = v3_is[0];
        let vt1 = v1_is[1]; let vt2 = v2_is[1]; let vt3 = v3_is[1];
        let vn1 = v1_is[2]; let vn2 = v2_is[2]; let vn3 = v3_is[2];
        // blender hammer (which is a non-smooth triangle mesh) shows same v and vt, but different vn
        // if you see the same v and vt but different vn, than consider them different verticies
        // consider them a repeat vertex if they have the same v, vt, vn
        
        // If not added before, add the verticies 

        // The code below doesnt work
        // maybe "flip" hashmap? -> 
        // for ((vi, vti, vni), ti) in cache.iter() {
            // 
        // }

        // tm.ts.len() is biggest unused vertex index
        let v1_i = *cache.entry((v1, vt1, vn1))
            .or_insert_with(|| {
                // tm.xyzs.push(vs[v1]);
                // tm.uvs.push(vts[vt1]);
                // tm.ns.push(vns[vn1]);
                tm.ts.len()
            });
        let v2_i = *cache.entry((v2, vt2, vn2))
            .or_insert_with(|| {
                // tm.xyzs.push(vs[v1]);
                // tm.uvs.push(vts[vt1]);
                // tm.ns.push(vns[vn1]);
                tm.ts.len()
            });
        let v3_i = *cache.entry((v3, vt3, vn3))
            .or_insert_with(|| {
                // tm.xyzs.push(vs[v1]);
                // tm.uvs.push(vts[vt1]);
                // tm.ns.push(vns[vn1]);
                tm.ts.len()
            });

        tm.ts.push([v1_i, v2_i, v3_i])
    }
    */
}


// Triangle in triangle mesh
// To get the xyz coords of triangle #17's verticies,
// v1 is at self.mesh.xyzs[self.mesh.ts[17][0]],
#[derive(Debug)]
pub struct Tri3<'a> {
    pub i: usize, // The index of the triangle in the mesh's ts vector.
    pub mesh: &'a TriMesh, // ref to mesh that the triangle is in
}

impl<'a> Tri3<'a> {
    // Helper function: let [i1, i2, i3] = self.my_is();
    fn my_is(&self) -> [usize; 3] {
        self.mesh.ts[self.i]
    }
    // Helper function: let (v1, v2, v3) = self.my_xyzs();
    fn my_xyzs(&self) -> (Vec3, Vec3, Vec3) {
        let [i1, i2, i3] = self.my_is();
        (
            self.mesh.verticies[i1].xyz,
            self.mesh.verticies[i2].xyz,
            self.mesh.verticies[i3].xyz,
        )
    }
    // Helper function: let (vt1, vt2, vt3) = self.my_uvs();
    fn my_uvs(&self) -> ((f64, f64), (f64, f64), (f64, f64)) {
        let [i1, i2, i3] = self.my_is();
        (
            self.mesh.verticies[i1].uv,
            self.mesh.verticies[i2].uv,
            self.mesh.verticies[i3].uv,
        )
    }
    // Helper function: let (vn1, vn2, vn3) = self.my_ns();
    fn my_ns(&self) -> (Vec3, Vec3, Vec3) {
        let [i1, i2, i3] = self.my_is();
        (
            self.mesh.verticies[i1].n,
            self.mesh.verticies[i2].n,
            self.mesh.verticies[i3].n,
        )
    }

    // Get the (u, v, w = 1 - u - v) coordinates of a point within the triangle.
    // Any point within the circle is a convex combination of the verticies.
    // Find the coeffs of that combo.
    // idk if this is accurate
    fn get_barycentric_coords(&self, point: Vec3) -> (f64, f64) {
        let (v1, v2, v3) = self.my_xyzs();
        let one_over_det = 1.0 / Vec3::s_trip_prod(v1, v2, v3);
        let u = Vec3::s_trip_prod(v1, point, v3) * one_over_det;
        let v = Vec3::s_trip_prod(v1, v2, point) * one_over_det;
        (u, v)
    }

    // Möller-Trumbore ray-triangle intersection algorithm
    // For triangles, you need to calculate the uv to calculate the t.
    // So finding uv doesn't take any extra work than finding t.
    // So return the t, u, v of the intersection if it exists.
    fn inner_intersection(&self, ray: &Ray) -> Option<(f64, f64, f64)> {
        let (v1, v2, v3) = self.my_xyzs();
        let e1 = v2 - v1;
        let e2 = v3 - v1;
        let o_minus_a = ray.start - v1;
        let neg_d = -ray.dir;
        let e1xe2 = Vec3::cross(e1, e2); // self.norm is this, normalized.
        // Taking the dot product of the direction and the normal vector also lets me know if the ray is parallel to the triangle.
        let det = neg_d * e1xe2;
        if feq(det, 0.0) {
            return None;
        }
        let recip_det = 1.0 / det;
        let u = neg_d * Vec3::cross(o_minus_a, e2) * recip_det;
        if u < 0.0 || u > 1.0 {
            return None;
        }
        let v = neg_d * Vec3::cross(e1, o_minus_a) * recip_det;
        if v < 0.0 || u + v > 1.0 {
            return None;
        }
        let t = o_minus_a * e1xe2 * recip_det;
        return Some((t, u, v));
    }
}

impl<'a> Object for Tri3<'a> {
// impl Object for Tri3<'_> {
    // idk if this works
    // ? shouldn't be used, should just be calculated when intersection testing
    // uv coords not relative to the triangle, but relative to the mesh
    fn uv_at(&self, point: Vec3) -> (f64, f64) {
        let ((u1, v1), (u2, v2), (u3, v3)) = self.my_uvs();
        let (bcu, bcv) = self.get_barycentric_coords(point);
        let bcw = 1.0 - bcu - bcv;
        (
            bcw * u1 + bcu * u2 + bcv * u3,
            bcw * v1 + bcu * v2 + bcv * v3,
        ) 
    }
    fn get_material_i(&self) -> usize {
        self.mesh.material
    }
    // It doesn't take any extra work to calculate the uv and t
    // just calculate t and uv and throw away the uv 
    fn is_hit_by(&self, ray: &Ray) -> Option<f64> {
        self.inner_intersection(ray).map(|(t, _, _)| t)
    }

    fn intersection(&self, ray: &Ray) -> Option<Intersection> {
        self.inner_intersection(ray)
            .map(|(t, u, v)| {
                let p = ray.plug_in_t(t);
                Intersection {
                    p,
                    uv: (u, v),
                    norm: self.norm_at(p),
                    wo: -ray.dir.normalize(),
                    obj: self,
                    t
                }
            })
    }
    fn norm_at(&self, _point: Vec3) -> Vec3 {
        let (v1, v2, v3) = self.my_xyzs();
        // TODO FIXME later
        // version 1
        // let (u, v) = self.uv_at(point);
        // v1.scale(1.0 - u - v) + v2.scale(u) + v3.scale(v)
        // version 2
        Vec3::cross(v2 - v1, v3 - v1).normalize()
    }
    fn get_aabb(&self) -> Aabb {
        let (v1, v2, v3) = self.my_xyzs();
        let mut aabb = Aabb::default();
        aabb.add_point(v1);
        aabb.add_point(v2);
        aabb.add_point(v3);
        return aabb;
    }
    fn get_centroid(&self) -> Vec3 {
        let (v1, v2, v3) = self.my_xyzs();
        let one_third = 1.0 / 3.0;
        let centroid = Vec3::new(
            (v1.x + v2.x + v3.x) * one_third,
            (v1.y + v2.y + v3.y) * one_third,
            (v1.z + v2.z + v3.z) * one_third
        );
        return centroid;
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::f64::consts::{SQRT_2, PI}; 
    use crate::color::Color;
    use crate::light2_implementations::PointLight;
    use crate::matrix::Mat;
    use crate::scene_and_light::Scene;
    use crate::camera::Camera;
    use crate::object::Object;
    use crate::material::Material;
    use crate::sphere::Sphere;
    use crate::plane::Plane;

// need to update tests
    /*
    #[test]
    fn test_of_tri_mesh_created_by_hand() {
        let tm1 = TriMesh {
            xyzs: vec![
                Vec3::new(0.0, 0.0, SQRT_2),
                Vec3::new(-1.0, 1.0, 0.0),
                Vec3::new(1.0, 1.0, 0.0),
                Vec3::new(-1.0, -1.0, 0.0),
                Vec3::new(1.0, -1.0, 0.0),
                Vec3::new(0.0, 0.0, -SQRT_2),
            ],
            uvs: vec![(0.0, 0.0); 6],
            ns: vec![
                Vec3::khat(),
                Vec3::new(-1.0, 1.0, 0.0).normalize(),
                Vec3::new(1.0, 1.0, 0.0).normalize(),
                Vec3::new(-1.0, -1.0, 0.0).normalize(),
                Vec3::new(1.0, -1.0, 0.0).normalize(),
                -Vec3::khat(),
            ],
            ts: vec![
                [0, 3, 4], [0, 4, 2], [0, 2, 1], [0, 1, 3],
                [5, 4, 3], [5, 2, 4], [5, 1, 2], [5, 3, 1]
            ],
            material: 0
        };
        let t0 = Tri3 { i: 0, mesh: &tm1 };
        let t1 = Tri3 { i: 1, mesh: &tm1 };
        let t2 = Tri3 { i: 2, mesh: &tm1 };
        let t3 = Tri3 { i: 3, mesh: &tm1 };
        let t4 = Tri3 { i: 4, mesh: &tm1 };
        let t5 = Tri3 { i: 5, mesh: &tm1 };
        let t6 = Tri3 { i: 6, mesh: &tm1 };
        let t7 = Tri3 { i: 7, mesh: &tm1 };
        // let sphere1 = Sphere::default();
 
        let cam = Camera::new(
            160 * 1, 90 * 1,
            1.0, 9.0 / 16.0, 1.0,
            Mat::identity(4)
                .rotate((1.0 / 2.0) * PI, (1.0 / 8.0) * PI, 0.0)
                .translate(5.0, 0.0, -2.0)
        );
        let s = Scene::new(
            vec![Box::new(&t0), Box::new(&t1), Box::new(&t2), Box::new(&t3), Box::new(&t4), Box::new(&t5), Box::new(&t6), Box::new(&t7)],
            // vec![Box::new(&sphere1)],
            vec![
            Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(0.0, -5.0, 3.0))),
            Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(-5.0, -5.0, -5.0))),
            ],
            cam,
            vec![
                Material::default(),
                Material::new(crate::material::OneOrTexture::One(Color { r: 1.0, g: 0.0, b: 0.0 }), crate::material::OneOrTexture::One(0.0), 0.9, 0.9, 10.0, crate::material::OneOrTexture::One(1.0))
            ]
        );
        s.render("out.png")
    }

    #[test]
    fn test_of_tri_mesh_created_by_hand2() {
        let tm1 = TriMesh {
            xyzs: vec![
                Vec3::new(0.0, 5.0, 0.0),
                Vec3::new(2.0, 10.0, 5.0),
                Vec3::new(3.0, 6.0, 3.0),
                Vec3::new(6.0, 8.0, 5.0),
                Vec3::new(7.0, 12.0, 4.0),
                Vec3::new(8.0, 3.0, 1.0),
                Vec3::new(12.0, 5.0, -4.0),
                Vec3::new(14.0, 9.0, 2.0),
                Vec3::new(16.0, 0.0, 3.0),
            ],
            uvs: vec![(0.0, 0.0); 9],
            ns: vec![Vec3::jhat(); 9],
            ts: vec![
                [0, 2, 1], [2, 3, 1], [5, 3, 2], [5, 4, 3],
                [5, 6, 4], [6, 7, 4], [8, 7, 6]
            ],
            material: 0
        };
        let t0 = Tri3 { i: 0, mesh: &tm1 };
        let t1 = Tri3 { i: 1, mesh: &tm1 };
        let t2 = Tri3 { i: 2, mesh: &tm1 };
        let t3 = Tri3 { i: 3, mesh: &tm1 };
        let t4 = Tri3 { i: 4, mesh: &tm1 };
        let t5 = Tri3 { i: 5, mesh: &tm1 };
        let t6 = Tri3 { i: 6, mesh: &tm1 };
        // let sphere1 = Sphere::default();
 
        let cam = Camera::new(
            160 * 1, 90 * 1,
            1.0, 9.0 / 16.0, 1.0,
            Mat::identity(4)
                .rotate(0.0, -(1.0 / 2.0) * PI, 0.0)
                .translate(9.0, 6.0, 25.0)
        );
        let s = Scene::new(
            vec![Box::new(&t0), Box::new(&t1), Box::new(&t2), Box::new(&t3), Box::new(&t4), Box::new(&t5), Box::new(&t6)],
            vec![
            // Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(0.0, -5.0, 3.0))),
            Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(9.0, 6.0, 25.0))),
            ],
            cam,
            vec![Material::default()]
        );
        s.render("out.png")
    }
    */

    #[test]
    fn obj_parser() {
        let mesh = TriMesh::from_obj_file("of1.obj");
        println!("verticies len: {:?}, ts len: {:?}", mesh.verticies.len(), mesh.ts.len());
        println!("mesh: {:?}", mesh);
        let objects: Vec<Box<dyn Object>> = (0..mesh.ts.len())
            .map(|i| Box::new(Tri3 { i, mesh: &mesh }) as Box<dyn Object>).collect();


        let cam = Camera::new(
            160 * 1, 90 * 1,
            1.0, 9.0 / 16.0, 1.0,
            Mat::identity(4)
            .rotate(0.0, -(1.0 / 2.0) * PI, 0.0)
            .translate(9.0, 6.0, 25.0)
        );
        let s = Scene::new(
            // &objects,
            objects,
            vec![
                // Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(0.0, -5.0, 3.0))),
                Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(9.0, 6.0, 25.0))),
                ],
                cam,
            vec![Material::default()],
            Color::black()
        );
        s.render("out.png")
    }
}
