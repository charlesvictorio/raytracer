use crate::core_math::feq;
use crate::vec3::Vec3;
use vecspace::VectorSpace;
use crate::color::Color;
use crate::matrix::Mat;
use crate::ray::Ray;
use crate::object::{Object, Intersection};
use crate::material::Material;
use crate::aabb::Aabb;

// Rectangular prism
#[derive(Debug)]
pub struct Cube {
    // matrix to transform points/vecs/rays from the object space of this plane into world space.
    pub o2w: Mat,
    // matrix to do the inverse
    pub w2o: Mat,
    // pub aabb: Aabb,
    pub material: usize,
    side_len: usize, // number of pixels of one side of one cube of its texture
}

#[derive(Debug)]
enum Face {
    Front, Back, Up, Down, Left, Right,
}
// Object space is a cube, centered at origin, apothems are 1.
// {(x, y, z) : -1 < x < 1, -1 < y < 1, -1 < z < 1}
impl Cube {
    pub fn new(o2w: Mat, material: usize, side_len: usize) -> Self {
        let w2o = o2w.inverse().unwrap();
        Self { o2w, w2o, /*aabb,*/ material, side_len }
    }

    // edit: should this be "is *in* cube"
    pub fn local_is_on_cube(&self, point: Vec3) -> bool {
        return
            // Check if x, y, and z coords are within (-1, 1)
            point.x * point.x < 1.0
            && point.y * point.y < 1.0
            && point.z * point.z < 1.0 
    }
    // Return the t values of the intersections (sorted nearest-furthest) between the ray and the local cube, if they exist
    // Doesn't need to be an inner func, only used once.
    // The furthest intersection is never used.
    pub fn local_ray_cube_ints(&self, ray: &Ray) -> Option<(f64, f64)> {
        // Early exits. If the ray starts to the left of the negative plane, and goes left, than it won't intersect the cube. And so on...
        // Not sure if this is worth it
        if
               (ray.start.x <= -1.0 && ray.dir.x <= 0.0)
            || (ray.start.x >=  1.0 && ray.dir.x >= 0.0)
            || (ray.start.y <= -1.0 && ray.dir.y <= 0.0)
            || (ray.start.y >=  1.0 && ray.dir.y >= 0.0)
            || (ray.start.z <= -1.0 && ray.dir.z <= 0.0)
            || (ray.start.z >=  1.0 && ray.dir.z >= 0.0)
        {
            return None;
        }
        let (xtmin, xtmax) = Cube::check_axis(ray.start.x, ray.dir.x);
        let (ytmin, ytmax) = Cube::check_axis(ray.start.y, ray.dir.y);
        let (ztmin, ztmax) = Cube::check_axis(ray.start.z, ray.dir.z);
        // intersection t values are the furthest intersection of the nearest of the two pair planes, and the nearest intersection of the farthest of the two pair planes.
        // largest minimum and smallest maximum
        let tmin = f64::max(f64::max(xtmin, ytmin), ztmin);
        let tmax = f64::min(f64::min(xtmax, ytmax), ztmax);
        if tmin > tmax {
            return None;
        }
        return Some((tmin, tmax));
    }
    // For each axis, see what values of t intersect the x = -1 and x = 1 planes
    fn check_axis(start_coord: f64, dir_coord: f64) -> (f64, f64) {
        let tmin_num = -1.0 - start_coord;
        let tmax_num = 1.0 - start_coord;
        let mut tmin: f64;
        let mut tmax: f64; 
        if !feq(dir_coord, 0.0) {
            tmin = tmin_num / dir_coord;
            tmax = tmax_num / dir_coord;
        } else {
            // There is no intersection with this axis's planes.
            // + or - infty depending on sign of numerator
            tmin = tmin_num * f64::INFINITY;
            tmax = tmax_num * f64::INFINITY;
        }
        if tmin > tmax {
            (tmin, tmax) = (tmax, tmin);
        }
        return (tmin, tmax);
    }

    // Get the face of the cube that the point is on
    // point is on surface of cube, in obj space
    fn face_from_point(p: Vec3) -> Face {
        /*
        let abs_x = p.x.abs();
        let abs_y = p.y.abs();
        let abs_z = p.z.abs();
        let face_coord = abs_x.max(abs_y).max(abs_z);
        if feq(face_coord, p.x) {
            Face::Right
        } else if feq(face_coord, -p.x) {
            Face::Left
        } else if feq(face_coord, p.z) {
            Face::Up
        } else if feq(face_coord, -p.z) {
            Face::Down
        } else if feq(face_coord, -p.y) {
            Face::Front
        } else { // p.y
            Face::Back
        }
        */
        // Edges and corners have 2 and 3 possible choices respectively,
        // But textures should be seamless
        // Also, no matter which choice you make, the edge is too small to matter
        if feq(p.x, 1.0) {
            Face::Right
        } else if feq(p.x, -1.0) {
            Face::Left
        } else if feq(p.y, 1.0) {
            Face::Back
        } else if feq(p.y, -1.0) {
            Face::Front
        } else if feq(p.z, 1.0) {
            Face::Up
        // } else { // p.z == 1.0
        //     Face::Down
        // }
        } else if feq(p.z, -1.0) {
            Face::Down
        } else {
            panic!("Shouldn't reach here.")
        }
    }

    // min max normalization
    // given start and end of interval, and x within interval,
    // find t such that start + t * (end - start) = x
    // inverse of Lerp?
    // output u or v within [0, 1]
    fn min_max_norm(num: f64, start: f64, end: f64) -> f64 {
        (num - start) / (end - start)
    }

    // switched = false: axis goes from start = -1 to end = 1
    // (num - start) / (end - start) = (num - -1) / (1 - -1)
    // = (num + 1) / 2 = (1/2) * num + (1/2)
    // switched = true: axis goes from start = 1 to end = -1
    // (num - start) / (end - start) = (num - 1) / (-1 - 1)
    // = (num - 1) / -2 = (-1/2) * num + (1/2)
    fn min_max_norm_specific(num: f64, switched: bool) -> f64 {
        (if switched {-0.5} else {0.5}) * num + 0.5
    }

    // given the uv coords of the bottom left corner of the face
    // and the uv coords of the point rel to the bottom left corner
    // find the abs uv coords of the point
    // coords of point = coords of corner + coords of point rel to corner
    // ...switched: does the axis go from -1 to 1 or 1 to -1
    fn uv_at_inner(&self, corner_u: f64, corner_v: f64, rel_u: f64, rel_v: f64, u_switched: bool, v_switched: bool) -> (f64, f64) {
        let l = self.side_len as f64;
        let max_u = l * 3.0;
        let max_v = l * 2.0;
        let u = (corner_u + Cube::min_max_norm_specific(rel_u, u_switched) * l) / max_u;
        let v = (corner_v + Cube::min_max_norm_specific(rel_v, v_switched) * l) / max_v;
        (u, v)
    }

}

impl Object for Cube {
    fn get_aabb(&self) -> Aabb {
        let aabb_in_os = Aabb::new(
            Vec3::new(-1.0, -1.0, -1.0),
            Vec3::new(1.0, 1.0, 1.0)
        );
        let aabb = aabb_in_os.transform(&self.o2w);
        return aabb;
    }
    fn get_centroid(&self) -> Vec3 {
        // \vec{0} is the centroid in object space, and I think o2w(centroid(shape)) ==
        // centroid(o2w(shape))
        Vec3::zero().transform(&self.o2w) // so the rightmost column, minus the bottom one 
    }

    fn is_hit_by(&self, ray: &Ray) -> Option<f64> {
        let ray_os = ray.transform(&self.w2o);
        // Finding the first intersection takes just as much work as finding both.
        self.local_ray_cube_ints(&ray_os)
            .map(|(nearest_t, _)| nearest_t)
    }

    // uses default intersection() (making it a default implementation in impl Object {} doesnt work tho)
    fn intersection(&self, ray: &Ray) -> Option<Intersection> {
        // default implementation
        self.is_hit_by(ray)
        .map(|t| {
            let p = ray.plug_in_t(t);
            Intersection {
                p,
                uv: self.uv_at(p),
                norm: self.norm_at(p),
                wo: -ray.dir.normalize(), // I know for a fact that ray.dir isn't always unit length
                obj: self,
                t
            }
        })
    }

    fn get_material_i(&self) -> usize {
        self.material
    }
    fn uv_at(&self, point: Vec3) -> (f64, f64) {
        let point_os = point.transform(&self.w2o);
        // can be microoptimized:
        // min_max_norm can be changed to either use -1 - 1, or 1 - -1.
        // and division can be stored

        let l = self.side_len as f64;
        println!("right before error: point: {:?}, point_os: {:?}", point, point_os);
        match Cube::face_from_point(point_os) {
            Face::Right => {
                // (Cube::min_max_norm(point_os.y, -1.0, 1.0),
                //  Cube::min_max_norm(point_os.z, -1.0, 1.0))
                self.uv_at_inner(l, 0.0, point_os.y, point_os.z, false, false)
            },
            Face::Left => {
                // (Cube::min_max_norm(point_os.y, 1.0, -1.0),
                //  Cube::min_max_norm(point_os.z, -1.0, 1.0))
                self.uv_at_inner(0.0, 0.0, point_os.y, point_os.z, true, false)
            },
            Face::Up => {
                // (Cube::min_max_norm(point_os.x, -1.0, 1.0),
                //  Cube::min_max_norm(point_os.y, -1.0, 1.0))
                self.uv_at_inner(2.0 * l, l, point_os.x, point_os.y, false, false)
            },
            Face::Down => {
                // (Cube::min_max_norm(point_os.x, -1.0, 1.0),
                //  Cube::min_max_norm(point_os.y, 1.0, -1.0))
                self.uv_at_inner(2.0 * l, 0.0, point_os.x, point_os.y, false, true)
            },
            Face::Front => {
                // (Cube::min_max_norm(point_os.x, -1.0, 1.0),
                //  Cube::min_max_norm(point_os.z, -1.0, 1.0))
                self.uv_at_inner(0.0, l, point_os.x, point_os.z, false, false)
            },
            Face::Back => {
                // (Cube::min_max_norm(point_os.x, 1.0, -1.0),
                //  Cube::min_max_norm(point_os.z, -1.0, 1.0))
                self.uv_at_inner(l, l, point_os.x, point_os.z, true, false)
            }
        }
    }
    fn norm_at(&self, point: Vec3) -> Vec3 {
        let p_os = point.transform(&self.w2o);
        let maxc = p_os.x.abs()
            .max(p_os.y.abs())
            .max(p_os.z.abs());
        let norm_os = {
            if maxc == p_os.x.abs() {
                Vec3::new(p_os.x, 0.0, 0.0)
            } else if maxc == p_os.y.abs() {
                Vec3::new(0.0, p_os.y, 0.0)
            } else {
                Vec3::new(0.0, 0.0, p_os.z)
            }
        };
        // multiply norms by inverse transpose of o2w
        let norm_ws = norm_os.transform(&self.w2o.transpose()).normalize();
        return norm_ws;
    }
}

impl Default for Cube {
    fn default() -> Self {
        Self::new(Mat::identity(4), 0, 100 /* for now??? */)
    }
}
