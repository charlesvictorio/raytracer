// I should write an algorithm to solve matrix-vec instead of storing both o2w and w2o? whatever is actually done
// Temp
#![allow(dead_code)]
#![allow(unused_imports)]
// #![deny(elided_lifetimes_in_paths)]

use std::f64::consts::PI;
mod color;
use color::{Color, ColorAxes};
use crate::{core_math::{QuadraticSolutions, quadratic_formula /* , EPSILON, feq */}, light2_implementations::RectAreaLight};
mod vec3;
use vec3::{Vec3, Vec3Axes};
mod core_math;
mod matrix;
use matrix::Mat;
mod transformation_matrix;
mod ray;
use ray::Ray;
mod object;
use object::{Intersection, Object};
mod sphere;
use sphere::Sphere;

use transformation_matrix::Mat3x3;
mod scene_and_light;
use scene_and_light::{Scene, Light};
mod camera;
use camera::Camera;

mod light2_implementations;
use light2_implementations::PointLight;
mod plane;
use plane::Plane;
mod cube;
use crate::cube::Cube;
mod aabb;
mod triangle_mesh;
mod material;
mod parametric_surface;
use material::{Material, Texture, ImageTexture};
mod probability_test1;

use image::{ImageBuffer, Rgba, Rgb, open, RgbImage};
use elsa::vec::FrozenVec;
use strum::IntoEnumIterator;

/*

// Chapter 02: Basic Raytracing ===============================================

/*
#[derive(Debug, Copy, Clone)]
pub struct Sphere {
    pub center: Vec3, // really a point. a position vector
    pub radius: f64,
    pub color: Rgb<f64>,
    pub specular: Option<i64>,
    pub reflectiveness: f64
}

impl Sphere {
    fn new(center: (f64, f64, f64), radius: f64, color: (f64, f64, f64), specular: Option<i64>, reflectiveness: f64) -> Sphere {
        Sphere {
            center: Vec3::new(center.0, center.1, center.2),
            radius,
            color: Rgb([color.0, color.1, color.2]),
            specular,
            reflectiveness
        }
    }
}
*/

//struct Canvas {
    //width: i64,
    //length: i64,
    //pixar: Vec<Rgb<f64>>
//}

//impl Canvas {
    //fn new() -> Self {
        //Self {}
    //}
//}

/*
replaced by sphere.ray_intersect() and hit()

// out: Either None if no intersection, 2 diff values if intsec at 2 diff points, or 2 close values if intsec at 1 point
// to get the coords, use the ray equation and plug in the t value(s) if they exist
// later, refactor into trait and use generics
// is coord from one t always closer than cord from other t? 
//fn compute_ray_sphere_intersection(&ray_start: &Vec3, &ray_direction: &Vec3, &sphere: &Sphere) -> Option<(f64, f64)> {
fn compute_ray_sphere_intersection(&ray_start: &Vec3, &ray_direction: &Vec3, &sphere: &Sphere) -> QuadraticSolutions {
    let sphere_center_to_ray_start: Vec3 = Vec3::sub(ray_start, sphere.center);
    let a = Vec3::dot(ray_direction, ray_direction);
    let b = 2.0 * Vec3::dot(sphere_center_to_ray_start, ray_direction);
    let c = Vec3::dot(sphere_center_to_ray_start, sphere_center_to_ray_start) - sphere.radius * sphere.radius;

    //let discriminant = b * b - 4.0 * a * c;
    //if discriminant < 0.0 {
        //return None;
    //}

    //let sqrt_disc: f64 = f64::sqrt(discriminant);
    //let t1 = (-b + sqrt_disc) / (2.0 * a);
    //let t2 = (-b - sqrt_disc) / (2.0 * a);
    //return Some((t1, t2));
    return quadratic_formula(a, b, c);
}

fn get_closest_ray_sphere_intersection<'a>(&ray_start: &Vec3, &ray_direction: &Vec3, min_t: f64, max_t: f64, spheres: &'a Vec<Sphere>) -> (Option<&'a Sphere>, f64) {
    // Return the closest sphere and value of t
    let mut closest_t: f64 = f64::INFINITY;
    let mut closest_sphere: Option<&Sphere> = None;
    for sphere in spheres.iter() {
        // need to match the output of crsi
        match compute_ray_sphere_intersection(&ray_start, &ray_direction, sphere) {
            //Some((t1, t2)) => {
            QuadraticSolutions::TwoSols(t1, t2) => {
                if t1 < closest_t && min_t < t1 && t1 < max_t {
                    closest_t = t1;
                    closest_sphere = Some(sphere);
                }
                if t2 < closest_t && min_t < t2 && t2 < max_t {
                    closest_t = t2;
                    closest_sphere = Some(sphere);
                }
            },
            QuadraticSolutions::OneSol(t1) => {
                if t1 < closest_t && min_t < t1 && t1 < max_t {
                    closest_t = t1;
                    closest_sphere = Some(sphere);
                }
            },
            //None => { continue; }
            QuadraticSolutions::ZeroSols => { continue; }
        }
    }
    (closest_sphere, closest_t)
}
*/

// vec of Thing: HasColor
// would they all need to be the same type?

//fn trace_ray<'a>(&ray_start: &Vec3, &ray_direction: &Vec3, min_t: f64, max_t: f64, spheres: &Vec<Sphere>, background_color: &'a Rgb<f64>, lights: &Vec<LightType: ComputeAddedBrightness>) -> &'a Rgb<f64> {
fn trace_ray(&ray_start: &Vec3, &ray_direction: &Vec3, min_t: f64, max_t: f64, spheres: &Vec<Sphere>, background_color: Rgb<f64>, lights: &Vec<Box<dyn ComputeAddedBrightness>>, recursion_depth: i64) -> Rgb<f64> {
//fn trace_ray<'a>(&ray_start: &Vec3, &ray_direction: &Vec3, min_t: f64, max_t: f64, spheres: &Vec<Sphere>, background_color: Rgb<f64>, lights: &Vec<Light>) -> Rgb<f64> {

    let (closest_sphere, closest_t) =  get_closest_ray_sphere_intersection(&ray_start, &ray_direction, min_t, max_t, spheres);

    match closest_sphere {
        Some(sphere) => {
            let point: Vec3 = Vec3::add(ray_start, Vec3::scale(ray_direction, closest_t));
            let normal: Vec3 = Vec3::normalize(Vec3::sub(point, sphere.center));
            let view: Vec3 = Vec3::scale(ray_direction, -1.0);
            //println!("ray_start {:?} ray_direction {:?} closest_t {} sphere.center {:?}", ray_start, ray_direction, closest_t, sphere.center);
            //println!("point {:?} normal {:?} view {:?}", point, normal, view);
            let lighting: f64 = compute_lighting(point, normal, view, sphere.specular, lights, spheres);
            //let lighting: f64 = compute_lighting_enums(point, normal, view, sphere.specular, lights);
            //println!("lighting {}", lighting);
            let local_color: Rgb<f64> = scale_color(sphere.color, lighting);

            //let output: Rgb<f64> = scale_and_clamp_color(sphere.color, lighting);
            //println!("lighting = {}, output = {:?}", lighting, output);
            //return output;
            
            // reflective
            if sphere.reflectiveness <= 0.0 || recursion_depth <= 0 {
                return local_color;
            }

            let reflected_ray: Vec3 = Vec3::reflect(view, normal);
            let reflected_color: Rgb<f64> = trace_ray(&point, &reflected_ray, 0.001, f64::INFINITY, &spheres, background_color, &lights, recursion_depth - 1);

            return add_color(
                scale_color(local_color, 1.0 - sphere.reflectiveness),
                scale_color(reflected_color, sphere.reflectiveness)
            );

        },
        None => return background_color,
    }
}

// Chapter 03: Light ==========================================================


#[derive(Debug)]
struct AmbientLight { intensity: f64 }
#[derive(Debug)]
struct PointLight { intensity: f64, position: Vec3 }
#[derive(Debug)]
struct DirectionalLight { intensity: f64, direction: Vec3 }

trait ComputeAddedBrightness {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64;
}

impl ComputeAddedBrightness for AmbientLight {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 { self.intensity }
}

fn compute_added_brightness_inner(point: Vec3, light_intensity: f64, point_to_light: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 {

    let (sphere_in_the_way, closest_t) = get_closest_ray_sphere_intersection(&point, &point_to_light, 0.001, f64::INFINITY, spheres);
    if let Some(sphere) = sphere_in_the_way {
        return 0.0;
    }

    // Diffuse reflection
    let n_dot_l = Vec3::dot(normal, point_to_light);
    //println!("n_dot_l {:?}", n_dot_l);

    let intensity_from_diffuse_reflection: f64 = {
        if n_dot_l > 0.0 {
            let cos_of_angle_btwn_normal_and_light_vecs: f64 = n_dot_l / (Vec3::length(normal) * Vec3::length(point_to_light));
            //added_intensity += light_intensity * cos_of_angle_btwn_normal_and_light_vecs;
            //println!("n_dot_l > 0, and intensity added is {}", light_intensity * cos_of_angle_btwn_normal_and_light_vecs);
            light_intensity * cos_of_angle_btwn_normal_and_light_vecs
        } else { 0.0 }
    };

    // Specular reflection
    let intensity_from_specular_reflection: f64 = {
        match specular {
            Some(s) => {
                let light_reflection: Vec3 = {
                    // component of point_to_line that is parallel to normal
                    // point_to_line projected onto normal
                    // proj_{\vec{b}} \vec{a} = \frac{\vec{a} \cdot \vec{b}}{\lVert \vec{b} \rVert}
                    // * \frac{\vec{b}}{\lVert \vec{b} \rVert}
                    // In this case, \vec{b} is a unit vector
                    let p2l_n_len: f64 = Vec3::dot(point_to_light, normal);
                    let p2l_n: Vec3 = Vec3::scale(normal, p2l_n_len);

                    Vec3::sub(Vec3::scale(p2l_n, 2.0), point_to_light)
                };
                let r_dot_v = Vec3::dot(light_reflection, view);
                //println!("vec_r {:?} r_dot_v {:?}", light_reflection, r_dot_v);
                if r_dot_v > 0.0 {
                    let cos_of_angle_btwn_reflection_and_view = r_dot_v / (Vec3::length(light_reflection) * Vec3::length(view));
                    //println!("n_dot_l > 0, and intensity added is {}", light_intensity * f64::powf(cos_of_angle_btwn_reflection_and_view, s as f64));
                    light_intensity * f64::powf(cos_of_angle_btwn_reflection_and_view, s as f64)
                } else { 0.0 }
            },
            None => { 0.0 }
        }
    };
    intensity_from_diffuse_reflection + intensity_from_specular_reflection
}

impl ComputeAddedBrightness for PointLight {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 {
        let point_to_light: Vec3 = Vec3::sub(self.position, point);
        compute_added_brightness_inner(point, self.intensity, point_to_light, normal, view, specular, spheres)
    }
}

impl ComputeAddedBrightness for DirectionalLight {
    fn compute_added_brightness(&self, point: Vec3, normal: Vec3, view: Vec3, specular: Option<i64>, spheres: &Vec<Sphere>) -> f64 {
        // the direction of `DirectionalLight`s go from object to light source (point to light)
        compute_added_brightness_inner(point, self.intensity, self.direction, normal, view, specular, spheres)
    }
}

//fn compute_lighting<LightType: ComputeAddedBrightness>(
fn compute_lighting(
    point: Vec3, // The point (eg on a sphere) that light (maybe) touches and colors
    normal: Vec3, // The unit normal vector, perpendicular to the surface at that point
    view: Vec3, // A vector from the point to the camera (i think)
    specular: Option<i64>, // The shininess of an object
    lights: &Vec<Box<dyn ComputeAddedBrightness>>, // The lights that will be illuminating that point
    spheres: &Vec<Sphere>
) -> f64 {
    // Compute how bright a surface should be at a point
    let mut total_intensity: f64 = 0.0;
    //let length_n: f64 = Vec3::length(normal);
    //let length_v: f64 = Vec3::length(view);

    for light in lights.iter() {
        total_intensity += light.compute_added_brightness(point, normal, view, specular, spheres);
    }
    total_intensity
}

//#[derive(Debug)]
//enum Light { // possible idea
    //Ambient(AmbientLight),
    //Point(PointLight),
    //Directional(DirectionalLight)
//}

//fn compute_lighting_enums(
    //point: Vec3, // The point (eg on a sphere) that light (maybe) touches and colors
    //normal: Vec3, // The unit normal vector, perpendicular to the surface at that point
    //view: Vec3, // A vector from the point to the camera (i think)
    //specular: Option<i64>, // The shininess of an object
    ////lights: &Vec<Box<dyn ComputeAddedBrightness>> // The lights that will be illuminating that point
    //lights: &Vec<Light> // The lights that will be illuminating that point
//) -> f64 {
    //// Compute how bright a surface should be at a point
    //let mut total_intensity: f64 = 0.0;

    //for light in lights.iter() {
        ////println!("light {:?}", light);
        // // Note: Can make this part better with `if let`
        // // https://doc.rust-lang.org/rust-by-example/flow_control/if_let.html
        //match light {
            //Light::Ambient(AmbientLight{intensity}) => {
                //total_intensity += intensity;
                ////println!("ambient added {}", intensity);
            //}
            //Light::Point(PointLight{intensity, position}) => {
                ////println!("point...");
                //let point_to_light: Vec3 = Vec3::sub(*position, point);
                //total_intensity += compute_added_brightness_inner(*intensity, point_to_light, normal, view, specular);
            //}
            //Light::Directional(DirectionalLight{intensity, direction}) => {
                ////println!("directional...");
                //total_intensity += compute_added_brightness_inner(*intensity, *direction, normal, view, specular);
            //}
        //}
    //}
    //total_intensity
//}
    
// =========


//struct Object;

//struct Scene {
    //objects: Vec<&Object>, // Vec<&T: can calculate intersections and normals>
    //lights: Vec<&Lights>,
//}

fn get_coord_color(
        //x: i64, y: i64,
        x: i64, z: i64,
        canvas_width: f64, canvas_height: f64, viewport_width: f64, viewport_height: f64, viewport_depth: f64,
        &ray_start: &Vec3, min_t: f64, max_t: f64, spheres: &Vec<Sphere>, background_color: Rgb<f64>, lights: &Vec<Box<dyn ComputeAddedBrightness>>
    ) -> Rgba<u8> {
    //let ray_direction: Vec3 = Vec3::from_tuple(canvas_x_y_to_viewport_tuple(x as f64, y as f64, canvas_width, canvas_height, viewport_width, viewport_height, viewport_depth));
    //let ray_direction: Vec3 = Vec3::from_tuple(canvas_x_z_to_viewport_tuple(x as f64, z as f64, canvas_width, canvas_height, viewport_width, viewport_height, viewport_depth));
    let ray_direction: Vec3 = canvas_x_z_to_viewport_vec3(x as f64, z as f64, canvas_width, canvas_height, viewport_width, viewport_height, viewport_depth);
    let theoretical_color_at_pixel: Rgb<f64> = trace_ray(&ray_start, &ray_direction, min_t, max_t, spheres, background_color, lights, 3);
    let practical_color_at_pixel: Rgba<u8> = theoretical_color_to_practical_color(theoretical_color_at_pixel);

    practical_color_at_pixel
}

*/

fn main() {
    let n = 1;
    let cam = Camera::new(
        160 * n, 90 * n,
        1.0, 9.0 / 16.0, 1.0,
        Mat::identity(4)
            .rotate((1.0 / 8.0) * PI, (-1.0 / 8.0) * PI, 0.0)
            .translate(3.0, -10.0, 3.0)
    );

    let mut mtls: Vec<Material> = vec![Material::default()]; // mtls[0] is the default material

    mtls.push(Material::new(Texture::One(Color::new(1.0, 0.2, 1.0)), Texture::One(0.2), 0.9, 0.9, 200.0, Texture::One(0.0)));
    let s1 = Sphere::new(
        Mat::identity(4),
        mtls.len() - 1
    );

    let align_check_cube_texture = ImageTexture::<Color>::from_file("textures/cube_texture2.jpg");
    // panic!("{:?}", align_check_cube_texture.uv2texel(0.0006, 0.5));
    mtls.push(Material::new(Texture::Image(align_check_cube_texture), Texture::One(0.2), 0.9, 0.9, 200.0, Texture::One(0.0)));
    let cube2 = Cube::new(
        Mat::identity(4)
            .scale(2.0, 2.0, 2.0)
            // .rotate(-1.1 * PI, -0.1 * PI, 0.0)
            .translate(-2.0, 0.0, -1.0),
        mtls.len() - 1,
        450
    );

    mtls.push(Material::new(Texture::One(Color::new(0.0, 0.5, 0.5)), Texture::One(0.2), 0.9, 0.9, 200.0, Texture::One(0.0)));
    let s2 = Sphere::new(
        Mat::identity(4).scale(0.25, 0.25, 0.25).translate(0.0, -2.0, 0.3),
        mtls.len() - 1
    );
    mtls.push(Material::new(Texture::One(Color::new(232.0 / 255.0, 172.0 / 255.0,104.0 / 255.0)), Texture::One(0.2), 0.9, 0.9, 200.0, Texture::One(0.0)));
    let s3 = Sphere::new(
        Mat::identity(4).scale(100.0, 100.0, 0.5).translate(0.0, 0.0, -3.0),
        mtls.len() - 1
    );
    
    mtls.push(Material::new(Texture::One(Color::new(179.0 / 255.0, 26.0 / 255.0, 71.0 / 255.0)), Texture::One(0.2), 0.9, 0.5, 10.0, Texture::One(0.0)));
    let s4 = Sphere::new(
        Mat::identity(4).scale(0.5, 0.5, 0.5).translate(-1.0, -0.5, -1.0),
        mtls.len() - 1
    );
    // let wood_t = image::open("textures/Wood.jpg").expect("failed").to_rgb8();
    // mtls.push(Material::new(Color::new(1.0, 0.0, 0.0), 0.2, 0.9, 0.9, 200.0, 0.5,
    // Some(wood_t)));
    // let p1 = Plane::new(
    //     Plane::o2w_from_p0_rot_a(
    //         Vec3::new(-5.0, 0.0, 0.0),
    //         (1.0 / 4.0) * PI, (1.0 / 2.0) * PI, 0.0, 
    //         5.0, 5.0
    //     ),
    //     mtls.len() - 1
    // );
    
    // Comment out the scene I'm not using so I only move mtls into one scene.
    /*
    let scene = Scene::new(
        vec![
            Box::new(&s1),
            Box::new(&s2),
            // Box::new(&s3),
            Box::new(&s4),
            // Box::new(&p1),
        ],
        vec![
            // Box::new(PointLight::new(0.8, Vec3::new(-10.0, -10.0, 10.0)))
            Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(-10.0, -10.0, 10.0))),
            /*
            Box::new(RectAreaLight::new(
                Vec3::new(-10.0, -10.0, 10.0),
                Vec3::new(-1.0, 1.0, 0.0).normalize().scale(2.0),
                Vec3::new(0.0, 0.0, 2.0),
                16,
                Color::white()
            ))
            */
        ],
        mtls
    );
    */

    /*
    mtls.push(Material::new(Color::new(0.0, 0.0, 1.0), 0.2, 0.9, 0.9, 10.0, 0.5, None));
    let s5 = Sphere::new(
    Mat::identity(4).translate(2.0, 2.0, 0.0),
    mtls.len() - 1
    );
    mtls.push(Material::new(Color::new(0.0, 1.0, 0.0), 0.2, 0.9, 0.9, 10.0, 0.5, None));
    let s6 = Sphere::new(
    Mat::identity(4).translate(-2.0, 2.0, 0.0),
        mtls.len() - 1
    );
    mtls.push(Material::new(
    Color::new(247.0 / 255.0, 110.0 / 255.0, 0.0 / 255.0), 0.2, 0.9, 0.9, 200.0, 0.5, None
    ));
    let c1 = Cube::new(
    Mat::identity(4)
            // .translate(-0.5, 2.0, 0.0)
            // .scale(2.0, 0.5, 2.0)
            .rotate(PI * (1.0 / 16.0), PI * (-2.0 / 16.0), 0.0),
        mtls.len() - 1
    );
    mtls.push(Material::default());
    let t1 = Triangle::new(
    Vec3::new(-3.0, 10.0, 1.0),
    Vec3::new(0.0, -4.0, -2.0),
    Vec3::new(2.0, 1.0, 0.0),
    mtls.len() - 1
    );
    */
    let objects_arena: Vec<Box<dyn Object>> = vec![
            Box::new(s1),

            Box::new(s2),
            Box::new(s3),
            Box::new(s4),
            // Box::new(s5),
            // Box::new(s6),
            // Box::new(p1),
            // Box::new(c1),
            // Box::new(t1),
            Box::new(cube2),
    ];
    let scene2 = Scene::new(
        // &objects_arena,
        objects_arena,
        vec![
            Box::new(PointLight::new(Color::new(1.0, 1.0, 1.0), Vec3::new(0.0, -5.0, 3.0))),
        ],
        cam,
        mtls,
        Color::black()
    );

    scene2.render("out.png");
    println!("{:?}",
        &scene2.color_at_ray(&scene2.camera.cam_ray_for_pixel(100, 40), 1.0, 3)   
    );


    /*
    let mut imgbuf = ImageBuffer::new(160, 90);
    for (col, row, pixel) in imgbuf.enumerate_pixels_mut() {
        let camera_ray: Ray = cam.cam_ray_for_pixel(col, row);
        let theoretical_color_at_pixel: Color = match t1.get_first_ray_obj_int(&camera_ray) {
            Some(_) => Color::new(1.0, 0.0, 0.0),
            None => Color::black()
        };
        let practical_color_at_pixel: Rgba<u8> = Color::theoretical_color_to_practical_color(theoretical_color_at_pixel);
    
        *pixel = practical_color_at_pixel;
        println!("0 {} {} {:?}", row, col, practical_color_at_pixel);
    }
    // Remember, this is relative to the terminal's cwd!
    imgbuf.save("out.png").unwrap();
    */

/*
    let viewport_size: f64 = 1.0;
    let viewport_depth: f64 = 1.0;
    //let camera_position: Vec3 = Vec3::new(0.0, 0.0, 0.0);
    let camera_position: Vec3 = Vec3::new(0.0, 8.0, 3.0);
    //let background_color: Rgb<f64> = Rgb([255.0, 255.0, 255.0]);
    let background_color: Rgb<f64> = Rgb([0.0, 0.0, 0.0]);
    let spheres: Vec<Sphere> = vec![
        //Sphere::new((0.0, -1.0, 3.0), 1.0, (255.0, 0.0, 0.0), Some(500), 0.2),
        Sphere::new((0.0, 3.0, -1.0), 1.0, (255.0, 0.0, 0.0), Some(500), 0.2),
        //Sphere::new((2.0, 0.0, 4.0), 1.0, (0.0, 0.0, 255.0), Some(500), 0.4),
        Sphere::new((2.0, 4.0, 0.0), 1.0, (0.0, 0.0, 255.0), Some(500), 0.4),
        //Sphere::new((-2.0, 0.0, 4.0), 1.0, (0.0, 255.0, 0.0), Some(10), 0.3),
        Sphere::new((-2.0, 4.0, 0.0), 1.0, (0.0, 255.0, 0.0), Some(10), 0.3),
        Sphere::new((0.0, 0.0, -5001.0), 5000.0, (255.0, 255.0, 0.0), Some(1000), 0.5),
    ];

    let stuff: &Vec<Sphere> = &spheres;
    let lights: Vec<Box<dyn ComputeAddedBrightness>> = vec![
        Box::new(AmbientLight { intensity: 0.2 }),
        //Box::new(PointLight { intensity: 0.6, position: Vec3::new(2.0, 1.0, 0.0) }),
        Box::new(PointLight { intensity: 0.6, position: Vec3::new(2.0, 0.0, 1.0) }),
        Box::new(DirectionalLight { intensity: 0.2, direction: Vec3::new(1.0, 4.0, 4.0) }),
    ];
    //let lights: Vec<Light> = vec![
        //Light::Ambient(AmbientLight { intensity: 0.2 }),
        //Light::Point(PointLight { intensity: 0.6, position: Vec3::new(2.0, 1.0, 0.0) }),
        //Light::Directional(DirectionalLight { intensity: 0.2, direction: Vec3::new(1.0, 4.0, 4.0) }),
    //];

    const EPSILON: f64 = 0.001;
    //const recursion_depth: i64 = 3;

    let canvas_width: u32 = 600;
    let canvas_height: u32 = 600;
    let mut imgbuf = ImageBuffer::new(canvas_width, canvas_height);

    //let rotation: Quat = Quat::from_axis_angle(Vec3::new(1.0, 0.0, 0.0), 1.0 * PI / 3.0);
    //let rotation: Quat = Quat::new(1.0, 0.0, 0.0, 0.0);

    let rotation_mat: Mat3x3 = Mat3x3::get_rotation_mat(PI, -PI / 6.0, PI / 12.0);

    for (col, row, pixel) in imgbuf.enumerate_pixels_mut() {
        //let (canvas_x, canvas_y) = col_row_to_canvas_x_y(col, row, canvas_width, canvas_height);
        let (canvas_x, canvas_z) = col_row_to_canvas_x_z(col, row, canvas_width, canvas_height);
        //let camera_direction: Vec3 = Vec3::from_tuple(canvas_x_y_to_viewport_tuple(canvas_x, canvas_y, canvas_width.into(), canvas_height.into(), viewport_size, viewport_size, viewport_depth));
        //let camera_direction_relative_to_camera_pyramid: Vec3 = Vec3::from_tuple(canvas_x_y_to_viewport_tuple(canvas_x, canvas_y, canvas_width.into(), canvas_height.into(), viewport_size, viewport_size, viewport_depth));
        //let camera_direction_relative_to_camera_pyramid: Vec3 = Vec3::from_tuple(canvas_x_z_to_viewport_tuple(canvas_x, canvas_z, canvas_width.into(), canvas_height.into(), viewport_size, viewport_size, viewport_depth));
        let camera_direction_relative_to_camera_pyramid: Vec3 = canvas_x_z_to_viewport_vec3(canvas_x, canvas_z, canvas_width.into(), canvas_height.into(), viewport_size, viewport_size, viewport_depth);
        //let camera_direction: Vec3 = Quat::rotate_vec(camera_direction_relative_to_camera_pyramid, rotation);
        let camera_direction: Vec3 = Mat3x3::mv_mul(rotation_mat, camera_direction_relative_to_camera_pyramid);

        let theoretical_color_at_pixel: Rgb<f64> = trace_ray(&camera_position, &camera_direction, viewport_depth, f64::INFINITY, stuff, background_color, &lights, 3);
        let practical_color_at_pixel: Rgba<u8> = theoretical_color_to_practical_color(theoretical_color_at_pixel);
    
        *pixel = practical_color_at_pixel;
        println!("{} {} {:?}", row, col, practical_color_at_pixel);
    }
    // remember, this is relative to the terminal's cwd!
    //imgbuf.save("../out.png").unwrap();
    imgbuf.save("out.png").unwrap();

    
    let c: Rgba<u8> = get_coord_color(
        300, -300,
        canvas_width as f64, canvas_height as f64, viewport_size, viewport_size, viewport_depth,
        &camera_position, viewport_depth, f64::INFINITY, stuff, background_color, &lights
    );
    println!("Color at (300, -300): {:?}", c);

*/

}
