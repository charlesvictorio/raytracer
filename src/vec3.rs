// Linear algebra helper stuff
// also quaternions

use std::fmt;
use std::ops::{Add, Sub, Mul, Neg, Index};

use constructor::Constructor;
use strum_macros::EnumIter;
use vecspace::VectorSpace;
use vecspace::common_vec;
use vecspace_derive::VectorSpace;

use crate::core_math::feq;

#[derive(Debug, Copy, Clone, Default, Constructor, VectorSpace)]
pub struct Vec3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vec3 {
    pub fn cross(v1: Vec3, v2: Vec3) -> Vec3 {
        Vec3 {
            x: v1.y * v2.z - v1.z * v2.y,
            y: v1.z * v2.x - v1.x * v2.z,
            z: v1.x * v2.y - v1.y * v2.x
        }
    }
    // Scalar Triple Product
    // a \cdot (b \times c)
    pub fn s_trip_prod(a: Vec3, b: Vec3, c: Vec3) -> f64 {
        a * Vec3::cross(b, c)
    }
    common_vec!(Vec3 zero = <0.0, 0.0, 0.0>);
    common_vec!(Vec3 ihat = <1.0, 0.0, 0.0>);
    common_vec!(Vec3 jhat = <0.0, 1.0, 0.0>);
    common_vec!(Vec3 khat = <0.0, 0.0, 1.0>);
}

/*
impl fmt::Display for Vec3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<{:.4}, {:.4}, {:.4}>", self.x, self.y, self.z)
    }
}

impl PartialEq for Vec3 {
    fn eq(&self, other: &Self) -> bool {
        feq(self.x, other.x) && feq(self.y, other.y) && feq(self.z, other.z)
    }
}

impl Eq for Vec3 {}

impl Add for Vec3 {
    type Output = Vec3;
    fn add(self, other: Vec3) -> Vec3 {
        Vec3 { x: self.x + other.x, y: self.y + other.y, z: self.z + other.z }
    }
}

impl Sub for Vec3 {
    type Output = Vec3;
    fn sub(self, other: Vec3) -> Vec3 {
        Vec3 { x: self.x - other.x, y: self.y - other.y, z: self.z - other.z }
    }
}

// Vec3 * Vec3 does the dot product. Vec3 * f64 and f64 * Vec3 scales the vector.
impl Mul for Vec3 {
    type Output = f64;
    fn mul(self, other: Vec3) -> f64 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }
}

impl Mul<f64> for Vec3 {
    type Output = Vec3;
    fn mul(self, c: f64) -> Vec3 {
        Vec3::new(self.x * c, self.y * c, self.z * c)
    }
}

impl Mul<Vec3> for f64 {
    type Output = Vec3;
    fn mul(self, v: Vec3) -> Vec3 {
        Vec3::new(v.x * self, v.y * self, v.z * self)
    }
}


impl Neg for Vec3 {
    type Output = Vec3;
    fn neg(self) -> Vec3 {
        Vec3 { x: -self.x, y: -self.y, z: -self.z }
    }
}

// To iterate over axes
// Maybe just have v.as_array() -> [v.x, v.y, v.z] and iterate over that array
// https://users.rust-lang.org/t/how-to-iterate-over-fields-of-struct/53356
#[derive(Copy, Clone, Debug, EnumIter)]
pub enum Axes { X, Y, Z }

impl Index<Axes> for Vec3 {
    type Output = f64;
    fn index(&self, axis: Axes) -> &Self::Output {
        match axis {
            Axes::X => &self.x,
            Axes::Y => &self.y,
            Axes::Z => &self.z,
        }
    }
}

impl Vec3 {
    pub fn new(x: f64, y: f64, z: f64) -> Vec3 {
        Vec3 { x, y, z }
    }

    /*
    pub fn scale(self, c: f64) -> Self {
        Self { x: self.x * c, y: self.y * c, z: self.z * c }
    }
    */

    pub fn cross(v1: Vec3, v2: Vec3) -> Vec3 {
        Vec3 {
            x: v1.y * v2.z - v1.z * v2.y,
            y: v1.z * v2.x - v1.x * v2.z,
            z: v1.x * v2.y - v1.y * v2.x
        }
    }

    pub fn length(self) -> f64 {
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    pub fn normalize(self) -> Vec3 {
        self * (1.0 / self.length())
    }



    // Common vectors
    pub fn zero()  -> Vec3 { Vec3::new(0.0, 0.0, 0.0) }
    pub fn ihat() -> Vec3 { Vec3::new(1.0, 0.0, 0.0) }
    pub fn jhat() -> Vec3 { Vec3::new(0.0, 1.0, 0.0) }
    pub fn khat() -> Vec3 { Vec3::new(0.0, 0.0, 1.0) }

    pub fn recip(&self) -> Self {
        // 1.0 / 0.0 -> INF, 1.0 / -0.0 -> NEG_INF
        Vec3::new(
            1.0 / self.x,
            1.0 / self.y,
            1.0 / self.z
        )
    }
}
*/

/*
// Use compiler to ensure unit vectors are used when parameters expect unit vectors
// instead of getting vecs that are too large
// newtype idiom
struct UnitVec3(Vec3);
*/

fn main() { }

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn adding_and_subtracting_two_tuples() {
        let a1: Vec3 = Vec3::new(3.0, -2.0, 5.0);
        let a2: Vec3 = Vec3::new(-2.0, 3.0, 1.0);
        assert_eq!(Vec3::add(a1, a2), Vec3 { x: 1.0, y: 1.0, z: 6.0 });
        // assert_eq!(Vec3)
    }

    #[test]
    fn reflecting_vecs() {
        let v: Vec3 = Vec3::new(1.0, -1.0, 0.0);
        let n: Vec3 = Vec3::new(0.0, 1.0, 0.0);
        assert_eq!(
            (-v).reflect(n),
            Vec3::new(1.0, 1.0, 0.0)
        );
    }

}
