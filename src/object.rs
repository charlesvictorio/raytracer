use crate::vec3::Vec3;
use crate::color::Color;
use crate::ray::Ray;
use crate::aabb::Aabb;

// Additionally, each struct should have a new() and default() method.
// These objects are primitives. They aren't compound objects.

// Maybe split Object trait into multiple traits?
// uv_at and norm_at are used to calculate Intersections

// 2023.07.13 maybe don't require funcs like uv_at, and have intersection() call a private func
pub trait Object {
    // for building bvh: get_aabb, get_centroid
    fn get_aabb(&self) -> Aabb;
    fn get_centroid(&self) -> Vec3;
    
    // Intersection testing
    // for traversing (leaf nodes of) bvh: get_first_ray_obj_int

    // If the object is not hit by the ray, return None.
    // If the object is hit by the ray 1 or more times,
    // return the t value of the nearest intersection.
    fn is_hit_by(&self, ray: &Ray) -> Option<f64>;

    // Find the first intersection between a ray and an object.
    // Not only do you find the t, you need to find everything necessary to calculate the coloring, such as uv and norm.
    fn intersection(&self, ray: &Ray) -> Option<Intersection>;
    // for color_at_ray: get_material_i, uv_at, norm_at
    // Once an intersection has been found, get info about the point of intersection to draw it.
    fn get_material_i(&self) -> usize;
    // maybe these shouldn't be in obj, but should just be helper functions. or maybe another trait like ManualInfo "Finding the uv and norm takes extra work than just finding the intersection (unlike for triangles, where finding the intersection requires you to find the uv)". i think intersection() is the same for all ManualInfo objects. maybe make that a default method body
    fn uv_at(&self, point: Vec3) -> (f64, f64);
    fn norm_at(&self, point: Vec3) -> Vec3; // Get a normal vec of a point on the obj.
}

// Intersection Information
// From what I have seen of my own code, if you need to know whether or not an intersection happened,
// you also need to know where (in terms of xyz and uv) and normal
// do you need to know point?
pub struct Intersection<'a> {
    pub p: Vec3, // xyz coordinates of the intersection
    pub uv: (f64, f64), // the intersection's uv coordinates on the surface parameterization of the object
    pub norm: Vec3, // normal unit vector
    // dpdu, dpdv, dndu, dndv
    pub wo: Vec3, // aka $\vec{v}$, the unit vector from object to whereever the ray came from
    // ptr to "bsdf, bssrdf"
    // dpdx, dpdy
    // dudx, dvdx, dudy, dvdy
    pub obj: &'a dyn Object, // Object that was intersected
    pub t: f64, // intersection is at ray.start + t * ray.direction
}

impl <'a> Intersection<'a> {
    pub fn new(p: Vec3, uv: (f64, f64), norm: Vec3, wo: Vec3, obj: &'a dyn Object, t: f64
    // xyz: Vec3, uv: (f64, f64), norm: Vec3
    ) -> Intersection<'a> {
        Intersection { p, uv, norm, wo, obj, t }
    }
}