use crate::core_math::{feq, EPSILON};
use crate::object::Intersection;
use crate::vec3::Vec3;
use vecspace::VectorSpace;
use crate::color::Color;
use crate::ray::Ray;
use crate::material::Material;
use crate::scene_and_light::{Scene, Light};
use rand::{Rng, thread_rng};

#[derive(Debug)]
pub struct PointLight {
    pub intensity: Color,
    pub position: Vec3,
    // pub cone_angle: Option<f64> /* default 360 */
}
#[derive(Debug)]
pub struct DirectionalLight {
    pub intensity: Color,
    pub direction: Vec3
}

// Parallelogram area light
#[derive(Debug, Clone)]
pub struct RectAreaLight {
    // Center of the parallelogram.
    pub center: Vec3,
    // half-base and half-height edges of parallelogram.
    pub u: Vec3,
    pub v: Vec3,
    pub nsamples: usize,
    pub norm: Vec3,
    pub intensity: Color,
    // pub cone_angle: Option<f64> /* default 180 */
}

impl PointLight {
    pub fn new(intensity: Color, position: Vec3) -> Self {
        Self { intensity, position }
    }
}

impl DirectionalLight {
    pub fn new(intensity: Color, direction: Vec3) -> Self {
        Self { intensity, direction }
    }
}

impl Light for PointLight {
    fn added_color/*<'a>*/(&self, scene: &Scene, intr: &Intersection, material_at_point: &Material, base_color_at_point: Color) -> Color {
        let vis = self.visibility(scene, intr.p);
        let l_point2light = (self.position - intr.p).normalize();
        // return <dyn Light>::parent_added_color_method(self, scene, point, material_at_point, point2light, normal_at_point, v_point2eye, self.intensity);
        return <dyn Light>::parent_added_color_method::<Self>(/*point,*/ vis, l_point2light, intr.norm, intr.wo, material_at_point, self.intensity, base_color_at_point)
    }

    /*
    fn get_l(&self, point: Vec3) -> Vec3 {
        return (self.position - point).normalize();
    }
    */

    fn visibility(&self, scene: &Scene, point: Vec3) -> f64 {
        // point2light intentionally not normalized, so max_t is 1.0.
        // Hopefully it doesn't mess up `solve_ray_sphere_intersection_quadratic_equation`
        return <dyn Light>::parent_visibility_method(scene, point, self.position - point, 1.0);
    }
}

impl Light for DirectionalLight {
    fn added_color(&self, scene: &Scene, intr: &Intersection, material_at_point: &Material, base_color_at_point: Color) -> Color {
        let vis = self.visibility(scene, intr.p);
        let l_point2light = (-self.direction).normalize();
        return <dyn Light>::parent_added_color_method::<Self>(/*point,*/ vis, l_point2light, intr.norm, intr.wo, material_at_point, self.intensity, base_color_at_point);
    }

    /*
    fn get_l(&self, _point: Vec3) -> Vec3 {
        assert!(feq(self.direction.length(), 1.0), "can't assume dir light's dir is unit length.");
        return (-self.direction).normalize();
    }
    */

    fn visibility(&self, scene: &Scene, point: Vec3) -> f64 {
        // point2light doesnt need to be normalized
        return <dyn Light>::parent_visibility_method(scene, point, -self.direction, f64::INFINITY)
    }
}

impl RectAreaLight {
    pub fn new(
        center: Vec3,
        u: Vec3,
        v: Vec3,
        nsamples: usize,
        intensity: Color,
        // pub cone_angle: Option<f64> /* default 180 */
    ) -> Self {
        let norm = Vec3::cross(u, v).normalize();
        RectAreaLight { center, u, v, nsamples, norm, intensity }
    }

    pub fn sample1position(&self) -> Vec3 {
        // RNG.gen::<f64>(): gets uniform random number from [0 to 1)
        // 2 * RNG.gen::<f64>() - 1: gets uniform random number from [-1 to 1)
        return self.center
        + self.u * (2.0 * thread_rng() /* should probably cache this, but where */.gen::<f64>() - 1.0)
        + self.v * (2.0 * thread_rng().gen::<f64>() - 1.0);
    }

}

// if you put area light up close to object, it will sample biasedly, since it samples by light area, not solid angle.
// unbiased if far away
impl Light for RectAreaLight {
    // add together the intensities for each sublight, if the point is partially visible.
    // if visibility of corner/center/random ray is 0, dont add anything.
    // if the vis is 1, add center ray as an approximation.
    // only sample multiple rays if vis is in btwn 0 and 1.
    // also refactor parent_added_color_method to pass vis from the outside
    fn added_color(&self, scene: &Scene, intr: &Intersection, material_at_point: &Material, base_color_at_point: Color) -> Color {
        let vis = self.visibility(scene, intr.p);
        if feq(vis, 0.0) {
            return Color::black();
        } else if feq(vis, 1.0) {
            return <dyn Light>::parent_added_color_method::<Self>(/*point,*/ vis, (self.center - intr.p).normalize(), intr.norm, intr.wo, material_at_point, self.intensity, base_color_at_point);
        } // semi-visible
        let mut total_intensity_from_this_light = Color::black();
        for sublight_position in self.clone().take(self.nsamples) {
            // method 1: use average vis
            total_intensity_from_this_light += <dyn Light>::parent_added_color_method::<Self>(/*point,*/ vis, (sublight_position - intr.p).normalize(), intr.norm, intr.wo, material_at_point, self.intensity, base_color_at_point);
            // method 2: calculate vis of this specific sublight, doing more work
            // let sublight: PointLight = PointLight::new(
            //     self.intensity,
            //     sublight_position
            // );
            // let vis = <dyn Light>::parent_visibility_method(scene, point, sublight_position - point /* not normalized */, 1.0);
            // total_intensity_from_this_light += <dyn Light>::parent_added_color_method(point, vis, (sublight_position - point).normalize(), normal_at_point, v_point2eye, material_at_point, self.intensity);
        }
        return total_intensity_from_this_light * (1.0 / self.nsamples as f64);
    }

    /*
    // just get point to center.
    // This function is not used for RectAreaLight.added_intensity().
    fn get_l(&self, point: Vec3) -> Vec3 {
        let center = self.corner + self.u.scale(0.5) + self.v.scale(0.5);
        return (center - point).normalize();
    }
    */

    // Average the visibility from all the sublights
    // This function is not used for RectAreaLight.added_intensity().
    fn visibility(&self, scene: &Scene, point: Vec3) -> f64 {
        if self.norm * (point - self.center).normalize() <= 0.0 /* cos theta */ {
            return 0.0;
        }

        // Check if anything intersects bounding oblique rectangular prism

        let mut total_vis: f64 = 0.0;
        for sublight_pos in self.clone().take(self.nsamples) {
            total_vis += <dyn Light>::parent_visibility_method(scene, point, sublight_pos - point, 1.0);
        }
        return total_vis / (self.nsamples as f64);
    }
}


// super/parent/default method that the various lights all extend.
// In a language with inheritance, this would be the parent method.
// Cannot simply write a default method in the trait because the children's methods (specifically RectAreaLight.visibility and maybe .added_intensity) need to call the parent method.
impl dyn Light {
    // calculate visibility from *1* ray with a given direction to a point on a given interval 
    fn parent_visibility_method(scene: &Scene, point: Vec3, point2light: Vec3, max_t: f64) -> f64 {
        // Make a ray going from point on object to light
        let point2light_ray = Ray::new(
            point,
            point2light
        );
        // Need to know what is the first object from point to light, not just whether or not there was one.
        // This is because we need to see if the object is semi-opaque, like glass, to see how visible it should be.
        // (this wont be implemented until much later)
        if let Some(_ /* throw away the specific object for now */) = scene.get_first_thing_ray_hits(
            &point2light_ray,
            EPSILON,
            max_t
        ) {
            return 0.0;
        }
        return 1.0;
    }

    // calculate visibility and l on the caller's side
    fn parent_added_color_method<T>(/*point: Vec3, */vis: f64, l_point2light: Vec3, normal_at_point: Vec3, v_point2eye: Vec3, material_at_point: &Material, lights_intensity: Color, base_color_at_point: Color) -> Color
    where T: Light {
        // Make sure that the object isn't in the light's shadow.
        if feq(vis, 0.0) {
            return Color::black();
        }
        // added color = base color * light.intensity * mat.diffuse * L.N
        // + light.intensity * mat.specular * R.V^mat.shininess
        let mut total_added_color_from_this_light = Color::black();
        // Calculate added intensity from diffuse lighting.
        let l_dot_n = l_point2light * normal_at_point;
        if l_dot_n > 0.0 {
            // total_added_color_from_this_light += (base_color_at_point * lights_intensity) * (diffuse * l_dot_n);
            total_added_color_from_this_light += base_color_at_point.schur(lights_intensity);
            // JUSTMAKEITWORK
        } else {
            // No diffuse light.
            // Also, don't move on to calculating specular.
            // "The specular term should only be included if the dot product of the diffuse term is positive."
            // - Wikipedia: https://en.wikipedia.org/wiki/Phong_reflection_model
            return Color::black();
        }
        // Calculate added intensity from specular lighting.
        let r = l_point2light.reflect(normal_at_point);
        let r_dot_v = r * v_point2eye;
        if r_dot_v > 0.0 {
            // total_added_color_from_this_light += lights_intensity * (material_at_point.specular * r_dot_v.powf(material_at_point.shininess as f64));
            total_added_color_from_this_light += lights_intensity * (0.5 * r_dot_v.powf(0.5 as f64));
            // 2023.01.10 JUSTMAKEITWORK: just so it works for now, gonna change the rt algo anyway
        }

        // total_added_color_from_this_light *= lights_intensity.scale(vis); already used li?
        total_added_color_from_this_light = total_added_color_from_this_light * vis;
        return total_added_color_from_this_light;
    }
}

impl Iterator for RectAreaLight {
    type Item = Vec3;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.sample1position())
    }
}