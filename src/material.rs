/*
Texture::<f64 or Color>::from_file() is not DRY. maybe make macro for code gen? 
*/

use crate::{color::Color, object::Intersection, vec3::Vec3};

use image::{open, RgbImage, DynamicImage};

// Material struct, which just holds properties of the object's surface.
// Just so shapes only need one getter func.

// I could use a design pattern to make this better, but perf is more important rn
#[derive(Debug, Clone)]
pub struct Material {
    // The base color of the object, without accounting for lighting affects.
    // What it looks like under strong white light.
    // Others call it 'albedo', which has other uses maybe.
    // No 'specular color' or 'refraction color'. It will reflect/transmit the same color that it is lol.
    pub color: Texture<Color>,
    // Some objects are light sources, like lightbulbs, suns, neon signs, or glowsticks.
    // Their material emits light. It has a certain color, usually white, and a strength
    pub emitted_light_color: Texture<Color>,
    pub emitted_light_strength: Texture<f64>, // does this need to be texture

    // Chance of specular reflection happening. 
    // diffuse_chance = 1 - specular_chance - refraction_chance
    pub specular_chance: f64,
    // How smooth the specular reflections and refractions are.
    // More smooth means more mirror-like reflections, less smooth means it is like a glossy ornament.
    // Zero means completely diffuse, but set spec_chance to 0 if u want completely diffuse.
    pub smoothness: f64, 

    pub refraction_chance: f64,
    // Index of refraction for refraction
    pub ior: f64,

    // can do this for normal mapping, displacement mapping, etc
    // None: calculate it. or maybe should be "procedural texture" and move uv2norm here??? or maybe both, since normal depends on shape (x sphere) not material
    pub normal: Option<Texture<Vec3>>,
    // Alpha map. Make some or all of the object transparent or semi-transparent
    pub alpha: Texture<f32>


    /*
    // Coefficients for each type of light
    pub ambient: Texture<f64>,
    pub diffuse: f64,
    pub specular: f64,
    // How tight the specular highlight should be.
    // Range: 10 (loose) to 200 (tight)
    pub shininess: f64,
    // How reflective the material is.
    // How much of the color should be from the local color vs from the object in the reflection.
    // Range: 0 (not reflective) to 1 (perfect mirror)
    pub reflectiveness: Texture<f64>,
    */



}

/* Dont wanna make materials trait objs bc then I have to put them in boxes
trait TextureStrategy {
    type Texel; // usually color or f64
    fn get_base_color_at(&self, intr: &Intersection) -> Self::Texel;
}

struct OneValue<T: Copy>(T);

impl<T: Copy> TextureStrategy for OneValue<T> {
    type Texel = T;
    fn get_base_color_at(&self, _intr: &Intersection) -> T {
        self.0
    }
}
*/

// A material can either have just one color or a texture of different colors at different points.
// Same for ambient, reflectiveness, etc
#[derive(Debug, Clone)]
pub enum Texture<TexelType: Copy, /*F: Fn(f64, f64) -> TexelType*/> {
    One(TexelType),
    // Procedural(F),
    Image(ImageTexture<TexelType>),
}

// Wrapper for uv2texel so you forget its an enum.
// Are there situations where it would be faster to match on the caller's side?
impl<TexelType: Copy> Texture<TexelType> {
    pub fn uv2texel(&self, u: f64, v: f64) -> TexelType {
        match self {
            Texture::One(texel) => {*texel},
            Texture::Image(texture) => {texture.uv2texel(u, v)},
        }
    }
}

/*
pub trait Texture {
    type Texel;
    // (u, v) -> color/f64/etc
    fn texel_at(&self, u: f64, v: f64) -> Self::Texel;
    // convert pictures (greyscale for f64 textures) to textures
    fn from_file(path: &str) -> Self;
}
*/

// Examples of texel types: f64, Color
// do mipmaps later
#[derive(Debug, Clone)]
pub struct ImageTexture<TexelType: Copy> {
    texels: Vec<TexelType>,
    ncols: usize,
    nrows: usize,
}

impl<TexelType: Copy> ImageTexture<TexelType> {
    fn colrow2texel(&self, col: usize, row: usize) -> TexelType {
        self.texels[row * self.ncols + col]
    }

    // u and v are within [0, 1], origin in bottom left
    pub fn uv2texel(&self, u: f64, v: f64) -> TexelType { // temporarily pub
        let col = (self.ncols as f64 * u) as usize;
        let row = ((self.nrows as f64) * (1.0 - v)) as usize;
        println!("error - 1: row {} col {} nrow {} ncols {} u {} v {}", row, col, self.nrows, self.ncols, u, v);
        self.colrow2texel(col, row)
    }
}

impl ImageTexture<f64> {
    // path example: "textures/Wood.jpg"
    pub fn from_file(path: &str) -> ImageTexture<f64> {
        let dyn_image = &open(path).expect("Texture image couldn't be loaded.");

        let buffer = dyn_image.to_luma8();
        /*
        let mut texels: Vec<f64> = Vec::new();
        for p in buffer.pixels() {
            texels.push(p.0[0] as f64);
        }
        */

        // let texels: Vec<f64> = dyn_image
            // .to_luma8()
        let texels: Vec<f64> = buffer
            .pixels()
            .map(|p| p.0[0] as f64)
            .collect();

        ImageTexture {
            texels,
            ncols: buffer.width() as usize,
            nrows: buffer.height() as usize,
        }
    }
}

impl ImageTexture<Color> {
    pub fn from_file(path: &str) -> ImageTexture<Color> {
        let dyn_image = &open(path).expect("Texture image couldn't be loaded.");

        let buffer = dyn_image.to_rgb8();
        /*
        let mut texels: Vec<Color> = Vec::new();
        for p in buffer.pixels() {
            texels.push(Color::from_rgb_object::<u8>(p));
        }
        */
        let texels: Vec<Color> = buffer
            .pixels()
            .map(|p| Color::from_rgb_object::<u8>(p))
            .collect();

        ImageTexture {
            texels,
            ncols: buffer.width() as usize,
            nrows: buffer.height() as usize,
        }
    }
}

impl Material {
    pub fn new(color: Texture<Color>, emitted_light_color: Texture<Color>, emitted_light_strength: Texture<f64>, specular_chance: f64, smoothness: f64, refraction_chance: f64, ior: f64, normal: Option<Texture<Vec3>>, alpha: Texture<f32>) -> Self {
        Self { color, emitted_light_color, emitted_light_strength, specular_chance, smoothness, refraction_chance, ior, normal, alpha }
        // Self { color, ambient, diffuse, specular, shininess, reflectiveness}
    }

    /* Turn these into constructors. Material::new_metal() sets metal defaults for non-parameters
pub struct Lambertian {
    pub color: Texture<Color>
}
pub struct Metal {
    pub color: Texture<Color>,
    pub fuzz: f64, // fuzzmapping with Texture<f64>
}
pub struct Dielectric {
    pub ir: f64,
}

pub struct DiffuseLight { // maybe light should be a subtrait of material
    pub emit: Texture<Color>; // idk why a texture and not a single color
}
*/
}

impl Default for Material {
    fn default() -> Self {
        Material {
            color: Texture::One(Color { r: 1.0, g: 0.0, b: 0.0 }),

            emitted_light_color: Texture::One(Color::black()),
            emitted_light_strength: Texture::One(0.0),
            specular_chance: 0.0,
            smoothness: 0.0, 
            refraction_chance: 0.0,
            ior: 1.0,
            normal: None,
            alpha: Texture::One(1.0),
        }
    }
}