use crate::vec3::Vec3;
use vecspace::VectorSpace;
use crate::matrix::Mat;
use crate::ray::Ray;

#[derive(Debug, Clone)]
pub struct Aabb {
    // Min x, y, and z coordinates
    pub min: Vec3,
    // Max x, y, and z coordinates
    pub max: Vec3
    // The min and max fields should be public
}

impl Aabb {
    pub fn new(min: Vec3, max: Vec3) -> Self {
        Aabb { min, max }
    }
    pub fn add_point(&mut self, point: Vec3) {
        self.min.x = f64::min(self.min.x, point.x);
        self.min.y = f64::min(self.min.y, point.y);
        self.min.z = f64::min(self.min.z, point.z);
        self.max.x = f64::max(self.max.x, point.x);
        self.max.y = f64::max(self.max.y, point.y);
        self.max.z = f64::max(self.max.z, point.z);
    }

    // Transform a Aabb by a matrix.
    // Transform the 8 corners of the box, and make a new Aabb with those points.
    // Make sure to "consolidate" all your transformations into one matrix before making an Aabb.
    // Aabb doesn't "distribute" over transformations.
    // ![diagram](aabb_transform.png)
    pub fn transform(&self, mat: &Mat) -> Self {
        let mut transformed_aabb = Aabb::default();
        for corner in [
            // eight corners
            self.min,
            Vec3::new(self.min.x, self.min.y, self.max.z),
            Vec3::new(self.min.x, self.max.y, self.min.z),
            Vec3::new(self.min.x, self.max.y, self.max.z),
            Vec3::new(self.max.x, self.min.y, self.min.z),
            Vec3::new(self.max.x, self.min.y, self.max.z),
            Vec3::new(self.max.x, self.max.y, self.min.z),
            self.max,
        ] {
            transformed_aabb.add_point(corner.transform(mat));
        }
        return transformed_aabb;
    }

    // Merge 2 Aabbs together.
    pub fn merge(b1: &Self, b2: &Self) -> Self {
        Self::new(
            Vec3::new(
                f64::min(b1.min.x, b2.min.x),
                f64::min(b1.min.y, b2.min.y),
                f64::min(b1.min.z, b2.min.z),
            ),
            Vec3::new(
                f64::max(b1.max.x, b2.max.x),
                f64::max(b1.max.y, b2.max.y),
                f64::max(b1.max.z, b2.max.z),
            )
        )
    }

    // like merge but aabb1.subsume(&aabb2) instead of aabb1 = Aabb::merge(&aabb1, &aabb2)
    pub fn subsume(&mut self, other: &Self) {
         self.min.x = f64::min(self.min.x, other.min.x);
         self.min.y = f64::min(self.min.y, other.min.y);
         self.min.z = f64::min(self.min.z, other.min.z);
                   
         self.max.x = f64::max(self.max.x, other.max.x);
         self.max.y = f64::max(self.max.y, other.max.y);
         self.max.z = f64::max(self.max.z, other.max.z);
    }

    // Check whether a point is in an Aabb.
    pub fn contains_point(&self, p: Vec3) -> bool {
           self.min.x <= p.x && p.x <= self.max.x
        && self.min.y <= p.y && p.y <= self.max.y
        && self.min.z <= p.z && p.z <= self.max.z
    }

    // Checks whether another Aabb is completely contained within an Aabb.
    pub fn contains_aabb(&self, other: Self) -> bool {
        self.contains_point(other.min) && self.contains_point(other.max)
    }

    pub fn diagonal(&self) -> Vec3 {
        self.max - self.min
    }

    pub fn volume(&self) -> f64 {
        let diagonal: Vec3 = self.diagonal();
        diagonal.dot(diagonal) // dot product. length_sq
    }

    pub fn surface_area(&self) -> f64 {
        let d: Vec3 = self.diagonal();
        2.0 * (d.x * d.y + d.y * d.z + d.z * d.x)
    }

    // if this doesnt work: read
    // - ray tracer challenge
    // - scratch a pixel: https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection.html
    pub fn intersect(&self, ray: &Ray) -> bool {
        let x1int = (self.min.x - ray.start.x) * ray.one_over_dir.x;
        let x2int = (self.max.x - ray.start.x) * ray.one_over_dir.x;
        let xmin = x1int.min(x2int);
        let xmax = x1int.max(x2int);
        let y1int = (self.min.y - ray.start.y) * ray.one_over_dir.y;
        let y2int = (self.max.y - ray.start.y) * ray.one_over_dir.y;
        let ymin = y1int.min(y2int);
        let ymax = y1int.max(y2int);
        let z1int = (self.min.z - ray.start.z) * ray.one_over_dir.z;
        let z2int = (self.max.z - ray.start.z) * ray.one_over_dir.z;
        let zmin = z1int.min(z2int);
        let zmax = z1int.max(z2int);
        let largest_minimum = xmin.max(ymin).max(zmin);
        let smallest_maximum = xmax.min(ymax).min(zmax);
        largest_minimum < smallest_maximum 
    }

    /*
    // Helper function for bvh.ray_intersect()
    // Finds the 2 points of intersection between a ray and 2 parallel axis-aligned planes.
    // Let's say the axis is x.
    // Find out where the ray intersects the planes: x = x1 and x = x2
    // start: the x value of the ray's origin
    // one_over_dir: one over the x value of the ray's direction vector
    fn check_axis(x1: f64, x2: f64, start: f64, one_over_dir: f64) -> (f64, f64) {

    }
    */

    // todo
    // - bvh / groups

}

impl Default for Aabb {
    fn default() -> Self {
        Aabb::new(
            Vec3::new(f64::INFINITY, f64::INFINITY, f64::INFINITY), // Min will decrease when new item is addded.
            Vec3::new(-f64::INFINITY, -f64::INFINITY, -f64::INFINITY), // Max will increase when new item is added.
        )
    }
}
